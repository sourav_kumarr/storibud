<?php
    include('header.php');
    $book_id = $_REQUEST['_'];
    echo "<input type='hidden' value=".$book_id." id='book_id' />";
?>
<style>
    .boxes{
        background: white;
        min-height:100px;
        border:1px solid #ddd;
        margin-bottom: 10px;
    }
    .Review{
        box-shadow: 3px 3px 2px #ccc;
        margin-bottom: 20px;
    }
</style>
<!-- page content -->
<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2 style="cursor:pointer" onclick="back()"><i class="fa fa-arrow-circle-left"></i> All Books <small></small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <button onclick="window.location='api/excelProcess.php?dataType=particularBook&book_id=<?php echo $book_id ?>'" class="btn btn-info btn-sm">Download Excel File</button>
                                </li>
                                <!--<li>
                                    <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                        <span></span> <b class="caret"></b>
                                    </div>
                                </li>-->
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                View "<span id="book_name1"></span>" Detail
                            </p>
                        </div>
                    </div>
                    <div class="col-md-7 boxes">
                        <h2 style="text-align: center">Book Detail</h2>
                        <hr>
                        <div class="col-md-12">
                            <div class="col-md-8" style="padding: 0">
                                <div class="form-group">
                                    <label>Book Name</label>
                                    <p id="book_name"></p>
                                </div>
                                <div class="form-group">
                                    <label>Book Author</label>
                                    <p id="book_author"></p>
                                </div>
                                <div class="form-group">
                                    <label>Book Narrator</label>
                                    <p id="book_narrator"></p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <img id="book_image" src="images/user.png" class="img-responsive img-thumbnail" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Book Description</label>
                                <p id="book_desc"></p>
                            </div>
                            <br>
                            <div class="clearfix"></div>
                            <div class="col-md-4 row">
                                <div class="form-group">
                                    <label>Audio File</label>
                                    <p id="audio_file"></p>
                                </div>
                            </div>
                            <div class="col-md-4 row">
                                <div class="form-group">
                                    <label>Book Category</label>
                                    <p id="book_category"></p>
                                </div>
                            </div>
                            <div class="col-md-4 row">
                                <div class="form-group">
                                    <label>Book Price</label>
                                    <p id="book_price"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-bottom: 30px">
                            <br>
                            <div class="col-md-4 row">
                                <div class="form-group">
                                    <label>Book Visibility</label>
                                    <p id="visibility"></p>
                                </div>
                            </div>
                            <div class="col-md-4 row">
                                <div class="form-group">
                                    <label>Play Time</label>
                                    <p id="playTime"></p>
                                </div>
                            </div>
                            <div class="col-md-4 row">
                                <div class="form-group">
                                    <label>Audio Preview</label>
                                    <p id="shortAudio"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="col-md-12 boxes">
                            <h2 style="text-align: center">Book Rating</h2>
                            <hr>
                            <div class="col-md-12" style="text-align: center;margin-bottom: 20px">
                                <label class="stars" style="margin-left:40%" id="userRate"></label>
                                <br><br>
                                <label>Rating (<span id="userRate2">0.0</span> / 5.0)</label>
                            </div>
                        </div>
                        <div class="col-md-12 boxes" id="userReviews">
                            <h2 style="text-align: center">User Review</h2>
                            <hr>
                            <p style="text-align: center"><label>No Reviews Available Yet</label></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    var book_id = $("#book_id").val();
    var url = "api/booksProcess.php";
    $.post(url,{"type":"getBook","bookId":book_id} ,function (data) {
        var status = data.Status;
        if (status == "Success") {
            var bookData = data.bookData;
            $("#book_name1").html(bookData.book_name);
            $("#book_name").html(bookData.book_name);
            $("#book_author").html(bookData.book_author);
            $("#book_narrator").html(bookData.book_narrator);
            $("#playTime").html("<i class='fa fa-clock-o'></i> "+bookData.play_time);
            $("#book_desc").html(bookData.book_desc);
            $("#audio_file").html(bookData.audio_file + "( <i onclick=playSong('" + bookData.audio_file + "') class='fa fa-play' style='color:green;cursor:pointer'></i> )");
            $("#shortAudio").html(bookData.short_audio_file + "( <i onclick=playSong('" + bookData.short_audio_file + "') class='fa fa-play-circle' style='color:green;cursor:pointer'></i> )");
            if (bookData.front_look != "") {
                $("#book_image").attr("src", "api/Files/images/" + bookData.front_look);
            }
            $("#book_category").html(data.Category);
            var price = parseFloat(bookData.list_price);
            price = price.toLocaleString();
            $("#book_price").html("$" + price);
            if (bookData.status == "0") {
                $("#visibility").html("InVisible");
            } else {
                $("#visibility").html("Visible");
            }
        }
    });
    $.post(url,{"type":"getRating","bookId":book_id} ,function (data) {
        var status = data.Status;
        if (status == "Success") {
            var userRate = data.userRate;
            var bookData2 = data.bookData;
            $("#userRate").html(parseFloat(userRate.toFixed(1)));
            $('label.stars').stars();
            $("#userRate2").html(parseFloat(userRate.toFixed(1)));
            var reviewDataDiv = "<h2 style='text-align: center'>User Review</h2><hr>";
            for(var i=0;i<bookData2.length;i++){
                var userImage = "";
                if(bookData2[i].user_image == ""){
                    userImage = "images/img.png";
                }else{
                    userImage = "api/Files/images/"+bookData2[i].user_image;
                }
                reviewDataDiv+="<div class='col-md-12 Review'><div class='col-md-2'><img src='"+userImage+"' " +
                "class='img-responsive img-circle' /></div><div class='col-md-10'><label>"+bookData2[i].user_name+"</label>" +
                "<label class='stars2'>"+parseFloat(bookData2[i].user_rate)+"</label><p>"+bookData2[i].user_review.substr(0,45)+"...</p></div></div>";
            }
            $("#userReviews").html(reviewDataDiv);
            $('label.stars2').stars();
        }
        else{
            //showMessage(data.Message,"red");
        }
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
    function currencyFormat(number)
    {
        number=number.toString();
        var lastThree = number.substring(number.length-3);
        var otherNumbers = number.substring(0,number.length-3);
        if(otherNumbers != '')
            lastThree = ',' + lastThree;
        return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
    }
</script>