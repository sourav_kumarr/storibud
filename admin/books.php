<?php
include('header.php');
include('api/Classes/CONNECT.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
require_once('api/Classes/CATEGORY.php');
$conn = new \Classes\CONNECT();
$category = new \Classes\CATEGORY();
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">
        <a href="users"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
                <div class="count" id="userCount"></div>
                <span class="count_bottom"><i class="green">Click </i>to Expand</span>
            </div></a>
        <a href="books"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-volume-up"></i> Total Audio Books</span>
                <div class="count" id="booksCount"></div>
                <span class="count_bottom"><i class="green"></i> in All Categories</span>
            </div></a>
        <a href="index"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-book"></i> Total Categories</span>
                <div class="count green" id="catCount"></div>
                <span class="count_bottom"><i class="green"></i> Click to Expand</span>
            </div></a>
        <a href="membership"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-users"></i> MemberShip Types</span>
                <div class="count" id="membershipCount"></div>
                <span class="count_bottom"> Click to View</span>
            </div></a>
        <a href="discount"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cc-discover"></i> Discount Coupons</span>
                <div class="count" id="allCoupon"></div>
                <span class="count_bottom"><i class="green" id="activeCoupon"></i> is Still Active</span>
            </div></a>
        <a href="orders"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-first-order"></i> Orders</span>
                <div class="count" id="orderCount"></div>
                <span class="count_bottom"><i class="green"></i>Click to Expand</span>
            </div></a>
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Books
                            <small></small>
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <!--                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>-->
                            <li><a class="btn btn-info" onclick='addNewBook()' style="color:white"><i
                                            class="fa fa-plus"></i> Add New Book</a></li>
                            <li>
                                <button onclick="window.location='api/excelProcess.php?dataType=allBooks'" class="btn btn-info btn-sm">Download Excel File</button>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            Maintain the Audio Book According to Category and From Books Pool as well
                        </p>
                        <table id="bookTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Front</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Author</th>
                                <th>Narrator</th>
                                <th>Price</th>
                                <th>Play Time</th>
                                <th>Visibility</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $link = $conn->connect();
                            if ($link) {
                                $query = "select * from books order by book_id DESC";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
                                        while ($bookData = mysqli_fetch_array($result)) {
                                            $j++;
                                            ?>
                                            <tr>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <td data-title='Image'>
                                                    <img src='api/Files/images/<?php echo $bookData['front_look'] ?>'
                                                         style='height:35px' class='img-thumbnail'>
                                                </td>
                                                <td data-title='Title'>
                                                    <a href='bdet?_=<?php echo $bookData['book_id'] ?>'><?php echo $bookData['book_name'] ?></a>
                                                </td>
                                                <td data-title='Category'>
                                                    <?php $catData = $category->getParticularCatData($bookData['cat_id']);
                                                    $catData = $catData['catData'];
                                                    echo $catData['cat_name'] ?>
                                                </td>

                                                <td data-title='Author'><?php echo $bookData['book_author'] ?>
                                                </td>
                                                <td data-title='Narrator'><?php echo $bookData['book_narrator'] ?></td>
                                                <?php
                                                $amount = $bookData['list_price'];
                                                setlocale(LC_MONETARY, 'en_IN');
                                                $amount = money_format('%!i', $amount);
                                                ?>
                                                <td data-title='Price'>$<?php echo $amount ?></td>
                                                <td data-title='Play Time'><i class='fa fa-clock-o'></i> <?php echo $bookData['play_time'] ?></td>
                                                <td class='buttonsTd' data-title='Visibility'>
                                                    <?php
                                                    $box = "";
                                                    if ($bookData['book_status'] == "1") {
                                                        $box = "checked";
                                                    }
                                                    ?>
                                                    <input data-onstyle='info' class='bookcheckstatus' <?php echo $box ?>
                                                    data-toggle='toggle' value="<?php echo $bookData['book_status'] ?>"
                                                    id="<?php echo $bookData['book_id']?>" data-size='mini' data-style='ios'
                                                    type='checkbox' />
                                                </td>
                                                <td data-title='Action'>

                                                    <i class='fa fa-play-circle'
                                                    onclick=playSong('<?php echo $bookData['short_audio_file'] ?>')
                                                    style='cursor: pointer;color: #1ABB9C'></i>&nbsp;&nbsp;&nbsp;

                                                    <i class='fa fa-play'
                                                    onclick=playSong('<?php echo $bookData['audio_file'] ?>')
                                                    style='cursor: pointer;color: #1ABB9C'></i>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <i class='fa fa-edit'
                                                    onclick=editBookData('<?php echo $bookData['book_id'] ?>')
                                                    style='cursor: pointer;color:#31B0D5'></i>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <i class='fa fa-trash'
                                                    onclick=deleteBook('<?php echo $bookData['book_id'] ?>')
                                                    style='color:#D05E61;cursor: pointer'/>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
</div>
<?php
    include('footer.php');
?>
<script>
    $(document).ready(function () {
        $('#bookTable').DataTable({});
    });
</script>