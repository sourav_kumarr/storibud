<?php
include('Classes/MP3File.php');
require_once ('Classes/BOOKS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$BooksClass = new \Classes\BOOKS();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $BooksClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "addBook")
{
    $requiredfields = array('book_title','book_desc','book_author','book_narrator','book_price','cat_id','book_status');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $BooksClass->apiResponse($response);
        return false;
    }
    $book_title = trim($_POST['book_title']);
    $book_desc = trim($_POST['book_desc']);
    $book_author = trim($_POST['book_author']);
    $book_narrator = trim($_POST['book_narrator']);
    $book_price = trim($_POST['book_price']);
    $cat_id = trim($_POST['cat_id']);
    $book_status = trim($_POST['book_status']);
    $discount_on_book = trim($_POST['discount_on_book']);
    ($response = $BooksClass->checkBookExistence($book_title,$cat_id));
    if($response[STATUS] == Success) {
        $response = $BooksClass->addBook($cat_id,$book_title,$book_desc,$book_author,$book_narrator,$book_price,$book_status,$discount_on_book);
    }
    if($response[STATUS] == Error){
        $BooksClass->apiResponse($response);
        return false;
    }
    $bookId = $response['bookId'];
    $temp = $BooksClass->getParticularBookData($bookId);
    $response['bookData'] = $temp['bookData'];
    $response[ImagesBaseURLKey] = ImagesBaseURL;
    unset($response['bookId']);
    $BooksClass->apiResponse($response);
}
else if($type == "editBook")
{
    $requiredfields = array('book_id','book_title','book_desc','book_author','book_narrator','book_price','cat_id',
    'book_status','imageChanged','audioChanged','shortaudioChanged');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $BooksClass->apiResponse($response);
        return false;
    }
    $book_id = trim($_POST['book_id']);
    $book_title = trim($_POST['book_title']);
    $book_desc = trim($_POST['book_desc']);
    $book_author = trim($_POST['book_author']);
    $book_narrator = trim($_POST['book_narrator']);
    $book_price = trim($_POST['book_price']);
    $cat_id = trim($_POST['cat_id']);
    $book_status = trim($_POST['book_status']);
    $discount_on_book = trim($_POST['discount_on_book']);
    $imageChanged = trim($_POST['imageChanged']);
    $audioChanged = trim($_POST['audioChanged']);
    $shortaudioChanged = trim($_POST['shortaudioChanged']);
    $response = $BooksClass->editBook($book_id,$book_title,$book_desc,$book_author,$book_narrator,$book_price,$cat_id,
    $book_status,$imageChanged,$audioChanged,$shortaudioChanged,$discount_on_book);
    if($response[STATUS] == Error){
        $BooksClass->apiResponse($response);
        return false;
    }
    $bookId = $response['bookId'];
    $temp = $BooksClass->getParticularBookData($bookId);
    $response['bookData'] = $temp['bookData'];
    $response[ImagesBaseURLKey] = ImagesBaseURL;
    $response[AudiosBaseURLKey] = AudiosBaseURL;
    unset($response['bookId']);
    $BooksClass->apiResponse($response);
}
else if($type == "getBooksOfCategory")
{
    $requiredfields = array('cat_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $BooksClass->apiResponse($response);
        return false;
    }
    $cat_id = trim($_POST['cat_id']);
    $response = $BooksClass->getCategoryAllBooks($cat_id);
    $BooksClass->apiResponse($response);
}
else if($type == "getBook"){
    $requiredfields = array('bookId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $BooksClass->apiResponse($response);
        return false;
    }
    $bookId = $_REQUEST['bookId'];
    $response = $BooksClass->getParticularBookData($bookId);
    $BooksClass->apiResponse($response);
}
else if($type == "getRating"){
    $requiredfields = array('bookId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $BooksClass->apiResponse($response);
        return false;
    }
    $bookId = $_REQUEST['bookId'];
    $response = $BooksClass->getRatesAndReviews($bookId);
    $BooksClass->apiResponse($response);
}
else if($type == "statusChange")
{
    $requiredfields = array('book_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $BooksClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $book_id = $_REQUEST['book_id'];
    $response = $BooksClass->statusChange($book_id,$value);
    if($response[STATUS] == Error) {
        $BooksClass->apiResponse($response);
        return false;
    }
    $BooksClass->apiResponse($response);
}
else if($type == "deleteBook")
{
    $requiredfields = array('book_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $BooksClass->apiResponse($response);
        return false;
    }
    $book_id = $_REQUEST['book_id'];
    $response = $BooksClass->deleteBook($book_id);
    if($response[STATUS] == Error) {
        $BooksClass->apiResponse($response);
        return false;
    }
    $BooksClass->apiResponse($response);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $BooksClass->apiResponse($response);
}
?>