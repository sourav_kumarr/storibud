<?php
function RequiredFields($getvars, $requiredfields)
{
	$error = "";
	$keys = array_keys($getvars);
	if(count($getvars) == count($requiredfields))
	{
		for($i=0;$i<count($requiredfields);$i++) 
		{
			$feild = $requiredfields[$i];
			if(in_array($feild, $keys))
			{
				if($getvars[$feild]=="") 
				{
					$error = $error.$feild.",";
				}
			}
			else
			{
				$error = $error.$feild.",";
			}
		}
		$error = trim($error);
	}
	else
	{
		for($i=0;$i<count($requiredfields);$i++) 
		{
			$feild = $requiredfields[$i];
			if(in_array($feild, $keys))
			{
				if($getvars[$feild]=="") 
				{
					$error = $error.$feild.",";
				}
			}
			else
			{
				$error = $error.$feild.",";
			}
		}
		$error = trim($error);
	}
	if($error != "")
	{
		$error = rtrim($error,",");
		$response[STATUS]=Error;
		$response[MESSAGE]='The following fields are required: '.$error;
		return $response;
	}
    $response[STATUS]=Success;
    $response[MESSAGE]='';
    return $response;
}
?>