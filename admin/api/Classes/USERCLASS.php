<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 1/5/17
 * Time: 1:27 PM
 */
namespace Classes;
require_once('CONNECT.php');
class USERCLASS
{
    public $link = null;
    public $response = array();
    public $currentDate=null;
    public $currentDateStamp=null;
    public $currentDateTime=null;
    public $currentDateTimeStamp=null;
    function __construct()
    {
        $this->link = new CONNECT();
        $this->currentDate = date('d M Y');
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateStamp = strtotime($this->currentDate);
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function registerUser($username, $email,$password,$registerSource,$file_name,$fcm_id)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "insert into users (user_name,user_email,password,user_status,register_source,user_profile,user_type,user_fcm_id)
            VALUES ('$username','$email','$password','1','$registerSource','$file_name','user','".$fcm_id."')";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Users Registered Successfully";
                $this->response['userId'] = $this->link->getLastId();
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function signIn($email, $password , $fcm_id)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_email = '$email' and password = '$password' and user_type='user'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $rows = mysqli_fetch_array($result);

                    $this->updateFcmId($fcm_id,$rows['user_id']);

                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Login Success";
                    $this->response['userId'] = $rows['user_id'];
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Login Credentials";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function checkEmailExistence($email)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from users where user_email = '$email' and user_type = 'user'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "E-Mail Address Already Registered";
                    $row = mysqli_fetch_array($result);
                    $this->response['userId'] = $row['user_id'];
                }
                else{
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Now Register";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularUserData($userId)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from users where user_id='$userId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $userData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid User";
                    $this->response['UserData'] = $userData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid User";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getAllUsersData()
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from users order by user_id DESC";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($rows = mysqli_fetch_assoc($result)){
                        $userData[] = $rows;
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "User Data Found";
                    $this->response['UserData'] = $userData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function updateUser($userId,$username, $email, $password, $registerSource,$file_name,$fcm_id)
    {
        $link = $this->link->connect();
        if($link) {
            if($registerSource == "email") {
                $query = "update users set user_name='$username',user_email='$email',password='$password' 
                ,register_source='$registerSource',user_profile='$file_name',user_fcm_id='$fcm_id' where user_id='$userId' ";
            }else{
                $query = "update users set user_name='$username',user_email='$email' 
                ,register_source='$registerSource',user_profile='$file_name',user_fcm_id='$fcm_id' where user_id='$userId' ";
            }
            $result = mysqli_query($link,$query);
            if($result)
            {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Data Updated Successfully";
                $this->response['userId'] = $userId;
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function updateFcmId($fcm_id,$userId)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "update users set user_fcm_id='$fcm_id' where user_id='$userId' ";

            $result = mysqli_query($link,$query);
            if($result)
            {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Data Updated Successfully";
                $this->response['userId'] = $userId;
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }


    public function statusChange($user_id,$value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE users SET user_status='$value' WHERE user_id='$user_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function deleteUser($user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from users WHERE user_id='$user_id'");
                    if ($update) {
                        $row = mysqli_fetch_array($result);
                        if($row['user_profile'] != "") {
                            unlink("Files/images/" . $row['user_profile']);
                        }
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Users Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function countAllUsers(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_type = 'user'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Count Found";
                $this->response['Count'] = $num;
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
                $this->response['Count'] = 0;
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            $this->response['Count'] = 0;
        }
        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}