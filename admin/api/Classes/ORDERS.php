<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/22/2017
 * Time: 9:18 PM
 */

namespace Classes;
require_once('CONNECT.php');
require_once('PRODUCTS.php');
require_once('USERCLASS.php');
class ORDERS
{
    public $link = null;
    public $bookClass = null;
    public $userClass = null;
    public $response = array();
    function __construct()
    {
        $this->link = new CONNECT();
        $this->productClass = new PRODUCTS();
        $this->userClass = new USERCLASS();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function addToCart($product_id,$user_id){
        $link = $this->link->connect();
        if($link) {
            $query="select * from cart where product_id='$product_id' and user_id = '$user_id'";
            $result = mysqli_query($link,$query);
            if($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Product Already Added in Cart";
                }
                else{
                    $query="insert into cart (product_id,user_id,added_on) values('$product_id','$user_id',
                    '$this->currentDateTimeStamp')";
                    $result = mysqli_query($link,$query);
                    if($result) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Product Added To Cart Successfully";
                        $cartResponse = $this->getCart($user_id);
                        $this->response['cartCount'] = count($cartResponse['cartArray']);
                    }
                    else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getCart($user_id){
        $link = $this->link->connect();
        if($link) {
            $query="select * from cart where user_id = '$user_id'";
            $result = mysqli_query($link,$query);
            if($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    while($rows = mysqli_fetch_array($result)){
                        $product_id = $rows['product_id'];
                        $user_id = $rows['user_id'];
                        $userResponse = $this->userClass->getParticularUserData($user_id);
                        $productResponse = $this->productClass->getParticularProductData($product_id);
                        $userData = $userResponse['UserData'];
                        $productData = $productResponse['ProductData'];
                        $cartArray[] = array(
                            "cart_id"=>$rows['cart_id'],
                            "product_id"=>$rows['product_id'],
                            "product_name"=>$productData['product_name'],
                            "product_price"=>$productData['product_price'],
                            "product_image"=>$productData['product_image'],
                            BaseURLKey=>BaseURL,
                            "user_id"=>$rows['user_id'],
                            "user_name"=>$userData['user_name'],
                            "added_on"=>$rows['added_on']
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Available in Cart";
                    $this->response['cartArray'] = $cartArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Cart is Empty";
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function deleteItemFromCart($cart_id){
        $link = $this->link->connect();
        if($link) {
            $query="select * from cart where cart_id = '$cart_id'";
            $result = mysqli_query($link,$query);
            if($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $query = "delete from cart where cart_id = '$cart_id'";
                    $result = mysqli_query($link,$query);
                    if($result){
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Item Deleted From Cart";
                    }else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Cart Request";
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function clearCart($user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "delete from cart where user_id = '$user_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Cart Cleared Successfully";
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function createOrder($json){
        $link = $this->link->connect();
        if($link) {
            $json = json_decode($json,true);
            $order_amount = $json['order_amount'];
            $order_user_id = $json['user_id'];
            $order_number = rand(000000000,999999999);
            $order_detail = $json['order_detail'];
            $query = "insert into orders (order_number,amount,payment_status,order_dated,order_userId,order_status) values
            ('$order_number','$order_amount','Pending','$this->currentDateTimeStamp','$order_user_id','Pending')";
            $result = mysqli_query($link,$query);
            if($result){
                $order_id = $this->link->getLastId();
                for($i=0;$i<count($order_detail);$i++){
                    $product_id = $order_detail[$i]['product_id'];
                    $query = "insert into orderdetail (order_id,item_id,user_id) values ('$order_id','$product_id','$order_user_id')";
                    $result = mysqli_query($link,$query);
                    if($result){
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Order Created Successfully";
                        $this->response['order_id'] = $order_id;
                    }
                    else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularOrderData($order_id)
    {
        $link = $this->link->connect();
        $order_detail=array();
        if($link) {
            $query="select * from orders where order_id='$order_id'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $row = mysqli_fetch_assoc($result);
                    $order_id = $row['order_id'];
                    $query2 = "select * from orderdetail where order_id = '$order_id'";
                    $result2 = mysqli_query($link,$query2);
                    if($result2){
                        $num = mysqli_num_rows($result2);
                        if($num>0){
                            while($detail = mysqli_fetch_array($result2)){
                                $product_id = $detail['item_id'];
                                $temp = $this->productClass->getParticularProductData($product_id);
                                $temp = $temp['ProductData'];
                                $order_detail[]=array(
                                    "detail_id"=>$detail['detail_id'],
                                    "product_name"=>$temp['product_name'],
                                    "product_id"=>$product_id,
                                    "product_image"=>$temp['product_image'],
                                    "product_desc"=>$temp['product_desc'],
                                    "file_type"=>$temp['file_type'],
                                    "product_price"=>$temp['product_price'],
                                    "file_name"=>$temp['file_name'],
                                    "user_id"=>$detail['user_id'],
                                    BaseURLKey=>BaseURL
                                );
                            }
                            $user_id = $row['order_userId'];
                            $userData = $this->userClass->getParticularUserData($user_id);
                            $userData = $userData['UserData'];
                            $order = array(
                                "order_id"=>$row['order_id'],
                                "order_number"=>$row['order_number'],
                                "transaction_id"=>$row['transaction_id'],
                                "amount"=>$row['amount'],
                                "payment_status"=>$row['payment_status'],
                                "order_dated"=>$row['order_dated'],
                                "order_userId"=>$row['order_userId'],
                                "order_user_name" => $userData['user_name'],
                                "order_user_profile" => $userData['user_profile'],
                                "order_detail"=>$order_detail
                            );
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "Order Data Exist";
                        }
                        else{
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "Empty Order Details";
                        }
                    }
                    else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Order Found";
                    $this->response['order'] = $order;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Order Identification";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getAllOrders()
    {
        $link = $this->link->connect();
        $order_detail=array();
        $order=array();
        if($link) {
            $query="select * from orders order by order_id desc";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($row = mysqli_fetch_assoc($result)) {
                        $order_id = $row['order_id'];
                        $query2 = "select * from orderdetail where order_id = '$order_id'";
                        $result2 = mysqli_query($link, $query2);
                        if ($result2) {
                            $num = mysqli_num_rows($result2);
                            if ($num > 0) {
                                unset($order_detail);
                                while ($detail = mysqli_fetch_array($result2)) {
                                    $product_id = $detail['item_id'];
                                    $temp = $this->productClass->getParticularProductData($product_id);
                                    $temp = $temp['ProductData'];
                                    $order_detail[]=array(
                                        "detail_id"=>$detail['detail_id'],
                                        "product_name"=>$temp['product_name'],
                                        "product_image"=>$temp['product_image'],
                                        "product_desc"=>$temp['product_desc'],
                                        "file_type"=>$temp['file_type'],
                                        "product_price"=>$temp['product_price'],
                                        "file_name"=>$temp['file_name'],
                                        "user_id"=>$detail['user_id'],
                                        BaseURLKey=>BaseURL
                                    );
                                }
                                $user_id = $row['order_userId'];
                                $userData = $this->userClass->getParticularUserData($user_id);
                                $userData = $userData['UserData'];
                                $order[] = array(
                                    "order_id" => $row['order_id'],
                                    "order_number" => $row['order_number'],
                                    "transaction_id" => $row['transaction_id'],
                                    "amount" => $row['amount'],
                                    "payment_status" => $row['payment_status'],
                                    "order_dated" => $row['order_dated'],
                                    "order_user_id" => $row['order_userId'],
                                    "order_user_name" => $userData['user_name'],
                                    "order_user_profile" => $userData['user_profile'],
                                    "order_detail" => $order_detail
                                );
                                $this->response[STATUS] = Success;
                                $this->response[MESSAGE] = "Order Data Exist";
                            } else {
                                $this->response[STATUS] = Success;
                                $this->response[MESSAGE] = "Empty Order Details";
                            }
                        } else {
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = $this->link->sqlError();
                        }
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Order Found";
                    $this->response['order'] = $order;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Order Identification";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function updatePaymentStatus($order_id,$transaction_number,$payment_status){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from orders where order_id = '$order_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    if($payment_status == "Success"){
                        $order_status = 'Success';
                    }else{
                        $order_status = 'Pending';
                    }
                    $query="update orders set transaction_id = '$transaction_number',payment_status='$payment_status',
                    order_status='$order_status' where order_id = '$order_id'";
                    $result = mysqli_query($link,$query);
                    if($result){
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Payment Updated";
                        if($payment_status == "Success"){
                            $order_response = $this->getParticularOrderData($order_id);
                            $orderData = $order_response['order'];
                            $order_detail = $orderData['order_detail'];
                            for($i=0;$i<count($order_detail);$i++){
                                $product_id = $order_detail[$i]['product_id'];
                                $user_id = $orderData['order_userId'];
                                $query = "insert into purchased_record (item_id,user_id,purchased_on)
                                values ('$product_id','$user_id','$this->currentDateTimeStamp')";
                                mysqli_query($link,$query);
                            }
                        }
                    }
                    else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Order Identification";
                }
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
    /*public function cancelOrder($order_id)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "select * from orders where order_id='$order_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE orders SET order_status='Cancelled' WHERE order_id='$order_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Order Has Been Cancelled Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }*/
}