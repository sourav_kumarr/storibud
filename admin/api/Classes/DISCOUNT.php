<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/22/2017
 * Time: 6:35 PM
 */

namespace Classes;
require_once('CONNECT.php');
class DISCOUNT
{
    public $link = null;
    public $response = array();

    function __construct()
    {
        $this->link = new CONNECT();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function addCoupon($coupon_code, $coupon_value, $generated_on,$expired_on, $coupon_status)
    {
        $link = $this->link->connect();
        if ($link) {
            $generated_on = strtotime($generated_on);
            $expired_on = strtotime($expired_on);
            $query = "insert into discount_coupon (coupon_code,coupon_value,generated_on,expired_on,coupon_status) 
            VALUES ('$coupon_code','$coupon_value','$generated_on','$expired_on','$coupon_status')";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "New Coupon Code Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function editCoupon($coupon_id,$coupon_code,$coupon_value,$generated_on,$expired_on,$coupon_status)
    {
        $link = $this->link->connect();
        if ($link) {
            $generated_on = strtotime($generated_on);
            $expired_on = strtotime($expired_on);
            $query = "update discount_coupon set coupon_code='$coupon_code',coupon_value='$coupon_value',
            generated_on='$generated_on',expired_on='$expired_on',coupon_status='$coupon_status' where coupon_id = '$coupon_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Coupon Updated SuccessFully";
                $this->response['planId'] = $coupon_id;
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function checkCouponExistence($coupon_code)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "select * from discount_coupon where coupon_code = '$coupon_code'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Coupon Code Already Registered Please Enter Different Code";
                    $row = mysqli_fetch_array($result);
                    $this->response['couponId'] = $row['coupon_id'];
                } else {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid Name";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularCouponData($coupon_id)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from discount_coupon where coupon_id='$coupon_id'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $couponData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Coupon Data Exist";
                    $this->response['couponData'] = $couponData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Coupon Identification";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getAllCoupons()
    {
        $couponArray = array();
        $link = $this->link->connect();
        if($link) {
            $query="select * from discount_coupon order by coupon_id DESC";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($couponData = mysqli_fetch_array($result)) {
                        $couponArray[]=array(
                            "coupon_id"=>$couponData['coupon_id'],
                            "coupon_code"=>$couponData['coupon_code'],
                            "coupon_value"=>$couponData['coupon_value'],
                            "generated_on"=>$couponData['generated_on'],
                            "expired_on"=>$couponData['expired_on'],
                            "coupon_status"=>$couponData['coupon_status']
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['data'] = $couponArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Discount Coupons Found";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function statusChange($coupon_id,$value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from discount_coupon where coupon_id='$coupon_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE discount_coupon SET coupon_status='$value' WHERE coupon_id='$coupon_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function deleteCoupon($coupon_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from discount_coupon where coupon_id='$coupon_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from discount_coupon WHERE coupon_id='$coupon_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Coupon Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}
