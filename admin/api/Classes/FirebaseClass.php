<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 5/29/2017
 * Time: 3:10 PM
 */

namespace Classes;
error_reporting(E_ALL);


class Firebase {

    public $response = array();

    // sending push message to single user by firebase reg id
    public function send($to, $message) {
        $fields = array(
            'to' => $to,
            'message' => $message,
        );
       return $this->sendPushNotification($fields) ;
    }

    // Sending message to a topic by topic name
    public function sendToTopic($to, $message) {
        $fields = array(
            'to' => '/topics/' . $to,
            'data' => $message,
        );
        return $this->sendPushNotification($fields);
    }

    // sending push message to multiple users by firebase registration ids
    public function sendMultiple($registration_ids, $message) {
        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => $message,

        );

        return $this->sendPushNotification($fields);
    }

    // function makes curl request to firebase servers
    private function sendPushNotification($fields) {


        // Set POST variables
//        $url = 'https://fcm.googleapis.com/fcm/send';
        $url = 'https://fcm.googleapis.com/fcm/send';
//        echo FIREBASE_API_KEY;
        $headers = array(
            'Authorization: key=' . FIREBASE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection

        $fields = json_encode ( $fields );
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch,CURLOPT_TIMEOUT,1000);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
//            die('Curl failed: ' . curl_error($ch));
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = 'Curl failed:';

        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = 'Message send successfully';
        $this->response['data'] = $result;

        // Close connection
        curl_close($ch);

        return $this->response;
    }

}