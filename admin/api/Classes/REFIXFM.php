<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 5/15/2017
 * Time: 5:38 PM
 */

namespace Classes;

namespace Classes;
require_once('CONNECT.php');
require_once('USERCLASS.php');
require_once ('PRODUCTS.php');
require_once ('ADVERT.php');

class REFIXFM
{
    public $link = null;
    public $userClass = null;
    public $response = array();
    public $prodClass = null;
    public $advertClass = null;

    function __construct()
    {
        $this->link = new CONNECT();
        $this->userClass = new USERCLASS();
        $this->prodClass = new PRODUCTS();
        $this->advertClass = new ADVERT();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }

    function getAllRefixFm($type) {
        $refixArray = array();
        $refix_query = "select * from refixfm,users where refixfm.fm_user_id = users.user_id and refixfm.fm_type='$type' order by fm_id desc";
        $link = $this->link->connect();
        if($link) {
            $result = mysqli_query($link,$refix_query);
            if($result) {
                $numRows = mysqli_num_rows($result);
                if($numRows>0) {
                  while($rows = mysqli_fetch_assoc($result)) {

                      $refData = $this->getParticularRefixFM($rows["fm_id"]);

                      $refixArray [] = array("fm_id"=>$rows["fm_id"],
                          "fm_image"=>$rows["fm_playlist_file"],
                          "fm_name"=>$rows["fm_playlist_name"],
                          "fm_desc"=>$rows["fm_playlist_desc"],
                          "fm_added_by"=>$rows["user_name"],
                          "fm_added_on"=>$rows["fm_added_on"],
                          "fm_file_type"=>$rows["fm_file_type"],
                          "fm_refix_list"=>$refData['refixData']['refData']);
                  }
                    $this->response[STATUS] = Success;
                    $this->response["refixData"] = $refixArray;
                    $this->response[MESSAGE] = "Data Found";

                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No RefixFM Found";
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = mysqli_error($link);
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = mysqli_error($link);
        }
        return $this->response;
    }

    function getParticularRefixFM($fm_id) {
        $refixArray = array();
        $refix_query = "select * from refixfm,users where refixfm.fm_user_id = users.user_id and refixfm.fm_id = '$fm_id' order by fm_id desc";
        $link = $this->link->connect();
        if($link) {
          $result = mysqli_query($link,$refix_query);
          if($result) {
              $rows = mysqli_fetch_array($result);
              $refixArray['play_name'] = $rows['fm_playlist_name'];
              $refixArray['play_desc'] = $rows['fm_playlist_desc'];
              $refixArray['play_file'] = $rows['fm_playlist_file'];
              $refixArray['play_user'] = $rows['user_name'];
              $refixArray['play_added_on'] = $rows['fm_added_on'];
              $refixArray['play_type'] = $rows['fm_type'];
              $refixArray['play_file_type'] = $rows['fm_file_type'];

              $refData = array();

              $add_query = "select * from refixfm_ref where ref_fm_id='$fm_id'";
              $result = mysqli_query($link,$add_query);
              if($result) {
                  while($row = mysqli_fetch_array($result)) {
                      $ref_type = $row['ref_type'];
                      if($ref_type == "video" || $ref_type == "music" || $ref_type == "books" ) {
                          $productData = $this->prodClass->getParticularProductData($row['ref_product_id']);

                          $refData[] =array("id"=>$row['ref_product_id'],
                              "name"=>$productData['ProductData']['product_name'],
                              'type'=>$ref_type,
                              "title_file"=>$productData['ProductData']['product_image'],
                              'file_name'=>$productData['ProductData']['file_name'],
                              'file_type'=>$productData['ProductData']['file_type'],
                              'artist_name'=>$productData['ProductData']['product_artist_name']);
                      }
                      else if($ref_type == "add") {
                          $addsData = $this->advertClass->getParticularAddvert($row['ref_product_id']);


                          $refData []= array("id"=>$row['ref_product_id'],
                              "name"=>$addsData['addData']['add_title'],
                              "title_file"=>$addsData['addData']['add_title_file'],
                              'type'=>'add',
                              "file_name"=>$addsData['addData']['add_file'],
                              "file_type"=>$addsData['addData']['add_filetype']

                          );
                      }
                  }
                  $refixArray['refData'] = $refData;

              }
              $this->response[STATUS] = Success;
              $this->response["refixData"] = $refixArray;
              $this->response[MESSAGE] = "Refix Data Found";

          }
          else{
              $this->response[STATUS] = Error;
              $this->response[MESSAGE] = mysqli_error($link);
          }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = mysqli_error($link);
        }
        return $this->response;
    }



    function getAllRefixFm1($type) {
        $refixArray = array();
        $refix_query = "select * from refixfm,users where refixfm.fm_user_id = users.user_id and refixfm.fm_type='$type' order by fm_id desc limit 0,1";
        $link = $this->link->connect();
        if($link) {
            $result = mysqli_query($link,$refix_query);
            if($result) {
                $numRows = mysqli_num_rows($result);
                if($numRows>0) {
                    while($rows = mysqli_fetch_assoc($result)) {

                        $refData = $this->getParticularRefixFM($rows["fm_id"]);
//                        print_r($refData) ;
                        $refixArray [] = array("fm_id"=>$rows["fm_id"],
                            "fm_image"=>$rows["fm_playlist_file"],
                            "fm_name"=>$rows["fm_playlist_name"],
                            "fm_desc"=>$rows["fm_playlist_desc"],
                            "fm_added_by"=>$rows["user_name"],
                            "fm_added_on"=>$rows["fm_added_on"],
                            "fm_file_type"=>$rows["fm_file_type"],
                            "fm_refix_list"=>$refData['refixData']['refData']);
                    }
                    $this->response[STATUS] = Success;
                    $this->response["refixData"] = $refixArray;
                    $this->response[MESSAGE] = "Data Found";

                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No RefixFM Found";
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = mysqli_error($link);
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = mysqli_error($link);
        }
        return $this->response;
    }

    function getParticularRefixFM1($fm_id) {
        $refixArray = array();
        $refix_query = "select * from refixfm,users where refixfm.fm_user_id = users.user_id and refixfm.fm_id = '$fm_id' order by fm_id desc";
        $link = $this->link->connect();
        if($link) {
            $result = mysqli_query($link,$refix_query);
            if($result) {
                $rows = mysqli_fetch_array($result);
                $refixArray['play_name'] = $rows['fm_playlist_name'];
                $refixArray['play_desc'] = $rows['fm_playlist_desc'];
                $refixArray['play_file'] = $rows['fm_playlist_file'];
                $refixArray['play_user'] = $rows['user_name'];
                $refixArray['play_added_on'] = $rows['fm_added_on'];
                $refixArray['play_type'] = $rows['fm_type'];

                $uploadData = array();
                $youtubeData = array();

                $add_query = "select * from refixfm_ref where ref_fm_id='$fm_id'";
                $result = mysqli_query($link,$add_query);
                if($result) {
                    while($row = mysqli_fetch_array($result)) {
                        $ref_type = $row['ref_type'];
                        if($ref_type == "video" ) {

                            $productData = $this->prodClass->getParticularProductData($row['ref_product_id']);
                            $uploadData[] =array("id"=>$row['ref_product_id'],
                                "name"=>$productData['ProductData']['product_name'],
                                'type'=>$ref_type,
                                "title_file"=>$productData['ProductData']['product_image'],
                                'file_name'=>$productData['ProductData']['file_name'],
                                'file_type'=>$productData['ProductData']['file_type'],
                                'artist_name'=>$productData['ProductData']['product_artist_name']);
                        }
                        else if($ref_type == "add") {
                            $addsData = $this->advertClass->getParticularAddvert($row['ref_product_id']);


                            $uploadData []= array("id"=>$row['ref_product_id'],
                                "name"=>$addsData['addData']['add_title'],
                                "title_file"=>$addsData['addData']['add_title_file'],
                                'type'=>'add',
                                "file_name"=>$addsData['addData']['add_file'],
                                "file_type"=>$addsData['addData']['add_filetype']

                            );
                        }
                    }
                    $refixArray['uploadData'] = $uploadData;
                    $refixArray['youtubeData'] = $youtubeData;


                }
                $this->response[STATUS] = Success;
                $this->response["refixData"] = $refixArray;
                $this->response[MESSAGE] = "Refix Data Found";

            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = mysqli_error($link);
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = mysqli_error($link);
        }
        return $this->response;
    }


    function addRefix1($admin_id,$playlist_name,$playlist_desc,$playlist_type,$main_list,$file_type) {
        $link = $this->link->connect();

        if($link) {
            if(!$this->playListExists($playlist_name)) {
                $image_response = $this->link->storeImage('playlist_file','images');

                if ($image_response[STATUS] == Error) {
                    $this->response[STATUS] = $image_response[STATUS];
                    $this->response[MESSAGE] = $image_response[MESSAGE];
                    return $this->response;
                }
                $image_file = $image_response['File_Name'];
                $query = "insert into refixfm (fm_playlist_name,fm_playlist_desc,fm_type,fm_user_id,fm_playlist_file,fm_file_type)values('$playlist_name','$playlist_desc','$playlist_type','$admin_id','$image_file','$file_type')";
                $result = mysqli_query($link, $query);
                if ($result) {
                    $fm_id = mysqli_insert_id($link);
                    $mainData = explode(',', $main_list);
                    if (count($mainData) > 0) {
                        $values = "values";
                        for ($i = 0; $i < count($mainData); $i++) {
                            $data_arr = explode(':',$mainData[$i]);
                            $data_id = $data_arr[0];
                            $data_type = $data_arr[1];

                            if ($i == count($mainData) - 1) {
                                $values = $values . "('" . $data_id . "','".$data_type."','','" . $fm_id . "')";
                            } else {
                                $values = $values . "('" . $data_id . "','".$data_type."','','" . $fm_id . "'),";
                            }
                        }
                        $fm_query = "insert into refixfm_ref(ref_product_id,ref_type,ref_file_type,ref_fm_id)" . $values;
                        $fm_result = mysqli_query($link, $fm_query);

                        if ($fm_result) {
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "RefixFM created successfully !!! ";

                        } else {
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = mysqli_error($link);

                        }
                    }
                    
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "Playlist already exists";
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = mysqli_error($link);
        }
        return $this->response;
    }


    function updateRefix1($admin_id,$playlist_name,$playlist_desc,$main_list,$fm_id,$file_changed) {
        $link = $this->link->connect();

        if($link) {

            $query = "";
            if($file_changed == "yes"){
                $image_response = $this->link->storeImage('playlist_file','images');

                if ($image_response[STATUS] == Error) {
                    $this->response[STATUS] = $image_response[STATUS];
                    $this->response[MESSAGE] = $image_response[MESSAGE];
                    return $this->response;
                }
                $image_file = $image_response['File_Name'];
                $query = "update  refixfm set  fm_playlist_name='$playlist_name',fm_playlist_desc='$playlist_desc',fm_user_id = '$admin_id',fm_playlist_file='$image_file' where fm_id='$fm_id'";

            }
            else{
                $query = "update  refixfm set  fm_playlist_name='$playlist_name',fm_playlist_desc='$playlist_desc',fm_user_id = '$admin_id' where fm_id='$fm_id'";

            }
            $result = mysqli_query($link, $query);
            $del_query = "delete from refixfm_ref where ref_fm_id='$fm_id'";
            mysqli_query($link,$del_query);

            if ($result) {
//                    $fm_id = mysqli_insert_id($link);
                $mainData = explode(',', $main_list);

                if (count($mainData) > 0) {
                    $values = "values";
                    for ($i = 0; $i < count($mainData); $i++) {
                        $data_arr = explode(':',$mainData[$i]);
                        $data_id = $data_arr[0];
                        $data_type = $data_arr[1];

                        if ($i == count($mainData) - 1) {
                            $values = $values . "('" . $data_id . "','".$data_type."','','" . $fm_id . "')";
                        } else {
                            $values = $values . "('" . $data_id . "','".$data_type."','','" . $fm_id . "'),";
                        }
                    }
                    $fm_query = "insert into refixfm_ref(ref_product_id,ref_type,ref_file_type,ref_fm_id)" . $values;
                    $fm_result = mysqli_query($link, $fm_query);

                    if ($fm_result) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "RefixFM updated successfully !!! ";

                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = mysqli_error($link);

                    }
                }


            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] ="error". $query;
            }

        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = mysqli_error($link);
        }
        return $this->response;
    }

    function deleteRefix($fm_id) {
        $link = $this->link->connect();

        if($link) {
           $del_fm = "delete from refixfm where fm_id='$fm_id'";
           $del_result = mysqli_query($link,$del_fm);
           if($del_result) {
               $del_fmref = "delete from refixfm_ref where ref_fm_id='$fm_id'";
               $del_result = mysqli_query($link,$del_fmref);
               if($del_result) {
                   $this->response[STATUS] = Success;
                   $this->response[MESSAGE] = "Data deleted successfully";
               }
               else{
                   $this->response[STATUS] = Error;
                   $this->response[MESSAGE] = mysqli_error($link);
               }
           }
           else{
               $this->response[STATUS] = Error;
               $this->response[MESSAGE] = mysqli_error($link);
           }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = mysqli_error($link);
        }
        return $this->response;
    }
    function playListExists($name) {
        $link = $this->link->connect();
        if($link) {
            $query = "select * from refixfm where fm_playlist_name = '$name'";
            $result = mysqli_query($link,$query);
            if($result) {
                $rows = mysqli_num_rows($result);
                if($rows>0) {
                    return true;
                }
            }

        }
        return false;
    }

}