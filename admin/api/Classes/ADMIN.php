<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/23/2017
 * Time: 3:46 PM
 */

namespace Classes;
require_once('CONNECT.php');
class ADMIN
{
    public $link = null;
    public $response = array();
    function __construct()
    {
        $this->link = new CONNECT();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
        session_start();
    }
    public function adminLogin($userName,$Password) {
        $link = $this->link->connect();
        if ($link){
            $stmt = $link->prepare("SELECT user_id,user_name,user_email,user_type FROM users WHERE user_email=? AND password=? AND (user_type='admin' or user_type='super') LIMIT 1");
            $stmt->bind_param('ss', $userName, $Password);
            $stmt->execute();
            $stmt->bind_result($user_id, $user_name, $user_email,$user_type);
            $stmt->store_result();
            if($stmt->num_rows == 1)  //To check if the row exists
            {
                if($stmt->fetch()) //fetching the contents of the row
                {
                    $this->response[STATUS]=Success;
                    $this->response[MESSAGE]="Login Success";
                    $_SESSION['SBAdminId']=$user_id;
                    $_SESSION['SBAdminEmail']=$user_email;
                    $_SESSION['SBAdminName']=$user_name;
                    $_SESSION['SBAdminType'] = $user_type;
                }
            }
            else
            {
                $this->response[STATUS]=Error;
                $this->response[MESSAGE]="Invalid Credentials";
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function getAdminEmail(){
        $link = $this->link->connect();
        if ($link){
            $query = "select * from admin_login";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $row = mysqli_fetch_array($result);
                    $adminEmail = $row['admin_email'];
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Email Found";
                    $this->response['admin_email'] = $adminEmail;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function changeAdminPassword($oldPassword,$newPassword,$adminId){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_id = '$adminId' and password='$oldPassword'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE users SET password='$newPassword' WHERE
                    password='$oldPassword' and user_id='$adminId'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Password Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Old Password";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function changeForgotPassword($newPassword,$admin_email){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_email = '$admin_email'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE users SET password='$newPassword' WHERE user_email='$admin_email'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Password Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Old Password";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function sendMaill($admin_email)
    {
        require 'SMTP/PHPMailerAutoload.php';
        $mail = new \PHPMailer();
        $mail->isSMTP();
        $mail->Host = 'md-in-68.webhostbox.net';
        $mail->SMTPAuth = true;
        $mail->Username = 'noreply@stsmentor.com';
        $mail->Password = 'noreply@sts';
        $mail->SMTPSecure = 'TLS';
        $mail->Port = 587;
        $mail->setFrom('noreply@stsmentor.com', 'Bolti Kitaab');   // sender
        $mail->addAddress($admin_email);
        //$mail->addCC('sbsunilbhatia9@gmail.com');				//to add cc
        $mail->addAttachment('');         // Add attachments
        $mail->addAttachment('');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'noreply@Bolti Kitaab';
        $mail->Body='<div style="width:100%;background:#ccc;padding-bottom:36px"><div style="width:80%;margin: 0 10%">
        <div style="background:#eee;border-bottom:1px solid #ccc;height:63px;padding:10px 0 0 10px"><img style="float:left;
        height:50px" src="" alt="Bolti Kitaab" />
        <p style="float: left;color:#666;font-size:15px;font-weight:bold;margin-left:10px">Hello ! Admin</p>
        </div><div style="background:#fff;margin-top:-15px;padding-left:69px;padding-bottom:35px">
        <p style="color:green;line-height:40px;font-weight:bold;font-size:15px">Greetings of the Day,</p>
        <p>Welcome To STS Mentor Family</p><p>Here Is The Change Password link . Click here to Change your Account Password.</p>
        <a style="text-decoration:none" href="'.MainServer.'/admin/api/admin_login.php?_='.$admin_email.'" >
        <input type="button" style="background:#d14130;border-radius:3px;color: white;font-weight:600;margin: 10px 0;
        padding: 10px 20px" value="Change Password" /></a>
        <div style="background:#eee;border:1px solid #ccc;height:63px;padding:0 0 10px 10px">
        <p style="font-weight:bold;color:#666">Thanks & Regards<br><br>Bolti Kitaab Team</p>
        </div></div></div>';
        $mail->AltBody = '';
        if(!$mail->send())
        {
            return $mail->ErrorInfo;
        }
        else {
            return "Email has been sent Successfully to your registered email address";
        }
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}
?>