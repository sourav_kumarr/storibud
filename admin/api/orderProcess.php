<?php
require_once('Classes/ORDERS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$orderClass = new \Classes\ORDERS();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $orderClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "addToCart"){
    $requiredfields = array('user_id','product_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $product_id = $_REQUEST['product_id'];
    $user_id = $_REQUEST['user_id'];
    $response = $orderClass->addToCart($product_id, $user_id);
    unset($response['cartArray']);
    $orderClass->apiResponse($response);
}
else if($type == "getCart"){
    $requiredfields = array('user_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $user_id = $_REQUEST['user_id'];
    $response = $orderClass->getCart($user_id);
    $orderClass->apiResponse($response);
}
else if($type == "clearCart"){
    $requiredfields = array('user_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $user_id = $_REQUEST['user_id'];
    $response = $orderClass->clearCart($user_id);
    $orderClass->apiResponse($response);
}
else if($type == "removeItemFromCart"){
    $requiredfields = array('cart_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $cart_id = $_REQUEST['cart_id'];
    $response = $orderClass->deleteItemFromCart($cart_id);
    $orderClass->apiResponse($response);
}
else if($type == "createOrder"){
    $requiredfields = array('json');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $json = $_REQUEST['json'];
    $response = $orderClass->createOrder($json);
    $orderClass->apiResponse($response);
}
elseif($type == "getOrders")
{
    $response = $orderClass->getAllOrders();
    $orderClass->apiResponse($response);
}
else if($type == "getOrder"){
    $requiredfields = array('order_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $order_id = $_REQUEST['order_id'];
    $response = $orderClass->getParticularOrderData($order_id);
    $orderClass->apiResponse($response);
}
else if($type == "updatePaymentStatus"){
    $requiredfields = array('order_id','transaction_number','payment_status');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $order_id = $_REQUEST['order_id'];
    $transaction_number = $_REQUEST['transaction_number'];
    $payment_status = $_REQUEST['payment_status'];
    $response = $orderClass->updatePaymentStatus($order_id,$transaction_number,$payment_status);
    if($response[STATUS]==Success){
        unset($response['order']);
        $response[MESSAGE] = "Payment Status Updated SuccessFully";
    }
    $orderClass->apiResponse($response);
}
/*else if($type == "cancelOrder")
{
    $requiredfields = array('order_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $order_id = $_REQUEST['order_id'];
    $response = $orderClass->cancelOrder($order_id);
    if($response[STATUS] == Error) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}*/
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $orderClass->apiResponse($response);
}
?>