<?php
require_once ('Classes/USERCLASS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$userClass = new \Classes\USERCLASS();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $userClass->apiResponse($response);
    return false;
}
$type = $_POST['type'];
if($type == "register")
{
    $requiredfields = array('username','email','registersource','fcm_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure') {
        $userClass->apiResponse($response);
        return false;
    }
    $registerSource = trim($_POST['registersource']);
    $fcm_id = trim($_POST['fcm_id']);

    if($registerSource == "email") {
        $requiredfields = array('password');
        $response = RequiredFields($_POST, $requiredfields);
        if($response['Status'] == 'Failure'){
            $userClass->apiResponse($response);
            return false;
        }
        $password=MD5($_POST['password']);
        $imageResponse = $userClass->link->storeImage('userImage','images');
        if($imageResponse[STATUS] == Error){
            $userClass->apiResponse($imageResponse);
            return false;
        }
        $file_name = $imageResponse['File_Name'];
    }
    else{
        $requiredfields = array('image_url');
        $response = RequiredFields($_POST, $requiredfields);
        if($response['Status'] == 'Failure'){
            $userClass->apiResponse($response);
            return false;
        }
        $file_name = $_POST['image_url'];
    }
    $username = trim($_POST['username']);
    $email = trim($_POST['email']);
    ($response = $userClass->checkEmailExistence($email));
    if($response[STATUS] == Success) {
        //new user
        $password = "";
        if($registerSource == "email"){
            $password=MD5($_POST['password']);
        }
        $response = $userClass->registerUser($username, $email,$password, $registerSource,$file_name,$fcm_id);
    }
    else{
        //old user
        $password = "";
        if($registerSource == "email"){
            $password=MD5($_POST['password']);
        }
        $userId = $response['userId'];
        $response = $userClass->updateUser($userId,$username, $email,$password, $registerSource,$file_name,$fcm_id);
    }
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $response['userId'];
    $temp = $userClass->getParticularUserData($userId);
    $response['userData'] = $temp['UserData'];
    $response[BaseURLKey] = BaseURL;
    unset($response['userId']);
    $userClass->apiResponse($response);
}
else if($type == "signIn") {
    $requiredfields = array('email','password','fcm_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $email = trim($_POST['email']);
    $password = MD5(trim($_POST['password']));
    $fcm_id = trim($_POST['fcm_id']);

    $response = $userClass->signIn($email,$password,$fcm_id);
    if($response[STATUS] == Error){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $response['userId'];
    $temp = $userClass->getParticularUserData($userId);
    $response['userData'] = $temp['UserData'];
    $response[BaseURLKey] = BaseURL;
    unset($response['userId']);
    $userClass->apiResponse($response);
}
else if($type == "getParticularUserData")
{
    $requiredfields = array('userId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $response = $userClass->getParticularUserData($userId);
    $userClass->apiResponse($response);
}
else if($type == "getAllUsersData")
{
    $response = $userClass->getAllUsersData();
    $userClass->apiResponse($response);
}
else if($type == "statusChange")
{
    $requiredfields = array('user_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $user_id = $_REQUEST['user_id'];
    $response = $userClass->statusChange($user_id,$value);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "deleteUser")
{
    $requiredfields = array('user_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $user_id = $_REQUEST['user_id'];
    $response = $userClass->deleteUser($user_id);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
///////////////////////////////////////////////////*/
/*else if($type == "updateUserInfo")
{
    $requiredfields = array('userId','token','userName','email','contactNumber','address');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $token = $_POST['token'];
    $tokenResponse = $userClass->link->isValidToken($userId,$token);
    if($tokenResponse[STATUS] == Error)
    {
        $userClass->apiResponse($tokenResponse);
        return false;
    }
    $userName = $_POST['userName'];
    $contactNumber = $_POST['contactNumber'];
    $email = $_POST['email'];
    $address = $_POST['address'];
    $token = $userClass->generateToken();
    $response = $userClass->updateUser($userId, $userName, $email, $contactNumber, $token, $address);
    if($response['Status'] == 'Failure')
    {
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $response['userId'];
    $temp = $userClass->getParticularUserData($userId);
    $response['UserData'] = $temp['UserData'];
    $response['ImagesBaseURL'] = ImagesBaseURL;
    unset($response['userId']);
    $userClass->apiResponse($response);
}*/
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $userClass->apiResponse($response);
}
?>