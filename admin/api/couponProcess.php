<?php
require_once('Classes/DISCOUNT.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$couponClass = new \Classes\DISCOUNT();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $couponClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "addCoupon")
{
    $requiredfields = array('coupon_code','coupon_value','generated_on','expired_on','coupon_status');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $couponClass->apiResponse($response);
        return false;
    }
    $coupon_code = trim($_POST['coupon_code']);
    $coupon_value = trim($_POST['coupon_value']);
    $expired_on = trim($_POST['expired_on']);
    $generated_on = trim($_POST['generated_on']);
    $coupon_status = trim($_POST['coupon_status']);
    ($response = $couponClass->checkCouponExistence($coupon_code));
    if($response[STATUS] == Success) {
        $response = $couponClass->addCoupon($coupon_code, $coupon_value, $generated_on,$expired_on, $coupon_status);
    }
    if($response[STATUS] == Error){
        $couponClass->apiResponse($response);
        return false;
    }
    $couponId = $response['couponId'];
    $temp = $couponClass->getParticularCouponData($couponId);
    $response['couponData'] = $temp['couponData'];
    unset($response['couponId']);
    $couponClass->apiResponse($response);
}
else if($type == "editCoupon")
{
    $requiredfields = array('coupon_id','coupon_code','coupon_value','generated_on','expired_on','coupon_status');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $couponClass->apiResponse($response);
        return false;
    }
    $coupon_id = trim($_POST['coupon_id']);
    $coupon_code = trim($_POST['coupon_code']);
    $coupon_value = trim($_POST['coupon_value']);
    $generated_on = trim($_POST['generated_on']);
    $expired_on = trim($_POST['expired_on']);
    $coupon_status = trim($_POST['coupon_status']);
    $response = $couponClass->editCoupon($coupon_id,$coupon_code,$coupon_value,$generated_on,$expired_on,$coupon_status);
    if($response[STATUS] == Error){
        $couponClass->apiResponse($response);
        return false;
    }
    $couponId = $response['couponId'];
    $temp = $couponClass->getParticularCouponData($couponId);
    $response['couponData'] = $temp['couponData'];
    unset($response['couponId']);
    $couponClass->apiResponse($response);
}
else if($type == "getCoupons")
{
    $response = $couponClass->getAllCoupons();
    $couponClass->apiResponse($response);
}
else if($type == "getCoupon"){
    $requiredfields = array('coupon_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $couponClass->apiResponse($response);
        return false;
    }
    $coupon_id = $_REQUEST['coupon_id'];
    $response = $couponClass->getParticularCouponData($coupon_id);
    $couponClass->apiResponse($response);
}
else if($type == "statusChange")
{
    $requiredfields = array('coupon_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $couponClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $coupon_id = $_REQUEST['coupon_id'];
    $response = $couponClass->statusChange($coupon_id,$value);
    if($response[STATUS] == Error) {
        $couponClass->apiResponse($response);
        return false;
    }
    $couponClass->apiResponse($response);
}
else if($type == "deleteCoupon")
{
    $requiredfields = array('coupon_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $couponClass->apiResponse($response);
        return false;
    }
    $coupon_id = $_REQUEST['coupon_id'];
    $response = $couponClass->deleteCoupon($coupon_id);
    if($response[STATUS] == Error) {
        $couponClass->apiResponse($response);
        return false;
    }
    $couponClass->apiResponse($response);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $couponClass->apiResponse($response);
}
?>