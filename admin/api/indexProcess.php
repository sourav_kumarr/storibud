<?php
require_once('Classes/PRODUCTS.php');
require_once('Classes/USERCLASS.php');
require_once('Classes/CATEGORY.php');
require_once('Classes/ORDERS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$productClass = new \Classes\PRODUCTS();
$usersClass = new \Classes\USERCLASS();
$catClass = new \Classes\CATEGORY();
$orderClass = new \Classes\ORDERS();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $productClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "getFrontPageData")
{
    $product_data = $productClass->totalproducts('Music');
    $MusicCount=$product_data['Count'];
    ////////////////////////////////////////
    $product_data = $productClass->totalproducts('Video');
    $VideoCount=$product_data['Count'];

    $product_data = $productClass->totalproducts('Art Work');
    $ArtworkCount=$product_data['Count'];

    $product_data = $productClass->totalproducts('Literature');
    $LiteratureCount=$product_data['Count'];

    $userData = $usersClass->countAllUsers();
    $Userscount=$userData['Count'];
    ////////////////////////////////////////
    /*$orderData = $orderClass->getAllOrders();
    $Ordercount=sizeof($orderData['order']);*/
    ////////////////////////////////////////

    $frontPageData=array(
        "userCount"=>$Userscount,
        "musicCount"=>$MusicCount,
        "videoCount"=>$VideoCount,
        "artCount"=>$ArtworkCount,
        "literatureCount"=>$LiteratureCount
        /*"orderCount"=>$Ordercount*/
    );
    $response[STATUS]=Success;
    $response[MESSAGE]="Data Found";
    $response['frontPageData']=$frontPageData;
    $productClass->apiResponse($response);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $productClass->apiResponse($response);
}
?>