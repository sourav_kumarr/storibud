<?php
require_once ('Classes/PRODUCTS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');


$productClass = new \Classes\PRODUCTS();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $productClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "addProduct") {
    $requiredfields = array('cat_id','prod_name','prod_desc','file_type','product_price','uploaded_by','user_id','admin_type','artist_name');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $productClass->apiResponse($response);
        return false;
    }
    $cat_id = trim($_POST['cat_id']);
    $prod_name = trim($_POST['prod_name']);
    $prod_desc = trim($_POST['prod_desc']);
    $file_type = trim($_POST['file_type']);
    $product_price = trim($_POST['product_price']);
    $uploaded_by = trim($_POST['uploaded_by']);
    $user_id = trim($_POST['user_id']);
    $user_type = trim($_POST['admin_type']);
    $artist_name = trim($_POST['artist_name']);
    $prod_file_type = '';
    $prod_file_name = '';
    if($file_type == 'Video') {
        $prod_file_type = trim($_POST['prod_file_type']);
        if($prod_file_type == 'prod_link') {
            $prod_file_name = trim($_POST['prod_file_name']);
        }
    }
    ($response = $productClass->checkProductExistence($prod_name,$cat_id));
    if($response[STATUS] == Success) {
        $response = $productClass->addProduct($cat_id,$prod_name,$prod_desc,$file_type,$product_price,$uploaded_by,$user_id,$user_type,$prod_file_type,$prod_file_name,$artist_name);
    }
    if($response[STATUS] == Error){
        $productClass->apiResponse($response);
        return false;
    }
    $productId = $response['productId'];
    $temp = $productClass->getParticularProductData($productId);
    $response['productData'] = $temp['productData'];
    $response[BaseURLKey] = BaseURL;
    unset($response['productId']);
    $productClass->apiResponse($response);
}
else if($type == "editProduct") {

    $requiredfields = array('prod_id','imageChanged','fileChanged','file_type','prod_name','prod_desc',
    'product_price','uploaded_by','user_id','artist_name');

    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $productClass->apiResponse($response);
        return false;
    }
    $prod_id = trim($_POST['prod_id']);
    $imageChanged = trim($_POST['imageChanged']);
    $fileChanged = trim($_POST['fileChanged']);
    $file_type = trim($_POST['file_type']);
    $prod_name = trim($_POST['prod_name']);
    $prod_desc = trim($_POST['prod_desc']);
    $file_type = trim($_POST['file_type']);
    $product_price = trim($_POST['product_price']);
    $uploaded_by = trim($_POST['uploaded_by']);
    $user_id = trim($_POST['user_id']);
    $user_type = trim($_POST['user_type']);
    $artist_name = trim($_POST['artist_name']);

    $prod_file_type = '';
    $prod_file_name = '';
    if($file_type == 'Video') {
        $prod_file_type = trim($_POST['prod_file_type']);
        if($prod_file_type == 'prod_link') {
            $prod_file_name = trim($_POST['prod_file_name']);
        }
    }
    $response = $productClass->editProduct($prod_id,$imageChanged,$fileChanged,$file_type,$prod_name,$prod_desc,$product_price,$uploaded_by,$user_id,$user_type,$prod_file_type,$prod_file_name,$artist_name);
    if($response[STATUS] == Error){
        $productClass->apiResponse($response);
        return false;
    }
    $productId = $response['productId'];
    $temp = $productClass->getParticularProductData($productId);
    $response['productData'] = $temp['productData'];
    $response[BaseURLKey] = BaseURL;
    unset($response['productId']);
    $productClass->apiResponse($response);
}
else if($type == "getProductsOfCategory") {
    $requiredfields = array('cat_id','user_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $productClass->apiResponse($response);
        return false;
    }
    $cat_id = trim($_POST['cat_id']);
    $user_id = trim($_POST['user_id']);

    $response = $productClass->getCategoryAllProducts($cat_id,$user_id);
    unset($response['commentData']);
    unset($response['Counter']);
    $productClass->apiResponse($response);
}

else if($type == "searchProducts") {
    $requiredfields = array('search_txt','user_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $productClass->apiResponse($response);
        return false;
    }
    $txt = trim($_POST['search_txt']);
    $user_id = trim($_POST['user_id']);

    $response = $productClass->searchProduct($txt,$user_id);

    unset($response['commentData']);
    unset($response['Counter']);
    $productClass->apiResponse($response);
}

else if($type == "getParticularProductData")
{
    $requiredfields = array('product_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $productClass->apiResponse($response);
        return false;
    }
    $product_id = trim($_POST['product_id']);

    $response = $productClass->getParticularProductData($product_id);
    unset($response['commentData']);
    unset($response['Counter']);
    $productClass->apiResponse($response);
}
else if($type == "deleteProduct")
{
    $requiredfields = array('product_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $productClass->apiResponse($response);
        return false;
    }
    $product_id = $_REQUEST['product_id'];
    $response = $productClass->deleteProduct($product_id);
    if($response[STATUS] == Error) {
        $productClass->apiResponse($response);
        return false;
    }
    $productClass->apiResponse($response);
}
else if($type == "deleteComment")
{
    $requiredfields = array('comment_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $productClass->apiResponse($response);
        return false;
    }
    $comment_id = $_POST['comment_id'];
    $response = $productClass->deleteComment($comment_id);
    if($response[STATUS] == Error) {
        $productClass->apiResponse($response);
        return false;
    }
    $productClass->apiResponse($response);
}
else if($type == "getComment")
{
    $requiredfields = array('product_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $productClass->apiResponse($response);
        return false;
    }
    $product_id = $_POST['product_id'];
    $response = $productClass->getComment($product_id);
    if($response[STATUS] == Error) {
        $productClass->apiResponse($response);
        return false;
    }
    $productClass->apiResponse($response);
}
else if($type == "addComment")
{
    $requiredfields = array('product_id','comment_text','user_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $productClass->apiResponse($response);
        return false;
    }
    $comment_text = $_POST['comment_text'];
    $product_id = $_POST['product_id'];
    $user_id = $_POST['user_id'];
    $response = $productClass->addComment($comment_text, $product_id, $user_id);
    if($response[STATUS] == Error) {
        $productClass->apiResponse($response);
        return false;
    }
    $productClass->apiResponse($response);
}
else if($type == "updateCounter")
{
    $requiredfields = array('user_id','product_id','counter_type');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $productClass->apiResponse($response);
        return false;
    }
    $user_id = $_POST['user_id'];
    $product_id = $_POST['product_id'];
    $counter_type = $_POST['counter_type'];
    $response = $productClass->counterUpdater($user_id,$product_id,$counter_type);
    if($response[STATUS] == Error) {

        $productClass->apiResponse($response);
        return false;
    }
    $productClass->apiResponse($response);
}
else if($type == "getPaymentStatus")
{
    $requiredfields = array('user_id','product_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $productClass->apiResponse($response);
        return false;
    }
    $user_id = $_POST['user_id'];
    $product_id = $_POST['product_id'];
    $response = $productClass->getPaymentStatus($product_id,$user_id);
    if($response[STATUS] == Error) {
        $productClass->apiResponse($response);
        return false;
    }
    $productClass->apiResponse($response);
}
else if($type == "getAllMusic")
{
    $response = $productClass->getAllMusic();
    $productClass->apiResponse($response);
}

else if($type == "getAllMusic1")
{
    $requiredfields = array('cat_type');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure') {
        $productClass->apiResponse($response);
        return false;
    }
    $cat_type = trim($_POST['cat_type']);
    $response = $productClass->getAllMusic1($cat_type);
    unset($response['commentData']);
    unset($response['Counter']);
    $productClass->apiResponse($response);
}
else if($type == "lastRecords") {
    $requiredfields = array('num_records');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure') {
        $productClass->apiResponse($response);
        return false;
    }
    $num_records = trim($_POST['num_records']);
    $response = $productClass->getLastProducts($num_records);
    unset($response['commentData']);
    unset($response['Counter']);
    $productClass->apiResponse($response);

}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $productClass->apiResponse($response);
}
?>