<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 5/15/2017
 * Time: 6:03 PM
 */

require_once ('Classes/CATEGORY.php');
require_once('Classes/PRODUCTS.php');
require_once('Classes/REFIXFM.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');

$catClass = new \Classes\CATEGORY();
$productClass = new \Classes\PRODUCTS();
$refixClass = new \Classes\REFIXFM();

$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $catClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];

 if($type == "getAllRefix" ) {
     $requiredfields = array('fm_type');
     $response = RequiredFields($_POST, $requiredfields);
     if($response['Status'] == 'Failure'){
         $catClass->apiResponse($response);
         return false;
     }
     $fm_type = trim($_POST['fm_type']);

    $response = $refixClass->getAllRefixFm($fm_type);
    $catClass->apiResponse($response);
 }

 else if($type == "getAllRefix1" ) {
    $requiredfields = array('fm_type');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $fm_type = trim($_POST['fm_type']);

    $response = $refixClass->getAllRefixFm1($fm_type);
    $catClass->apiResponse($response);
 }

 else if($type == "addNewAdvert") {
     $requiredfields = array('playlist_name','playlist_desc','admin_id','main_list','fm_type');

     $response = RequiredFields($_POST, $requiredfields);
     if($response['Status'] == 'Failure'){
         $catClass->apiResponse($response);
         return false;
     }
     $play_name = trim($_POST['playlist_name']);
     $play_desc = trim($_POST['playlist_desc']);
     $user_id = trim($_POST['admin_id']);
     $main_list = trim($_POST['main_list']);
     $fm_type = trim($_POST['fm_type']);
     $file_type = "";
     if(isset($_POST['file_type'])) {
         $file_type = $_POST['file_type'];
     }
//     $add_list = trim($_POST['add_list']);
     $response = $refixClass->addRefix1($user_id,$play_name,$play_desc,$fm_type,$main_list,$file_type);
     $catClass->apiResponse($response);
 }
 else if($type == "getParticularRefix" ) {
     $requiredfields = array('fm_id');

     $response = RequiredFields($_POST, $requiredfields);
     if($response['Status'] == 'Failure'){
         $catClass->apiResponse($response);
         return false;
     }
    $fm_id = trim($_POST['fm_id']);

    $response = $refixClass->getParticularRefixFM($fm_id);
    $catClass->apiResponse($response);
 }
 else if($type == "editRefix") {
     $requiredfields = array('playlist_name','playlist_desc','admin_id','main_list',"fm_id","file_changed");

     $response = RequiredFields($_POST, $requiredfields);
     if($response['Status'] == 'Failure'){
         $catClass->apiResponse($response);
         return false;
     }
     $play_name = trim($_POST['playlist_name']);
     $play_desc = trim($_POST['playlist_desc']);
     $user_id = trim($_POST['admin_id']);
     $main_list = trim($_POST['main_list']);
     $fm_id = trim($_POST['fm_id']);
     $file_changed = trim($_POST['file_changed']);

     $response = $refixClass->updateRefix1($user_id,$play_name,$play_desc,$main_list,$fm_id,$file_changed);
     $catClass->apiResponse($response);
 }

 else if($type == "deleteRefix") {

         $requiredfields = array('fm_id');

         $response = RequiredFields($_POST, $requiredfields);
         if($response['Status'] == 'Failure'){
             $catClass->apiResponse($response);
             return false;
         }
         $fm_id = trim($_POST['fm_id']);

         $response = $refixClass->deleteRefix($fm_id);
         $catClass->apiResponse($response);

  }
?>