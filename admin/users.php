<?php
include('header.php');
include('api/Classes/CONNECT.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
require_once('api/Classes/USERCLASS.php');
$userClass = new \Classes\USERCLASS();
?>
<!-- page content -->
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Users <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <!--<button onclick="window.location='api/excelProcess.php?dataType=allUsers'" class="btn btn-info btn-sm">Download Excel File</button>-->
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            View the Details of All Users
                        </p>
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Profile</th>
                                <th>User Name</th>
                                <th>E-Mail</th>
                                <th>Registration Source</th>
                                <th>Login Access</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                $userResponse = $userClass->getAllUsersData();
                                if($userResponse[STATUS] == Success) {
                                    $userData = $userResponse['UserData'];
                                    $j=0;
                                    for($i=0;$i<count($userData);$i++) {
                                        if($userData[$i]['user_type'] == "user") {
                                            $j = $j+1;
                                            ?>
                                            <tr>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <td data-title='Profile'>
                                                    <?php
                                                    if ($userData[$i]['user_profile'] == "") {
                                                        $userProfile = "api/Files/images/img.png";
                                                    } else {
                                                        if($userData[$i]['register_source'] == "email"){
                                                            $userProfile = "api/Files/images/".$userData[$i]['user_profile'];
                                                        }else{
                                                            $userProfile = $userData[$i]['user_profile'];
                                                        }
                                                    }
                                                    ?>
                                                    <img src='<?php echo $userProfile ?>' style='height:35px' class='img-thumbnail' />
                                                </td>
                                                <td data-title='User Name'><?php echo ucfirst($userData[$i]['user_name']) ?></td>
                                                <td data-title='Email'><?php echo $userData[$i]['user_email'] ?></td>
                                                <td data-title='Registered Through'><?php echo ucfirst($userData[$i]['register_source']) ?></td>
                                                <td class='buttonsTd' data-title='Login Access'>
                                                    <?php
                                                    $box = "";
                                                    if ($userData[$i]['user_status'] == "1") {
                                                        $box = "checked";
                                                    }
                                                    ?>
                                                    <input data-onstyle='info' class='userstatus' <?php echo $box ?>
                                                           data-toggle='toggle'
                                                           value="<?php echo $userData[$i]['user_status'] ?>"
                                                           id="<?php echo $userData[$i]['user_id'] ?>" data-size='mini'
                                                           data-style='ios'
                                                           type='checkbox'/>
                                                </td>
                                                <td data-title='Action'>
                                                    <i class='fa fa-trash'
                                                       onclick=deleteUser('<?php echo $userData[$i]['user_id'] ?>')
                                                       style='color:#D05E61;cursor: pointer'/>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    $(document).ready(function () {
        $('#bookTable').DataTable({});
    });
</script>