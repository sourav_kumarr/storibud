<?php
    include('header.php');
    $fm_id = $_REQUEST['_'];
    echo "<input type='hidden' value=".$fm_id." id='fm_id' />";
?>
<style>
    .boxes{
        background: white;
        min-height:100px;
        border:1px solid #ddd;
        margin-bottom: 10px;
    }
    .Review{
        box-shadow: 3px 3px 2px #ccc;
        margin-bottom: 20px;
    }
</style>
<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 style="cursor:pointer" onclick="back()"><i class="fa fa-arrow-circle-left"></i> All Refixes <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <!--<button onclick="window.location='api/excelProcess.php?dataType=particularBook&book_id=<?php /*echo $book_id */?>'" class="btn btn-info btn-sm">Download Excel File</button>-->
                        </li>
                        <!--<li>
                            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                <span></span> <b class="caret"></b>
                            </div>
                        </li>-->
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        View "<span id="product_name1">RefixFM </span>" Detail
                    </p>
                </div>
            </div>
            <div class="col-md-12 boxes">
                <h2 style="text-align: center" id="details">RefixFM Detail</h2>
                <hr>
                <div class="col-md-12" id="refix_data">
                    <div class="col-md-6" style="padding: 0">
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label id="name_title">RefixFM Name</label>
                                <p id="fm_name"></p>
                            </div>

                        </div>
                        <div class="form-group">
                            <label id="fm_desc_title" >RefixFM Description</label>
                            <p id="fm_desc"></p>
                        </div>
                        <div class="form-group">
                            <label id="fm_added_title">RefixFM Added By</label>
                            <p id="fm_user"></p>
                        </div>
                        <div class="form-group">
                            <label id="fm_on_title">RefixFM Added On</label>
                            <p id="fm_date"></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label id="fm_list_title">RefixFM Playlist</label>
                        <table id="refData" class="table table-bordered"></table>
                    </div>


                </div>

        </div>

    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>

<script>
    var fm_id = $("#fm_id").val();

    var url = "api/refixProcess.php";


    $.post(url,{'type':'getParticularRefix','fm_id':fm_id},function (data) {
       console.log('data --- '+JSON.stringify(data));
       var status = data.Status;
       var refix_data = data.refixData;
       var ref_data = refix_data.refData;
       var ref_table="<thead><tr><th>#</th><th>Name</th><th>Type</th></tr></thead><tbody>";
       if(status == "Success") {
           $("#fm_name").html(refix_data.play_name);
           $("#fm_desc").html(refix_data.play_desc);
           $("#fm_user").html(refix_data.play_user);
           $("#fm_date").html(refix_data.play_added_on);
           var play_type =  refix_data.play_type;
           if(play_type == "fm") {
               $("#details").html("RefixFM Detail");
               $("#product_name1").html("RefixFM");
               $("#name_title").html("RefixFM Name");
               $("#fm_desc_title").html("RefixFM Description");
               $("#fm_added_title").html("RefixFM Added By");
               $("#fm_on_title").html("RefixFM Added On");
               $("#fm_list_title").html("RefixFM Playlist");
           }
           else if(play_type == "tv") {
               $("#details").html("RefixTV Detail");
               $("#product_name1").html("RefixTV");
               $("#name_title").html("RefixTV Name");
               $("#fm_desc_title").html("RefixTV Description");
               $("#fm_added_title").html("RefixTV Added By");
               $("#fm_on_title").html("RefixTV Added On");
               $("#fm_list_title").html("RefixTV Playlist");

           }

           else if(play_type == "literature") {
               $("#details").html("RefixLiterature Detail");
               $("#product_name1").html("RefixLiterature");
               $("#name_title").html("RefixLiterature Name");
               $("#fm_desc_title").html("RefixLiterature Description");
               $("#fm_added_title").html("RefixLiterature Added By");
               $("#fm_on_title").html("RefixLiterature Added On");
               $("#fm_list_title").html("RefixLiterature Playlist");

           }
           for(var i=0;i<ref_data.length;i++) {
             ref_table = ref_table+"<tr><td>"+(i+1)+"</td><td>"+ref_data[i].name+"</td><td>"+ref_data[i].type+"</td></tr>";
           }
           $("#refData").html(ref_table+"</tbody>");
           $('#refData').DataTable({});
           $("#refData_paginate").css("margin-bottom","2%");

       }
       else if(status == "Failure") {
           $("#refix_data").html("RefixFM Data not found");

       }
    });

</script>