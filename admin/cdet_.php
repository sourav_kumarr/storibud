<?php

include('header.php');

include('api/Classes/CONNECT.php');

include('api/Constants/DbConfig.php');

include('api/Constants/configuration.php');

require_once('api/Classes/CATEGORY.php');

$conn = new \Classes\CONNECT();

$category = new \Classes\CATEGORY();

?>

<!-- page content -->


<!-- page content -->

<div id="overlay">

    <div id="progstat">....Please Wait....<br>Loading</div>

    <div id="progress"></div>

</div>

<div class="">

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">

                <div class="x_title">

                    <h2>All Categories <small></small></h2>

                    <ul class="nav navbar-right panel_toolbox"></ul>

                    <div class="clearfix"></div>

                </div>

                <div class="x_content">

                    <p class="text-muted font-13 m-b-30">

                        From here admin can manage/modify the content of the catalogs

                    </p>

                    <table id="catTable" class="table table-striped table-bordered display">

                        <thead>

                        <tr>

                            <th>#</th>

                            <th>Image</th>

                            <th>Category</th>

                            <th>Added On</th>

                            <th>Added By</th>

                            <th>Category Level</th>

                            <th>Approval Status</th>

                            <th>Action</th>

                        </tr>

                        </thead>

                        <tbody>
<?php

$response = $category->getAllCategories();
print_r($response);
?>
