<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 5/29/2017
 * Time: 3:57 PM
 */
require_once ('api/Classes/USERCLASS.php');
require_once ('api/Constants/functions.php');
require_once('api/Constants/configuration.php');
require_once('api/Constants/DbConfig.php');
require_once('api/Classes/FirebaseClass.php');
require_once('api/Classes/model/Push.php');
require_once('api/Classes/USERCLASS.php');

$response = array();
$push = new Push();
$firebase = new \Classes\Firebase();
$userClass = new \Classes\USERCLASS();

$push->setTitle("Storibud");
$push->setMessage("New Product added");
$push->setIsBackground(false);
$push->setPayload(array("product_name"=>'hello',"product_desc"=>'prod found',
    "product_image"=>'a.mp3',"product_price"=>'1',"file_type"=>'Music',
    "artist_name"=>'R.D Rehman'));

$json = $push->getPush();
$user_response = $userClass->getAllUsersData();
if($user_response[STATUS] == Success) {
    $user_data = $user_response['UserData'];
    $users = array();
    $counter = 0;
    for($i=0;$i<count($user_data);$i++) {
        $fcm_id = $user_data[$i]['user_fcm_id'];
        if($fcm_id!="") {
            $users[$counter] = $fcm_id;
            $counter++;
        }

    }

    if(count($users)>0) {
//        print_r($users);

        $firebase_response = $firebase->sendMultiple($users,$json);
        if($firebase_response[STATUS] == Error) {
            $response[STATUS] = Success;
            $response[MESSAGE] = "New Product Added SuccessFully !!!! unable to send notification";
            $response['productId'] = '1';
        }
        else if($firebase_response[STATUS] == Success) {
            $response[STATUS] = Success;
            $response[MESSAGE] = "New Product Added SuccessFully";
            $response['productId'] = '1';

        }
        echo '<br/>';
        print_r($firebase_response);
    }

}


?>