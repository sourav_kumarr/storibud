
<?php echo phpinfo();?>
<!-- page content -->
<html>
<head>
    <script src="js/jquery.min.js"></script>
    <script src = "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script src = "js/bootstrap.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">

</head>
<body>
<div class="container">
    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">

                <div class="x_title">

                    <h2>All Users <small></small></h2>

                    <ul class="nav navbar-right panel_toolbox">

                        <li>

                            <!--<button onclick="window.location='api/excelProcess.php?dataType=allUsers'" class="btn btn-info btn-sm">Download Excel File</button>-->

                        </li>

                    </ul>

                    <div class="clearfix"></div>

                </div>

                <div class="x_content">

                    <p class="text-muted font-13 m-b-30">

                        View the Details of All Users

                    </p>

                    <table id="datatable-buttons" class="table table-striped table-bordered" ng-app="mainApp" ng-controller ="userController">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Profile</th>
                            <th>User Name</th>
                            <th>E-Mail</th>
                            <th>Registration Source</th>
                            <th>Login Access</th>
                            <th>Action</th>

                        </tr>
                        <thead>
                        <tbody>
                        <tr ng-repeat="user in users" ng-show="isData">
                            <td>{{user.user_id}}</td>
                            <td>{{user.user_name}}</td>
                            <td>{{user.user_email}}</td>
                            <td>{{user.user_id}}</td>
                        </tr>

                        </tbody>
                    </table>

                </div>

            </div>

        </div>

    </div>

</div>



<!-- /page content -->


<script>


        var mainApp = angular.module("mainApp", []);
//        var xsrf = $.param({type: "getAllUsersData"});
        mainApp.controller("userController",["$http", "$scope",function($http,$scope) {
            var url = "api/userProcess.php";
            var xsrf = $.param({type: "getAllUsersData"});
            var req = {
                method: 'POST',
                url: url,
                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data:xsrf
            };

            $http(req).then(function (response) {
                // success function
                console.log('success -- ' + response.data);
                $scope.users = response.data.UserData;
//                $scope.users = [];
                if($scope.users.length>0) {
                    $scope.isData = true
                }
                else{
                    $scope.isData = false;
                }
//            $scope.users = response.data;
            }, function (response) {
                // Failure Function
                console.log('failure -- ' + JSON.stringify(response.data));
            });

        }]);
        $("#datatable-buttons").dataTable();


</script>
</body>
</html>

