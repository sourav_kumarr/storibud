<?php
include('header.php');

$cat_type = "";
$cat_id = "";
if(isset($_REQUEST)) {
    $cat_id = $_REQUEST['_'];
}
if(isset($_REQUEST['type'])) {
    $cat_type = $_REQUEST['type'];
}
echo "<input type='hidden' value=".$cat_id." id='cat_id' />";
echo "<input type='hidden' value=".$cat_type." id='cat_type' />";

?>
<style>
    .boxes{
        background: white;
        min-height:100px;
        border:1px solid #ddd;
        margin-bottom: 10px;
        padding-bottom: 10px;
    }
    .Review{
        box-shadow: 3px 3px 2px #ccc;
        margin-bottom: 20px;
    }

</style>
<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 style="cursor:pointer" onclick="back()"><i class="fa fa-arrow-circle-left"></i> Back <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li id="litrature_refix" style="display: none;"><a class="btn btn-info" onclick="viewRefixLiterature('<?php echo $cat_id ?>')" style="color: white"> <i id="btn_name">Refix Litrature</i></a></li>
                        <li id="refixfm" style="display: none;"><a class="btn btn-info" onclick="viewRefixFM('<?php echo $cat_id ?>')" style="color: white"> <i id="btn_name">Refix FM</i></a></li>
                        <li id="songbankbtn" style="display: none;"><a class="btn btn-info" onclick="viewSongBank('<?php echo $cat_id ?>')" style="color: white"> <i id="btn_name">Song Bank</i></a></li>
                        <li id="refixtv" style="display: none;"><a class="btn btn-info" onclick="viewRefixTV('<?php echo $cat_id ?>')" style="color: white"> <i id="btn_name">Refix TV</i></a></li>
                        <li id="videobankbtn" style="display: none;"><a class="btn btn-info" onclick="viewVideoBank('<?php echo $cat_id ?>')" style="color: white"> <i id="btn_name">Video Bank</i></a></li>
                        <li id="addcategorybtn"><a class="btn btn-info" onclick="addNewCategory('<?php echo $cat_id ?>')" style="color: white"><i class="fa fa-plus"></i> <i id="btn_name">Add New Sub-Category</i></a></li>
                        <li id="addproductbtn" style="display:none;"><a class="btn btn-info" onclick="addNewProduct('<?php echo $cat_id ?>')" style="color: white"><i class="fa fa-plus"></i> <i id="btn_name">Add New Product</i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        View "<span id="cat_name1"></span>" Detail
                    </p>
                </div>
            </div>
            <div class="col-md-8 boxes">
                <h2 style="text-align: center" id="mainDetail">Category Detail</h2>
                <hr>
                <div class="col-md-12">
                    <div class="col-md-1 form-group">
                        <label>Image</label>
                        <img id="cat_image" src="images/user.png" class="img-responsive img-thumbnail" />
                    </div>
                    <div class="col-md-2 form-group">
                        <label>Name</label>
                        <p id="cat_name"></p>
                    </div>
                    <div class="col-md-2 form-group">
                        <label>Created On</label>
                        <p id="added_on"></p>
                    </div>
                    <div class="col-md-2 form-group">
                        <label>Created By</label>
                        <p id="admin_name" style="text-transform: capitalize"></p>
                    </div>
                    <div class="col-md-2 form-group">
                        <label>Level</label>
                        <p id="level"></p>
                    </div>
                    <div class="col-md-2 form-group">
                        <label>Approval</label>
                        <p id="approval"></p>
                    </div>

                </div>
                <div class="col-md-12" >
                    <hr>
                    <h4 style="text-align:center;" id="subtype">All Genres</h4>
                    <table class="table table-stripped table-bordered" id="sub_cat_table"></table>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-12 boxes" id="approval_status">
                    <h2 style="text-align: center">Approval Status</h2>
                    <hr>
                    <div class="col-md-12" style="margin-bottom: 20px" id="approvalTable"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    var cat_id = $("#cat_id").val();
    var cat_type = $("#cat_type").val();

    var url = "api/categoryProcess.php";

    console.log('cat id -- '+cat_id+" cat type -- "+cat_type);


    if(cat_type == "songbank") {

        $(".loader").show();
        $("#addcategorybtn").css("display", "none");
        $("#addproductbtn").css("display", "block");
        $("#cat_name1").html("Song Bank");
        $("#mainDetail").html("Genre Detail");
        $("#cat_name").html("Song Bank");
        $("#admin_name").html("Admin (Super)");
        $("#approval").html("No Need To Approve");
        var addedon = "--";
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        $("#added_on").html(addedon);

        $("#level").html("Music");

        var productUrl = "api/productProcess.php";
        $("#subtype").html("All Products");
//        $("#approval_status").css("display","none");
        $.post(productUrl, {"type": "getAllMusic1","cat_type":"Music"}, function (data) {
            console.log('data here --- '+JSON.stringify(data));
            var status = data.Status;
            if(status == "Success") {
                var ProductData = data.ProductData;
                if (ProductData.length > 0) {
                    var product_table = "<thead><tr><th>#</th><th>Image</th><th>Name</th><th>Added On</th><th>Added By</th>" +
                        "<th>Price</th><th>Approval</th><th>Action</th></tr></thead><tbody>";
                    var j = 0;
                    for (var i = 0; i < ProductData.length; i++) {
                        var approvalbtns = "Approved";
                        var admin_type = ProductData[i].user_type;
                        if (admin_type != "super") {
                            for (var k = 0; k < ProductData[i].approval.length; k++) {
                                if (ProductData[i].approval[k].admin_id == $("#admin_id").val()) {
                                    if (ProductData[i].approval[k].approval_status == "1") {
                                        approvalbtns = "<button type='button' class='btn btn-sm btn-success' disabled >" +
                                            "<i class='fa fa-check'></i></button><button type='button' class='btn btn-sm btn-danger' " +
                                            "onclick=changeApproval('" + ProductData[i].approval[k].row_id + "','"+ProductData[i].product_id+"','0') ><i class='fa fa-close'>" +
                                            "</i></button>";
                                    } else {
                                        approvalbtns = "<button type='button' class='btn btn-sm btn-success' " +
                                            "onclick=changeApproval('" + ProductData[i].approval[k].row_id + "','"+ProductData[i].product_id+"','1')>" +
                                            "<i class='fa fa-check'></i></button><button type='button' disabled " +
                                            "class='btn btn-sm btn-danger'><i class='fa fa-close'></i></button>";
                                    }
                                }
                            }
                        }
                        else {
                            approvalbtns = "<button type='button' class='btn btn-sm btn-success' disabled>" +
                                "<i class='fa fa-check'></i></button><button type='button' disabled " +
                                "class='btn btn-sm btn-danger'><i class='fa fa-close'></i></button>";

                        }
                        var addedon = new Date(ProductData[i].added_on * 1000);
                        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                        addedon = addedon.getDate() + " " + months[addedon.getMonth()] + ", " + addedon.getFullYear();
                        product_table += "<tr><td>" + (i + 1) + "</td><td><img class='img-responsive img-thumbnail' " +
                            "style='height:50px;width: 50px' src='api/Files/images/" + ProductData[i].product_image + "'/></td>" +
                            "<td style='width: 26%'><a href='pdet.php?_=" + ProductData[i].product_id + "'>" + ProductData[i].product_name + "</a></td>" +
                            "<td>" + addedon + "</td><td>"  + ProductData[i].user_name+"</td><td>$" + ProductData[i].product_price + "</td><td class='buttonsTd' " +
                            "data-title='Approval'>" + approvalbtns + "</td><td data-title='Action'><i class='fa fa-edit'" +
                            " onclick=editProductData('" + ProductData[i].product_id + "') style='cursor:pointer;color:#31B0D5'></i>" +
                            "&nbsp;&nbsp;<i class='fa fa-trash' onclick=delete_product('" + ProductData[i].product_id + "') " +
                            "style='color:#D05E61;cursor:pointer'></i></td></tr>";
                    }
                }
                else {
                    product_table = "<tr><td colspan='6' style='text-align: center'>Not Data Available Yet</td></tr>";
                }
            }
            else{
                product_table = "<tr><td colspan='6' style='text-align: center'>Not Data Available Yet</td></tr>";
            }
            $(".loader").css("display","none");
         $("#approvalTable").html("<span>No need to approved</span>");
         $("#approvalTable").css("text-align","center");
        $("#sub_cat_table").html(product_table+'</tbody>');
            $("#sub_cat_table").dataTable({
                lengthMenu: [ [2, 4, 8, -1], [2, 4, 8, "All"] ],
                pageLength: 4
            });


        });

    }
    else if(cat_type == 'videobank') {
        $(".loader").show();
        $("#addcategorybtn").css("display", "none");
        $("#addproductbtn").css("display", "block");
        $("#cat_name1").html("Video Bank");
        $("#mainDetail").html("Genre Detail");
        $("#cat_name").html("Video Bank");
        $("#admin_name").html("Admin (Super)");
        $("#approval").html("No Need To Approve");
        var addedon = "--";
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        $("#added_on").html(addedon);

        $("#level").html("Video");

        var productUrl = "api/productProcess.php";
        $("#subtype").html("All Products");
//        $("#approval_status").css("display","none");
        $.post(productUrl, {"type": "getAllMusic1","cat_type":"Video"}, function (data) {
            console.log('data here --- '+JSON.stringify(data));
            var status = data.Status;
            if(status == "Success") {
                var ProductData = data.ProductData;
                if (ProductData.length > 0) {
                    var product_table = "<thead><tr><th>#</th><th>Image</th><th>Name</th><th>Added On</th><th>Added By</th>" +
                        "<th>Price</th><th>Approval</th><th>Action</th></tr></thead><tbody>";
                    var j = 0;
                    for (var i = 0; i < ProductData.length; i++) {
                        var approvalbtns = "Approved";
                        var admin_type = ProductData[i].user_type;
                        if (admin_type != "super") {
                            for (var k = 0; k < ProductData[i].approval.length; k++) {
                                if (ProductData[i].approval[k].admin_id == $("#admin_id").val()) {
                                    if (ProductData[i].approval[k].approval_status == "1") {
                                        approvalbtns = "<button type='button' class='btn btn-sm btn-success' disabled >" +
                                            "<i class='fa fa-check'></i></button><button type='button' class='btn btn-sm btn-danger' " +
                                            "onclick=changeApproval('" + ProductData[i].approval[k].row_id + "','"+ProductData[i].product_id+"','0') ><i class='fa fa-close'>" +
                                            "</i></button>";
                                    } else {
                                        approvalbtns = "<button type='button' class='btn btn-sm btn-success' " +
                                            "onclick=changeApproval('" + ProductData[i].approval[k].row_id + "','"+ProductData[i].product_id+"','1')>" +
                                            "<i class='fa fa-check'></i></button><button type='button' disabled " +
                                            "class='btn btn-sm btn-danger'><i class='fa fa-close'></i></button>";
                                    }
                                }
                            }
                        }
                        else {
                            approvalbtns = "<button type='button' class='btn btn-sm btn-success' disabled>" +
                                "<i class='fa fa-check'></i></button><button type='button' disabled " +
                                "class='btn btn-sm btn-danger'><i class='fa fa-close'></i></button>";

                        }
                        var addedon = new Date(ProductData[i].added_on * 1000);
                        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                        addedon = addedon.getDate() + " " + months[addedon.getMonth()] + ", " + addedon.getFullYear();
                        product_table += "<tr><td>" + (i + 1) + "</td><td><img class='img-responsive img-thumbnail' " +
                            "style='height:50px;width: 50px' src='api/Files/images/" + ProductData[i].product_image + "'/></td>" +
                            "<td style='width: 26%'><a href='pdet.php?_=" + ProductData[i].product_id + "'>" + ProductData[i].product_name + "</a></td>" +
                            "<td>" + addedon + "</td><td>"  + ProductData[i].user_name+"</td><td>$" + ProductData[i].product_price + "</td><td class='buttonsTd' " +
                            "data-title='Approval'>" + approvalbtns + "</td><td data-title='Action'><i class='fa fa-edit'" +
                            " onclick=editProductData('" + ProductData[i].product_id + "') style='cursor:pointer;color:#31B0D5'></i>" +
                            "&nbsp;&nbsp;<i class='fa fa-trash' onclick=delete_product('" + ProductData[i].product_id + "') " +
                            "style='color:#D05E61;cursor:pointer'></i></td></tr>";
                    }
                }
                else {
                    product_table = "<tr><td colspan='6' style='text-align: center'>Not Data Available Yet</td></tr>";
                }
            }
            else{
                product_table = "<tr><td colspan='6' style='text-align: center'>Not Data Available Yet</td></tr>";
            }
            $(".loader").css("display","none");
            $("#approvalTable").html("<span>No need to approved</span>");
            $("#approvalTable").css("text-align","center");
            $("#sub_cat_table").html(product_table+'</tbody>');
            $("#sub_cat_table").dataTable({
                lengthMenu: [ [2, 4, 8, -1], [2, 4, 8, "All"] ],
                pageLength: 4
            });

//            $(".loader").hide();

        });
    }

    else if(cat_type == 'refixfm') {
        $(".loader").show();
        var refix_url = "api/refixProcess.php";
        var addedon = "--";
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        $("#addcategorybtn").css("display", "none");
        $("#addproductbtn").css("display", "block");
        $("#cat_name1").html("Refix FM");
        $("#mainDetail").html("Genre Detail");
        $("#cat_name").html("Song Bank");
        $("#admin_name").html("Admin (Super)");
        $("#approval").html("No Need To Approve");

        $("#added_on").html(addedon);

        $("#level").html("Music");
        $("#subtype").html("All RefixFm");

        $.post(refix_url,{"type":"getAllRefix","fm_type":"fm"},function (data) {
//            $(".loader").hide();
            $(".loader").css("display","none");
           console.log("data ----- "+JSON.stringify(data));
            var status = data.Status;
            var product_table = "";

            if(status == "Success") {
              var refixData = data.refixData;
              if(refixData.length>0) {
                  var product_table = "<thead><tr><th>#</th><th>Image</th><th>Name</th><th>Description</th><th>Added By</th><th>Added On</th>" +
                      "<th>Action</th></tr></thead><tbody>";
                   for(var i=0;i<refixData.length;i++) {
                       product_table = product_table+"<tr><td>"+(i+1)+"</td><td><img src='api/Files/images/"+refixData[i].fm_image+"' style='width: 30px;height: 30px'/></td><td><a href='fmdet.php?_="+refixData[i].fm_id+"'>"+refixData[i].fm_name+"</a></td><td>"+
                           refixData[i].fm_desc+"</td><td>"+refixData[i].fm_added_by+"</td><td>"+refixData[i].fm_added_on+"</td><td data-title='Action'><i class='fa fa-edit'" +
                           " style='cursor:pointer;color:#31B0D5' onclick=editRefixFM('"+refixData[i].fm_id+"')></i>&nbsp;&nbsp;<i class='fa fa-trash' style='color:#D05E61;cursor:pointer' onclick=deleteRefixFM('"+refixData[i].fm_id+"')></i></td></tr>";
                   }

              }
              else{
                  product_table = "<tr><td colspan='6' style='text-align: center'>Not Data Available Yet</td></tr>";
              }
            }
            else if(status == "Failure"){
                product_table = "<tr><td colspan='6' style='text-align: center'>Not Data Available Yet</td></tr>";

            }
//            $(".loader").hide();
            $(".loader").css("display","none");
            $("#approvalTable").html("<span>No need to approved</span>");
            $("#approvalTable").css("text-align","center");
            $("#sub_cat_table").html(product_table+'</tbody>');
            $("#sub_cat_table").dataTable({
                lengthMenu: [ [2, 4, 8, -1], [2, 4, 8, "All"] ],
                pageLength: 4
            });
        });
    }
    else if(cat_type == 'refixtv') {
//        alert('refix tv clicked ---- ');
        $(".loader").show();
        var refix_url = "api/refixProcess.php";
        var addedon = "--";
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        $("#addcategorybtn").css("display", "none");
        $("#addproductbtn").css("display", "block");
        $("#cat_name1").html("Refix TV");
        $("#mainDetail").html("Genre Detail");
        $("#cat_name").html("Video Bank");
        $("#admin_name").html("Admin (Super)");
        $("#approval").html("No Need To Approve");

        $("#added_on").html(addedon);

        $("#level").html("Video");
        $("#subtype").html("All RefixTV");

        $.post(refix_url,{"type":"getAllRefix","fm_type":"tv"},function (data) {
//            $(".loader").hide();
            $(".loader").css("display","none");
            console.log("data ----- "+JSON.stringify(data));
            var status = data.Status;
            var product_table = "";

            if(status == "Success") {
                var refixData = data.refixData;
                if(refixData.length>0) {
                    var product_table = "<thead><tr><th>#</th> <th>Image</th><th>Name</th><th>Description</th><th>Added By</th><th>Added On</th>" +
                        "<th>Action</th></tr></thead><tbody>";

                    for(var i=0;i<refixData.length;i++) {
                        product_table = product_table+"<tr><td>"+(i+1)+"</td><td><img src='api/Files/images/"+refixData[i].fm_image+"' style='height: 30px;width:30px'/></td><td><a href='fmdet.php?_="+refixData[i].fm_id+"'>"+refixData[i].fm_name+"</a></td><td>"+
                            refixData[i].fm_desc+"</td><td>"+refixData[i].fm_added_by+"</td><td>"+refixData[i].fm_added_on+"</td><td data-title='Action'><i class='fa fa-edit'" +
                            " style='cursor:pointer;color:#31B0D5' onclick=editRefixTV('"+refixData[i].fm_id+"')></i>&nbsp;&nbsp;<i class='fa fa-trash' style='color:#D05E61;cursor:pointer' onclick=deleteRefixFM('"+refixData[i].fm_id+"')></i></td></tr>";
                    }

                }
                else{
                    product_table = "<tr><td colspan='6' style='text-align: center'>Not Data Available Yet</td></tr>";
                }
            }
            else if(status == "Failure"){
                product_table = "<tr><td colspan='6' style='text-align: center'>Not Data Available Yet</td></tr>";

            }
//            $(".loader").hide();
            $(".loader").css("display","none");
            $("#approvalTable").html("<span>No need to approved</span>");
            $("#approvalTable").css("text-align","center");
            $("#sub_cat_table").html(product_table+'</tbody>');
            $("#sub_cat_table").dataTable({
                lengthMenu: [ [2, 4, 8, -1], [2, 4, 8, "All"] ],
                pageLength: 4

            });

        });
    }

    else if(cat_type == 'refixlit') {
//        alert('refix tv clicked ---- ');
        $(".loader").show();
        var refix_url = "api/refixProcess.php";
        var addedon = "--";
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        $("#addcategorybtn").css("display", "none");
        $("#addproductbtn").css("display", "block");
        $("#cat_name1").html("Refix Literature");
        $("#mainDetail").html("Genre Detail");
        $("#cat_name").html("Literature");
        $("#admin_name").html("Admin (Super)");
        $("#approval").html("No Need To Approve");

        $("#added_on").html(addedon);

        $("#level").html("Literature");
        $("#subtype").html("All Refix Literature");

        $.post(refix_url,{"type":"getAllRefix","fm_type":"literature"},function (data) {
//            $(".loader").hide();
            $(".loader").css("display","none");
            console.log("data ----- "+JSON.stringify(data));
            var status = data.Status;
            var product_table = "";

            if(status == "Success") {
                var refixData = data.refixData;
                if(refixData.length>0) {
                    var product_table = "<thead><tr><th>#</th> <th>Image</th><th>Name</th><th>Description</th><th>Added By</th><th>Added On</th>" +
                        "<th>Action</th></tr></thead><tbody>";

                    for(var i=0;i<refixData.length;i++) {
                        product_table = product_table+"<tr><td>"+(i+1)+"</td><td><img src='api/Files/images/"+refixData[i].fm_image+"' style='height: 30px;width:30px'/></td><td><a href='fmdet.php?_="+refixData[i].fm_id+"'>"+refixData[i].fm_name+"</a></td><td>"+
                            refixData[i].fm_desc+"</td><td>"+refixData[i].fm_added_by+"</td><td>"+refixData[i].fm_added_on+"</td><td data-title='Action'><i class='fa fa-edit'" +
                            " style='cursor:pointer;color:#31B0D5' onclick=editRefixLitrature('"+refixData[i].fm_id+"')></i>&nbsp;&nbsp;<i class='fa fa-trash' style='color:#D05E61;cursor:pointer' onclick=deleteRefixFM('"+refixData[i].fm_id+"')></i></td></tr>";
                    }

                }
                else{
                    product_table = "<tr><td colspan='6' style='text-align: center'>Not Data Available Yet</td></tr>";
                }
            }
            else if(status == "Failure"){
                product_table = "<tr><td colspan='6' style='text-align: center'>Not Data Available Yet</td></tr>";

            }
//            $(".loader").hide();
            $(".loader").css("display","none");
            $("#approvalTable").html("<span>No need to approved</span>");
            $("#approvalTable").css("text-align","center");
            $("#sub_cat_table").html(product_table+'</tbody>');
            $("#sub_cat_table").dataTable({
                lengthMenu: [ [2, 4, 8, -1], [2, 4, 8, "All"] ],
                pageLength: 4
            });

        });
    }
    else {
        $(".loader").show();
        $("#approval_status").css("display","block");
        $.post(url, {"type": "getCategory", "cat_id": cat_id}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                var catData = data.catData;
                console.log('cat name --- '+JSON.stringify(data));

                var cat_name = catData.cat_name;
                 if(cat_name == "Music") {
                     $("#refixfm").css("display","block");
                     $("#songbankbtn").css("display","block");
                     $("#refixtv").css("display","none");
                     $("#videobankbtn").css("display","none");
                     $("#litrature_refix").css("display","none");
                 }
                 else if(cat_name == "Video") {
                     $("#refixfm").css("display","none");
                     $("#songbankbtn").css("display","none");
                     $("#refixtv").css("display","block");
                     $("#videobankbtn").css("display","block");
                     $("#litrature_refix").css("display","none");
                 }

                 else if(catData.cat_type == "Sub Category" || cat_name == "Literature") {
                     $("#refixfm").css("display","none");
                     $("#songbankbtn").css("display","none");
                     $("#refixtv").css("display","none");
                     $("#videobankbtn").css("display","none");
                     $("#litrature_refix").css("display","block");
                 }

                $("#cat_name1").html(catData.cat_name);
                $("#cat_name").html(catData.cat_name);
                $("#admin_name").html(catData.admin_name);
                var addedon = new Date(catData.added_on * 1000);
                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                $("#added_on").html(addedon.getDate() + " " + months[addedon.getMonth()] + ", " + addedon.getFullYear());
                if (catData.cat_image != "") {
                    $("#cat_image").attr("src", "api/Files/images/" + catData.cat_image);
                }
                $("#level").html(catData.cat_type);
                var approvalTable = "<table class='table table-bordered'><tr><td>S.No</td><td>Admin Name</td>" +
                    "<td>Approval Status</td></tr>";
//            if(catData.user_type != "super") {
                for (var i = 0; i < catData.approval.length; i++) {
                    if (catData.user_type == "admin" || catData.user_type == "user") {
                        if (catData.approval[i].admin_id == $("#admin_id").val()) {
                            if (catData.approval[i].approval_status == "1") {
                                $("#approval").html("<td class='buttonsTd' data-title='Approval'><button type='button' " +
                                    "class='btn btn-success' disabled ><i class='fa fa-check'></i></button>" +
                                    "<button type='button' class='btn btn-danger' onclick=changeApproval('" + catData.approval[i].row_id + "','"+catData.cat_id+"','0') ><i class='fa fa-close'></i></button></td>")
                            }
                            else {
                                $("#approval").html("<td class='buttonsTd' data-title='Visibility'><button type='button' " +
                                    "class='btn btn-success' onclick=changeApproval('" + catData.approval[i].row_id + "','"+catData.cat_id+"','1')><i class='fa fa-check'></i></button>" +
                                    "<button type='button' class='btn btn-danger' disabled><i class='fa fa-close'></i></button></td>")
                            }
                        }
                    }
                    else {
                        $("#approval").html("<td class='buttonsTd' data-title='Visibility'><button type='button' " +
                            "class='btn btn-success' disabled><i class='fa fa-check'></i></button>" +
                            "<button type='button' class='btn btn-danger' disabled><i class='fa fa-close'></i></button></td>")

                    }


                    var textStatus = "<i class='fa fa-close' style='color:red'></i> Declined";
                    if (catData.approval[i].approval_status == "1") {
                        textStatus = "<i class='fa fa-check' style='color:green'></i> Approved";
                    }
                    approvalTable += "<tr><td class='buttonsTd' data-title='S.No'>" + (i + 1) + "</td><td class='buttonsTd' " +
                        "data-title='Admin Name'>" + catData.approval[i].admin_name.charAt(0).toUpperCase() +
                        catData.approval[i].admin_name.slice(1) + "</td><td class='buttonsTd' " +
                        "data-title='Approval Status'>" + textStatus + "</td></tr>";
                }
            }
            approvalTable += "</table>";
            $("#approvalTable").html(approvalTable);
//        }
        });
        $.post(url, {"type": "isChildExist", "cat_id": cat_id}, function (data) {

            var status = data.Status;
            if (status == "Success") {
//                $("#songbankbtn").css("display", "block");
//                $("#refixfm").css("display", "block");

                $.post(url, {"type": "getCategories","parentId":cat_id}, function (data) {
//                    console.log("cats --- " + JSON.stringify(data));
                    var status = data.Status;
                    if (status == "Success") {

                        var catArray = data.data;
                        var sub_cat_table = "<thead><tr><th>#</th><th>Image</th><th>Name</th><th>Added On</th><th>Added By</th>" +
                            "<th>Type</th><th>Approval</th><th>Action</th></tr></thead><tbody>";
                        var j = 0;
                        for (var i = 0; i < catArray.length; i++) {
//                            if (catArray[i].parent_id == cat_id) {
                                j++;
                                var typee = "";

                                if (catArray[i].cat_type == "Sub Category") {
                                    typee = "Sub Category";
                                    $("#subtype").html("All Sub Categories");
                                }
                                else {
                                    typee = "Genre";

                                }
                                var approvalbtns = "approved";
                                var admin_type = $("#admin_type").val();

                                /*  if(admin_type == "super") {
                                 approvalbtns ="<button type='button' class='btn btn-success' disabled >" +
                                 "<i class='fa fa-check'></i></button><button type='button' class='btn btn-danger' " +
                                 "disabled><i class='fa fa-close'>" +
                                 "</i></button>";
                                 }*/
                                if (catArray[i].user_type == "super") {
                                    approvalbtns = "<button type='button' class='btn btn-sm btn-success' disabled >" +
                                        "<i class='fa fa-check'></i></button><button type='button' class='btn btn-danger' " +
                                        "disabled><i class='fa fa-close'>" +
                                        "</i></button>";

                                }
                                else {
                                    for (var k = 0; k < catArray[i].approval.length; k++) {

                                        if (catArray[i].approval[k].admin_id == $("#admin_id").val()) {
//                                        alert(catArray[i].approval[k].admin_type);
//                                        if (catArray[i].approval[k].admin_type =="admin" || catArray[i].approval[k].admin_type =="user"){
                                            if (catArray[i].approval[k].approval_status == "1") {
                                                approvalbtns = "<button type='button' class='btn btn-sm btn-success' disabled >" +
                                                    "<i class='fa fa-check'></i></button><button type='button' class='btn btn-danger' " +
                                                    "onclick=changeApproval('" + catArray[i].approval[k].row_id + "','"+catArray[i].cat_id+"','0') ><i class='fa fa-close'>" +
                                                    "</i></button>";
                                            }

                                            else {
                                                approvalbtns = "<button type='button' class='btn btn-sm btn-success' " +
                                                    "onclick=changeApproval('" + catArray[i].approval[k].row_id + "','"+catArray[i].cat_id+"','1')>" +
                                                    "<i class='fa fa-check'></i></button><button type='button' disabled " +
                                                    "class='btn btn-danger'><i class='fa fa-close'></i></button>";
                                            }
//                                        }


                                        }

                                    }
                                }

                                var addedon = new Date(catArray[i].added_on * 1000);
                                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                                addedon = addedon.getDate() + " " + months[addedon.getMonth()] + ", " + addedon.getFullYear();
                                sub_cat_table += "<tr><td>" + j + "</td><td><img class='img-responsive img-thumbnail' " +
                                    "style='max-height:35px' src='api/Files/images/" + catArray[i].cat_image + "'/></td>" +
                                    "<td><a href='cdet.php?_=" + catArray[i].cat_id + "'>" + catArray[i].cat_name + "</a></td>" +
                                    "<td>" + addedon + "</td><td>" + catArray[i].admin_name.charAt(0).toUpperCase() +
                                    catArray[i].admin_name.slice(1) + "</td><td>" + typee + "</td><td class='buttonsTd' " +
                                    "data-title='Approval'>" + approvalbtns + "</td><td data-title='Action'><i class='fa fa-edit'" +
                                    " onclick=editCategoryData('" + catArray[i].cat_id + "') style='cursor:pointer;color:#31B0D5'></i>" +
                                    "&nbsp;&nbsp;<i class='fa fa-trash' onclick=deleteCategory('" + catArray[i].cat_id + "') " +
                                    "style='color:#D05E61;cursor:pointer'></i></td></tr>";
//                            }
                        }
                        if (catArray.length > 0) {
                            $("#sub_cat_table").html(sub_cat_table+'</tbody>');
                            $("#sub_cat_table").dataTable({
                                lengthMenu: [ [2, 4, 8, -1], [2, 4, 8, "All"] ],
                                pageLength: 4
                            });
                        } else {
                            $("#sub_cat_table").html("<tr><td colspan='6' style='text-align: center'>Not Any Sub-Category" +
                                " Available Yet</td></tr>");
                        }
                    }
//                    $(".loader").hide();
                    $(".loader").css("display","none");
                });
            } else {
                $("#subtype").html("All Products");
                $("#mainDetail").html("Genre Detail");
                $("#addcategorybtn").css("display", "none");
                $("#addproductbtn").css("display", "block");

                $("#songbankbtn").css("display", "none");
                $("#refixfm").css("display", "none");
                $("#videobankbtn").css("display", "none");
                $("#refixtv").css("display", "none");

                var productUrl = "api/productProcess.php";

                $.post(productUrl, {"type": "getProductsOfCategory", "cat_id": cat_id,"user_id":$("#admin_id").val()}, function (data) {
                    var status = data.Status;
                    if (status == "Success") {
                        var ProductData = data.ProductData;
                        var product_table = "<thead><tr><th>#</th><th>Image</th><th>Name</th><th>Added On</th><th>Added By</th>" +
                            "<th>Price</th><th>Approval</th><th>Action</th></tr></thead><tbody>";
                        var j = 0;
                        for (var i = 0; i < ProductData.length; i++) {
                            var approvalbtns = "";
                            var admin_type = ProductData[i].user_type;
                            if (admin_type != "super") {
                                for (var k = 0; k < ProductData[i].approval.length; k++) {
                                    if (ProductData[i].approval[k].admin_id == $("#admin_id").val()) {
                                        if (ProductData[i].approval[k].approval_status == "1") {
                                            approvalbtns = "<button type='button' class='btn btn-sm btn-success' disabled >" +
                                                "<i class='fa fa-check'></i></button><button type='button' class='btn btn-danger' " +
                                                "onclick=changeApproval('" + ProductData[i].approval[k].row_id + "','"+ProductData[i].product_id+"','0') ><i class='fa fa-close'>" +
                                                "</i></button>";
                                        } else {
                                            approvalbtns = "<button type='button' class='btn btn-sm btn-success' " +
                                                "onclick=changeApproval('" + ProductData[i].approval[k].row_id + "','"+ProductData[i].product_id+"','1')>" +
                                                "<i class='fa fa-check'></i></button><button type='button' disabled " +
                                                "class='btn btn-danger'><i class='fa fa-close'></i></button>";
                                        }
                                    }
                                }
                            }
                            else {
                                approvalbtns = "<button type='button' class='btn btn-sm btn-success' disabled>" +
                                    "<i class='fa fa-check'></i></button><button type='button' disabled " +
                                    "class='btn btn-danger'><i class='fa fa-close'></i></button>";

                            }
                            var addedon = new Date(ProductData[i].added_on * 1000);
                            var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                            addedon = addedon.getDate() + " " + months[addedon.getMonth()] + ", " + addedon.getFullYear();
                            product_table += "<tr><td>" + (i + 1) + "</td><td><img class='img-responsive img-thumbnail' " +
                                "style='max-height:35px' src='api/Files/images/" + ProductData[i].product_image + "'/></td>" +
                                "<td><a href='pdet.php?_=" + ProductData[i].product_id + "'>" + ProductData[i].product_name + "</a></td>" +
                                "<td>" + addedon + "</td><td>" + ProductData[i].uploaded_by.charAt(0).toUpperCase() + ProductData[i].uploaded_by.slice(1) + " ( " + ProductData[i].user_name.charAt(0).toUpperCase() +
                                ProductData[i].user_name.slice(1) + " )</td><td>$" + ProductData[i].product_price + "</td><td class='buttonsTd' " +
                                "data-title='Approval'>" + approvalbtns + "</td><td data-title='Action'><i class='fa fa-edit'" +
                                " onclick=editProductData('" + ProductData[i].product_id + "') style='cursor:pointer;color:#31B0D5'></i>" +
                                "&nbsp;&nbsp;<i class='fa fa-trash' onclick=delete_product('" + ProductData[i].product_id + "') " +
                                "style='color:#D05E61;cursor:pointer'></i></td></tr>";
                        }
                    }
                    else {
                        product_table = "<tr><td colspan='6' style='text-align: center'>Not Data Available Yet</td></tr>";
                    }
                    $(".loader").css("display","none");

                    $("#sub_cat_table").html(product_table+'</tbody>');
                    $("#sub_cat_table").dataTable({
                        lengthMenu: [ [2, 4, 8, -1], [2, 4, 8, "All"] ],
                        pageLength: 4
                    });
//                    $(".loader").hide();

                });
            }
        });
    }

</script>
