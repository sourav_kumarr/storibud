<?php
include('header.php');
include('api/Classes/CONNECT.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
require_once('api/Classes/ADVERT.php');
$advertClass = new \Classes\ADVERT();

?>

<!-- page content -->
<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>All Adverts <small></small></h2>
                    <button class="pull-right btn btn-danger" onclick="onAdvertAdd();"><i class="fa fa-plus"></i>&nbsp;&nbsp; Add Addvert</button>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <!--<button onclick="window.location='api/excelProcess.php?dataType=allUsers'" class="btn btn-info btn-sm">Download Excel File</button>-->
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        View the Details of All Advertisement
                    </p>
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Image</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Type</th>
                            <th>Added By</th>
                            <th>Added On</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $advertResponse = $advertClass->getAllAddvert();
//                        print_r($advertResponse);
                        if($advertResponse[STATUS] == Success) {
                            $addData = $advertResponse["addData"];
                            if(count($addData)>0) {
//                                echo "count -- ".count($addData);
                                  for($i=0;$i<count($addData);$i++) {
                                    ?>
                                      <tr><td><?php echo ($i+1);?></td>
                                          <td><?php echo '<img src="api/Files/images/'.$addData[$i]["add_image"].'" style="width:50px;height:50px;border:1px gray solid " />';?></td>
                                          <td><?php echo $addData[$i]['add_title']?></td>
                                          <td><?php if($addData[$i]['add_desc'] == ''){ echo "Description goes here";}else{ echo $addData[$i]['add_desc'];}?></td>
                                          <td style="text-align: center">
                                              <?php if($addData[$i]['add_filetype'] == 'audio')
                                              {
                                                  echo "<i class='fa fa-play fa-2x' style='color: #c9302c'></i>";
                                              }
                                              else if($addData[$i]['add_filetype'] == 'video') {
                                                  echo "<i class='fa fa-video-camera fa-2x' style='color: #c9302c'></i>";
                                              }
                                              else if($addData[$i]['add_filetype'] == 'books'){
                                                  echo "<i class='fa fa-book fa-2x' style='color: #c9302c'></i>";
                                              }
                                              else{
                                                      echo '<img src="api/Files/images/'.$addData[$i]['add_file'].'" style="width:30px;height:30px">';
                                                  }
                                              ?></td>
                                          <td><?php echo$addData[$i]['add_user']?></td>
                                          <td><?php echo$addData[$i]['add_added_on']?></td>
                                          <td style="text-align: center"><i class="fa fa-2x fa-trash" style="color: #c9302c;cursor:pointer" onclick="confirmDelete('<?php echo $addData[$i]['add_id']?>')"></i></td></tr>
                                     <?php
                                  }
                            }
                            else{
                                ?>
                                <tr><td>No Advert Found</td></tr>
                                <?php
                            }
                        }
                        else{
                            ?>
                            <tr><td>No Advert Found</td></tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    $(document).ready(function () {
        $('#bookTable').DataTable({});
    });

    function onAdvertAdd() {

        $(".modal-header").css({"display": "block"});
        $(".modal-title").html("Add New Addvert");
        $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
            "<div class='col-md-12' style='margin: 0px;padding:0px'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
            "</label></div> <div class='col-md-6' style='margin: 0px;padding:0px;margin-top: 10px;border-right: 1px solid;padding-right: 16px;padding-bottom: 5px'><input type='text' placeholder='enter add title' id='add_title' style='width:100%;margin-right: 10px' class='col-md-6'/>" +
            "<textarea type='text' class='col-md-6' id='add_desc' placeholder='enter add description' style='width: 100%;margin-right: 5px;height: 90px;margin-top: 5px;resize: none' /> " +
            "</div> <div class='col-md-6' style='margin: 0px;padding: 0px'><label style='margin-left: 8px;margin-top: 7px'>Select Add File</label></div><input type='file' id='upload_file' class='col-md-6'/> " +
            "<div class='col-md-6' style='margin-top: 5px'><label>Select Title File</label></div> <div class='col-md-6' ><img src='api/Files/images/img.png' style='width:57px;height: 51px;border: 2px gray solid' onclick='onTitleClicked()' id='title'/><input type='file' id='title_file' style='display: none'/>" +
            "</div><div class='col-md-12' style='text-align: right;margin-top: 10px'><img src='images/default.gif' id='loader' style='width: 30px;height: 30px;display: none'/><button class='btn btn-sm btn-danger' onclick='addNewAddvert()'>Submit</button><button class='btn btn-sm btn-default' data-dismiss='modal' >Cancel</button></div>");

        $(".modal-footer").css({"display": "none"});
        $(".modal-body").css("height","230px");

        $("#myModal").modal("show");
    }

    function addNewAddvert() {

//        alert('addNewAdvert is called---');
        var add_title = $("#add_title").val();
        var add_desc = $("#add_desc").val();
        var file_selected = $("#upload_file").val();
        var title_file = $('#title_file').val();

        if(add_title == "") {
            $(".error").html("Add title must not be empty");
            return;
        }
        else if(add_desc == "") {
            $(".error").html("Add description must not be empty");
             return;
        }
        else if(file_selected == "" || title_file == "") {
            $(".error").html("Please enter all fields");
            return;
        }

        else{
            var img =["png","jpg","gif","jpeg"];
            var audio =  ["mp3","wav","amr"];
            var video = ["mp4","mov","wmv","flv","avi","3gp"];
            var pdf = ["pdf"];

            var ext = file_selected.substr(file_selected.lastIndexOf(".")+1,file_selected.length);
            var ext1 = title_file.substr(title_file.lastIndexOf(".")+1,title_file.length);

            var file_type = "";

            if(!isItemExists(ext1,img)) {
                $(".error").html("Please select valid title file as jpg,jpeg,gif or png");
                return false;
            }
            if(isItemExists(ext,img)) {
                file_type = "image";
            }
            else if(isItemExists(ext,audio)){
                file_type = "audio";
            }
            else if(isItemExists(ext,video)) {
                file_type = "video";
            }
            else if(isItemExists(ext,pdf)) {
                file_type = "books";
            }
            else{
                $(".error").html("Please select audio , image or pdf file");
                return false;

            }
            var img_file = document.getElementById('upload_file');
            var title_file = document.getElementById('title_file');
            $("#loader").css("display","block");
            var data = new FormData();
            data.append("add_title",add_title);
            data.append("add_desc",add_desc);
            data.append("add_type",file_type);
            data.append("upload_file",img_file.files[0]);
            data.append("title_file",title_file.files[0]);
            data.append("type","addNewAddvert");
            data.append("admin_id",$("#admin_id").val());

            var request = new XMLHttpRequest();
            request.onreadystatechange = function() {
                $("#loader").css("display","none");
                if(request.readyState == 4){
                    console.log(request.response);
                    var response = $.parseJSON(request.response);
                    var status = response.Status;
                    if(status == "Success")
                    {
                        $("#myModal").modal("hide");
                        location.reload(true);
                    }
                    else
                    {
                        $("#message").html(response.Message);
//                        $(".loadNow").css({"display": "none"});
                        return false;
                    }
                }
            };
            request.open('POST', 'api/advertProcess.php');
            request.send(data);

        }

    }

    function confirmDelete(id) {
        $(".modal-header").css({"display": "block"});
        $(".modal-title").html("Delete Addvert");
        $(".modal-body").html("<p id='message' style='text-align: left;font-size: 14px;color: red'>Would you like to delete advert (y/n)?</p>" +
            "<div class='col-md-12' style='text-align: right;margin-top: 10px;font-weight:bold '><button class='btn btn-sm btn-danger' onclick=deleteAdvert('"+id+"')>Delete</button>" +
            "<button class='btn btn-sm btn-default' data-dismiss='modal' >Cancel</button></div>");
        $(".modal-footer").css({"display": "none"});
        $(".modal-body").css("height","100px");
        $("#myModal").modal("show");

    }

    function deleteAdvert(id) {
        $("#myModal").modal("hide");
        $.post('api/advertProcess.php',{'type':'deleteAdvert','add_id':id},function (data) {
         var status = data.Status;

         if(status == "Success") {
             window.location.reload();
         }
         else if(status == "Failure") {
             setTimeout(function () {
                 $(".modal-header").css({"display": "block"});
                 $(".modal-title").html("Delete Addvert");
                 $(".modal-body").html("<p id='message' style='text-align: left;font-size: 14px;color: red'>"+data.Message+"</p>" +
                     "<button class='btn btn-sm btn-default' data-dismiss='modal' >Cancel</button></div>");
                 $(".modal-footer").css({"display": "none"});
                 $(".modal-body").css("height","100px");
                 $("#myModal").modal("show");
             },1000);


         }
      });
    }

    function isItemExists(item,arr) {
        for(var i=0;i<arr.length;i++) {
            if(arr[i] == item) {
                return true;
            }
        }
    }

    function onTitleClicked() {
        $("#title_file").click();

        $('#title_file').change(function (event) {

            var selectedFile = event.target.files[0];
            var reader = new FileReader();

            var imgtag = document.getElementById("title");
            imgtag.title = selectedFile.name;

            reader.onload = function(event) {
                imgtag.src = event.target.result;
            };
            reader.readAsDataURL(selectedFile);
        });
    }

</script>