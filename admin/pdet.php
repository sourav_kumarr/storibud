
<?php
    include('header.php');
    $product_id = $_REQUEST['_'];
    echo "<input type='hidden' value=".$product_id." id='product_id' />";
?>
<style>
    .boxes{
        background: white;
        min-height:100px;
        border:1px solid #ddd;
        margin-bottom: 10px;
    }
    .Review{
        box-shadow: 3px 3px 2px #ccc;
        margin-bottom: 20px;
    }
</style>
<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 style="cursor:pointer" onclick="back()"><i class="fa fa-arrow-circle-left"></i> All Products <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <!--<button onclick="window.location='api/excelProcess.php?dataType=particularBook&book_id=<?php /*echo $book_id */?>'" class="btn btn-info btn-sm">Download Excel File</button>-->
                        </li>
                        <!--<li>
                            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                <span></span> <b class="caret"></b>
                            </div>
                        </li>-->
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        View "<span id="product_name1"></span>" Detail
                    </p>
                </div>
            </div>
            <div class="col-md-8 boxes">
                <h2 style="text-align: center">Book Detail</h2>
                <hr>
                <div class="col-md-12">
                    <div class="col-md-8" style="padding: 0">
                        <div class="row">
                            <div class="col-md-8 form-group">
                                <label>Product Name</label>
                                <p id="product_name"></p>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Approval Status</label>
                                <p id="approval"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Product Category</label>
                            <p id="product_category"></p>
                        </div>
                        <div class="form-group">
                            <label>Product Price</label>
                            <p id="product_price"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <img id="product_image" style="max-height:157px" src="images/user.png" class="img-responsive img-thumbnail" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Product Description</label>
                        <p id="product_desc"></p>
                    </div>
                    <br>
                    <div class="clearfix"></div>
                    <div class="col-md-4 row">
                        <div class="form-group">
                            <label>Product File</label>
                            <p id="product_file"></p>
                        </div>
                    </div>
                    <div class="col-md-3 row">
                        <div class="form-group">
                            <label>File Type</label>
                            <p id="file_type"></p>
                        </div>
                    </div>
                    <div class="col-md-3 row">
                        <div class="form-group">
                            <label>Added By</label>
                            <p id="added_by"></p>
                        </div>
                    </div>
                    <div class="col-md-2 row">
                        <div class="form-group">
                            <label>Added On</label>
                            <p id="added_on"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-bottom: 30px">
                    <br>
                    <div class="col-md-4 row">
                        <div class="form-group">
                            <label>Likes</label>
                            <p id="likes"></p>
                        </div>
                    </div>
                    <div class="col-md-3 row">
                        <div class="form-group">
                            <label>Dislikes</label>
                            <p id="dislikes"></p>
                        </div>
                    </div>
                    <div class="col-md-3 row">
                        <div class="form-group">
                            <label>Play Time</label>
                            <p id="play_time"></p>
                        </div>
                    </div>
                    <div class="col-md-2 row">
                        <div class="form-group">
                            <label>Downloads</label>
                            <p id="downloads"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-12 boxes">
                    <h2 style="text-align: center">Approval Status</h2>
                    <hr>
                    <div class="col-md-12" style="margin-bottom: 20px" id="approvalTable"></div>

                </div>
                <div class="col-md-12 boxes" id="userReviews" style="height: 207px;overflow: scroll;overflow-x: hidden"></div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    var product_id = $("#product_id").val();
    var url = "api/productProcess.php";
    $.post(url,{"type":"getParticularProductData","product_id":product_id} ,function (data) {
        var status = data.Status;
        if (status == "Success") {
            var ProductData = data.ProductData;
            $("#product_name1").html(ProductData.product_name);
            $("#product_name").html(ProductData.product_name);
            $("#product_category").html(ProductData.category_name);
            $("#product_price").html("$"+ProductData.product_price);
            $("#product_desc").html(ProductData.product_desc);
            if(ProductData.file_type == "Music") {
                $("#product_file").html(ProductData.file_name + "( <i onclick=playSong('" + ProductData.file_name + "') class='fa fa-play' style='color:green;cursor:pointer'></i> )");
                $("#file_type").html("<i class='fa fa-music' style='color:green'></i> Music");
            }
            else if(ProductData.file_type == "Video") {
                $("#product_file").html(ProductData.file_name + "( <i onclick=playVideoSong('" + ProductData.file_name + "','"+ProductData.product_image+"') class='fa fa-play' style='color:green;cursor:pointer'></i> )");
                $("#file_type").html("<i class='fa fa-video-camera' style='color:green'></i> Video");
            }
            else if(ProductData.file_type == "Art Work") {
                $("#product_file").html(ProductData.file_name + "(<a href='api/Files/art/"+ProductData.file_name+"'>" +
                " <i class='fa fa-download' style='color:green;cursor:pointer'></i></a> )");
                $("#file_type").html("<i class='fa fa-file' style='color:green'></i> "+ProductData.file_type);
            }
            else if(ProductData.file_type == "Literature") {
                $("#product_file").html(ProductData.file_name + "( <a href='api/Files/literature/"+ProductData.file_name+"'>" +
                " <i class='fa fa-download' style='color:green;cursor:pointer'></i></a> )");
                $("#file_type").html("<i class='fa fa-file' style='color:green'></i> "+ProductData.file_type);
            }
            if (ProductData.product_image != "") {
                $("#product_image").attr("src", "api/Files/images/"+ProductData.product_image);
            }
            var addedon = new Date(ProductData.added_on*1000);
            var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
            addedon = addedon.getDate()+" "+months[addedon.getMonth()]+", "+addedon.getFullYear();
            $("#added_on").html(addedon);
            $("#added_by").html(ProductData.user_name.charAt(0).toUpperCase()+ProductData.user_name.slice(1)
            +" ( "+ProductData.uploaded_by.charAt(0).toUpperCase()+ProductData.uploaded_by.slice(1)+" )");
            $("#likes").html("<i class='fa fa-thumbs-up' style='color:green'></i> "+ProductData.likes);
            $("#dislikes").html("<i class='fa fa-thumbs-down' style='color:red'></i> "+ProductData.dislikes);
            $("#downloads").html("<i class='fa fa-download' style='color:green'></i> "+ProductData.downloads);
            $("#play_time").html("<i class='fa fa-clock-o' style='color:green'></i> "+ProductData.playtime);
            var approvalTable = "<table class='table table-bordered'><tr><td>S.No</td><td>Admin Name</td>" +
            "<td>Approval Status</td></tr>";

            for(var i=0;i<ProductData.approval.length;i++) {
                if(ProductData.user_type !="super") {
                    if (ProductData.approval[i].admin_id == $("#admin_id").val()) {
                        if (ProductData.approval[i].approval_status == "1") {
                            $("#approval").html("<td class='buttonsTd' data-title='Approval'><button type='button' " +
                                "class='btn btn-success' disabled ><i class='fa fa-check'></i></button>" +
                                "<button type='button' class='btn btn-danger' onclick=changeApproval('" + ProductData.approval[i].row_id + "','"+ProductData.product_id+"','0') ><i class='fa fa-close'></i></button></td>")
                        }
                        else {
                            $("#approval").html("<td class='buttonsTd' data-title='Visibility'><button type='button' " +
                                "class='btn btn-success' onclick=changeApproval('" + ProductData.approval[i].row_id + "','"+ProductData.product_id+"','1')><i class='fa fa-check'></i></button>" +
                                "<button type='button' class='btn btn-danger' disabled><i class='fa fa-close'></i></button></td>")
                        }
                    }
                }
                else{
                    $("#approval").html("<td class='buttonsTd' data-title='Visibility'><button type='button' " +
                        "class='btn btn-success' disabled><i class='fa fa-check'></i></button>" +
                        "<button type='button' class='btn btn-danger' disabled><i class='fa fa-close'></i></button></td>");

                }
                var textStatus = "<i class='fa fa-close' style='color:red'></i> Declined";
                if(ProductData.approval[i].approval_status == "1"){
                    textStatus = "<i class='fa fa-check' style='color:green'></i> Approved";
                }
                approvalTable += "<tr><td class='buttonsTd' data-title='S.No'>"+(i+1)+"</td><td class='buttonsTd' " +
                "data-title='Admin Name'>"+ProductData.approval[i].admin_name.charAt(0).toUpperCase()+
                ProductData.approval[i].admin_name.slice(1)+"</td><td class='buttonsTd' " +
                "data-title='Approval Status'>"+textStatus+"</td></tr>";
            }
            approvalTable += "</table>";
            $("#approvalTable").html(approvalTable);

            var commentData = ProductData.comments;
            var commentDataDiv = "<h2 style='text-align: center'>User Comments</h2><hr>";
            if(commentData.length>0) {
                for (var i = 0; i < commentData.length; i++) {
                    var userImage = "";
                    if (commentData[i].user_profile == "") {
                        userImage = "images/img.png";
                    } else {
                        userImage = "api/Files/images/" + commentData[i].user_profile;
                    }
                    commentDataDiv += "<div class='col-md-12 Review'><div class='col-md-2'><img src='" + userImage + "' " +
                        "class='img-responsive img-circle' /></div><div class='col-md-10'><label>" + commentData[i].user_name.toUpperCase() + "</label>" +
                        "<p>" + commentData[i].comment_text + "...</p></div></div>";
                }
            }else{
                commentDataDiv+="<p style='text-align: center'><label>No Comments Available Yet</label></p>";
            }
            $("#userReviews").html(commentDataDiv);
        }
    });
</script>