<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin || Storibud</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">

    <link href="http://vjs.zencdn.net/5.19.2/video-js.css" rel="stylesheet">
    <script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
    <style>
        .content{
            /*width:70%;*/

        }
        .ol-padding{
            padding-left: 12px;
            margin-top: 17px;
        }
        .ol-padding>ol li{
            word-wrap: break-word;
        }
        .ol-padding>ol ul {
            word-wrap: break-word;
        }

        .ol-padding>ol ul li{
            word-wrap: break-word;
        }
        .term-heading{
            color:red;
            font-size:20px ;
            padding-bottom: 5px;
            text-align: center;
        }
        .tchild.term-heading {
            font-size:15px ;
        }
    </style>

</head>

<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 5/31/2017
 * Time: 7:42 PM
 */
  include ('header1.php');
  ?>
   <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12" >
       <ul class="list-unstyled">
           <li class="term-heading">Term of Service</li>
           <li class="tchild term-heading ">Privacy Policy</li>
           <li class="tchild term-heading">Most Guarantee Term and Condition</li>
       </ul>
   </div>

   <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12" >

       <h2 class="col-md-12 col-xs-10 col-sm-10 col-lg-12" style="padding: 0px">Terms of Service</h2>
   </div>
     <div class="col-md-12 col-xs-10 col-sm-10 col-lg-12 content" style="width: 100%">

       This agreement covers the products and services you license (or purchase) from STORIBUD, unless and until we enter into a new agreement that expressly replaces this one. If you use the Tibet Innovations products and services as an employee of or for the benefit of your company, you represent that you have the power and authority to accept this agreement on behalf of your company. Your company will be the licensee under this agreement. By clicking on the "Accept" button of this agreement, or by downloading, installing or using the Storibudproducts or services, you consent to the terms and conditions of this agreement on behalf of yourself and the company on whose behalf you will use the Tibet Innovations products and services provided under this agreement. The effective date of this agreement is the date that you first download, install or use the Tibet Innovations products or services. If you do not agree to the terms and conditions of this agreement or if you do not have the power and authority to accept the terms and conditions of this agreement on behalf of your company, you may not use the Tibet Innovations products and services and Tibet Innovations is unwilling to provide you with them.
       All Section references in these Terms and Conditions shall be references to provisions in these Terms and Conditions unless explicitly stated otherwise.
     </div>
    <div class="col-md-12 col-xs-9 col-sm-9 col-lg-12" style="width: 100%">

            <ol class="ol-padding">
                <li>
                    <label>DEFINITIONS. As used in the Agreement, these terms shall have the following definitions:<label>
                </li>
                <ul>
                    <li>“Agreement” means collectively (i) these Terms and Conditions, and (ii) the Software LicenseAgreement.</li>

                    <li>“Effective Date” means the date indicated as the Effective Date in Section 1 of the SoftwareLicense Agreement.</li>

                    <li>“Licensed Software” means the software identified as the Licensed Software in theSoftware License Agreement, including all Updates and Upgrades provided in accordance with Section2.7 below</li>

                    <li>“Licensee” means the party identified as the Licensee the Software LicenseAgreement.</li>

                    <li>“Licensee Products” means those software products of Licensee, if any, that incorporate theLicensed Software.

                     <li>“Software License Agreement” means the particular Software License Agreement to which theseTerms and Conditions are attached and incorporated into by reference.</li>

                      <li>“Updates” and “Upgrades” have the meanings given in Section 2.7 below.</li>
                    </ul>
                <li><label>Products and Services<label></li>
                <ul>
                    <li><label>The Storibudproducts and services that you may license or purchase under this agreement may include (but are not limited to):<label></label>
                            <li>	STORIBUD</li>
                            <li>	Licensed Software, which means a Storibudsoftware product as specified above.</li>
                            <li>	Maintenance Services, which means the software maintenance and support services described below.</li>
                    <li style="margin-top: 10px">
                        Additional terms and conditions (beyond those stated here and in any attached supplements) may apply to certain customized Licensed Productsdepending on the features demanded and length of time to deliver. When ordering these Licensed Products, you will be given an opportunity to review the additional terms and conditions that must be accepted for you to receive the Licensed Products. If the additional terms and conditions conflict in any way with the terms and conditions stated here, the additional terms and conditions will prevail.
	Licensor may use third party subcontractors in providing the Implementation and Professional Services. Licensor shall be responsible for the performance of such subcontractors.
                    </li>
                </ul>
                <li><label>LICENSE RIGHTS AND LIMITATIONS</label></li>
                <ul style="list-style: none">
                    <li>
                        <label> 3.1 Internal Use License.</label> Subject to the terms and conditions of the Agreement and Licensee’s compliance therewith, Licensor hereby grants to Licensee a non-exclusive, non-transferable, license to use the Software solely for the internal business purposes of Licensee.</li>
                    <li style="margin-top: 10px;"><label>3.2 Redistribution Rights. </label>Only if the Software License Agreement indicates that Licensee has Redistribution Rights, then, subject to the terms and conditions of the Agreement and Licensee’s compliance therewith, Licensor hereby grants to Licensee a non-exclusive, non-transferable, worldwide license to distribute through multiple tiers of distribution a number of copies of the object code version of the Licensed Software indicated in the Software License Agreement solely as incorporated in the Licensee Products. IN NO CIRCUMSTANCES MAY MVID-CODE OR DOCUMENTATION BE REDISTRIBUTED BY LICENSEE.</li>
                    <li style="margin-top: 10px;"><label>3.3 Source Code. </label>The license granted to Licensee hereunder is only to use the object code version of the Licensed Software unless the Software License Agreement indicates that Licensee has obtained a source code license. In that case, the source code version may only be used by that number of individuals for which Licensee has purchased Single-User licenses for the source code as indicated in the Software License Agreement. Object code compiled from Licensee’s use of the source code shall be subject to all of the restrictions and limitations on Licensed Software hereunder.</li>
                    <li style="margin-top: 10px;"><label>3.4 Number of Single-User Licenses. </label>Unless the Software License Agreement expressly states that the number of users of the Licensed Software is unlimited and can be used in multiple locations of the same organisation, then the use of the Licensed Software by Licensee is limited to the number of Single Users within one business location, asindicated in the Software License Agreement and for which Licensee has paid Licensor license fees. The restriction on the number of Single-User licenses does not restrict the number of copies of Licensed Software that may be redistributed in accordance with Section 3.2 if Redistribution Rights are granted under the Software License Agreement.</li>
                    <li style="margin-top: 10px;"><label>3.5 Additional Restrictions.</label></li>
                    <ul style="list-style: none"><li>(a) The Licensed Software may not be sold, leased, assigned, loaned or otherwise transferred or provided to a third party, except that (a) if Licensee has been granted Redistribution Rights pursuant to Section 3.2, then it may redistribute object code versions of the Licensed Software solely in accordance with Section 3.2, and (b) the Agreement may be assigned in the circumstances described in Section 12.8.</li>
                    <li>(b) The Licensed Software may not be modified by Licensee unless Licensor obtains a license to the source code version of the Licensed Software in accordance with Section 3.3.</li>
                    <li>(c) Licensee is not permitted to make copies of the Licensed Software or Documentation, other than that number of copies which is necessary to exercise Licensee’s rights granted hereunder. Licensee shall maintain a record of the location of all permitted copies.</li>
                    <li>(d) THE LICENSED SOFTWARE IS NOT DESIGNED, INTENDED, OR AUTHORIZED FOR USE IN ANY TYPE OF SYSTEM OR APPLICATION IN WHICH THE FAILURE OF THE SYSTEM OR APPLICATION COULD CREATE A SITUATION WHERE PERSONAL INJURY OR DEATH MAY OCCUR (E.G., MEDICAL SYSTEMS, LIFE-SUSTAINING OR LIFE-SAVING SYSTEMS).</li>
                        <li>(e) Licensee’s use of the License Software shall be subject to the additional restrictions set forth in the Software License Agreement, if any.</li></ul>
                    <li style="margin-top: 10px;"><label>3.6 Documentation License. </label>Licensor hereby grants to Licensee a non-transferable, nonexclusive, world-wide license to use any materials and documentation that Licensor may provide with the Licensed Software (the “Documentation”), if any, solely in connection with Licensee’s authorized use of the Licensed Software.</li>
                    <li style="margin-top: 10px;"><label>3.7 Updates and Upgrades.</label> Licensee acknowledges and agrees that, Licensor will use commercially reasonable efforts to provide updates and upgrades to provide error corrections to the Licensed Products, as well as minor improvements to the Licensed Products, as such corrections and improvements become generally available. Any other upgrades or enhancements to the Licensed Products are not made available by Tibet Innovation as part of these Terms and Conditions and may be subject to additional charges..</li>
                    Storibud will have no obligation to provide Maintenance Services for any Licensed Products that are damaged, modified (by anyone other than Tibet Innovation), incorporated into other software, or installed in any computing environment not supported by Tibet Innovation; or for any version of a Licensed Product other than the latest and immediately preceding version; or for any problems caused by your negligence, abuse, misuse, or by any causes beyond Tibet Innovation‘s reasonable control.</li>
                    <li style="margin-top: 10px;"><label>3.8 Licensor Intellectual Property Rights.</label> Licensee agrees that the Licensed Software and Documentation, including the structure, sequence and organization of the Licensed Software are proprietary and confidential to Licensor, and Licensee will take all reasonable measures to protect the confidentiality of the Licensed Software and Documentation unless, and to the extent, Licensor makes them available to the public without such restrictions. Licensee further agrees that all right, title and interest in and to the Licensed Software (and all intellectual property rights embodied therein) or any modification or improvement thereof made by Licensor is proprietary to Licensor. All rights therein not expressly granted to Licensee under the Agreement are reserved by Licensor.</li>
                    <li style="margin-top: 10px;"><label>3.9 Licensee Intellectual Property Rights.</label> Licensor acknowledges that nothing herein shall grant Licensor any rights in Licensee, Products or any intellectual property rights associated therewith.</li>
                    Licensee acknowledges that the development of the Software is an ongoing process and that Licensee and other licensees of the Software benefit from the improvements resulting from such ongoing development. To facilitate such ongoing development, Licensee may provide certain suggestions, documentation, materials and other data to Tibet Innovation regarding the use, improvement or applications of the Software (the “Contributed Ideas”), and Licensee hereby acknowledges and agrees that all Contributed Ideas may be used by Tibet Innovation in the development of the Software and/or related products and services. Unless specifically provided in a writing signed by Tibet Innovation and Licensee and specifically relating to the disclosure of any Contributed Ideas, and notwithstanding any provision in this Agreement to the contrary, Licensee hereby grants to Tibet Innovation the irrevocable, perpetual, nonexclusive, worldwide, royalty-free right and license to disclose, use and incorporate the Contributed Ideas in connection with the development of the Software and/or related products and services, and the demonstration, display, license, reproduction, modification and distribution and sale of the Software and/or related products and services, without any obligation to provide any accounting or other reporting.</li>

                </ul>

                <li><label>LICENSEE’S OBLIGATIONS. </label> Except as expressly set forth in the Agreement otherwise, Licensee agrees: (a) not to remove from any copies of the Licensed Software or Documentation any product identification, copyright or other notices; and (b) not to reproduce, modify, translate or create derivative works of all or any portion of the Licensed Software except as expressly permitted by the Agreement. Licensee further agrees that it shall not make any use of the Licensed Software and Documentation except as specifically permitted by the Agreement.</li>

                <li> <label>DELIVERY. </label> Licensor shall deliver the Licensed Software in accordance with Section 5 of the Software License Agreement.</li>

                <li> <label>PAYMENT TERMS. </label> Licensee shall pay the fees to Licensor as stated in the Software License Agreement (“Fees”) on the due date(s) indicated therein. Such Fee(s) are exclusive of any federal, state, municipal or other governmental taxes, duties, licenses, fees, excises or tariffs (“Charges”) imposed on the production, storage, licensing, sale, transportation, import, export, or use of the Licensed Software. Licensee agrees to pay, and to indemnify and hold Licensor harmless from such Charges; provided, however, Licensee shall not be responsible for taxes based on Licensor’s income.Licensee shall, if applicable, provide an exemption certificate acceptable to Licensor and the applicable authority as necessary. All amounts payable hereunder by Licensee shall be payable in Euros without deductions for taxes, assessments, fees, or charges of any kind. Cheques shall be madepayable to Tibet Innovations and shall be forwarded to
                    <ul style="list-style:none;padding: 0px"><li><label>STORIBUD</label></li>
                        <li><label>Chaussée de Dieleghem 83, F01, 1090</label></li>
                        <li><label></label>Brussels, Belgium.</li></li>
                        <li><label>BANK ACCOUNT DETAILS:</label></li>
                        <li><label>Bank name:PNB PARIBAS</label></li>
                        <li><label>ACCOUNT NUMBER: BE 53 0017 8729 8253</label></li>
                        <li><label>SWIFT /BIC CODE:GEBABEBB</label></li>

                        <li><label>PAYPAL ACCOUNT</label></li>
                        <li><label>storibud.storibud@gmail.com (Paypal charges are to be paid by licensee)</label></li>
                    </ul>
                </li>

                <li> <label>REPORTS AND AUDIT.</label> No more than once annually and upon reasonable prior notice, Licensor may audit Licensee’s usage and records to ensure that Licensee is using the Licensed Software in compliance with the Agreement. Any such audit will be conducted during regular business hours at Licensee’s offices and shall not interfere unreasonably with Licensee’s business activities. If an audit reveals that Licensee has underpaid the total fees due for the audited period by more than five percent(5%) or other material non-compliance with the Agreement, then Licensee shall pay Licensor's costs of conducting the audit, as well as any underpaid amount within thirty (30) days of notice from Licensor.</li>

                <li><label>WARRANTY DISCLAIMER.</label> THE LICENSED SOFTWARE AND DOCUMENTATION ARE PROVIDED “AS IS.” LICENSOR MAKES NO WARRANTIES OR REPRESENTATIONS RELATING TO THE LICENSED SOFTWARE, EXPRESS OR IMPLIED, STATUTORY OR OTHERWISE, AND EXPRESSLY EXCLUDES THE WARRANTY OF NON-INFRINGEMENT OF THIRD-PARTY RIGHTS, FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY. LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL SATISFY LICENSEE’S REQUIREMENTS, THAT THE LICENSED SOFTWARE IS WITHOUT DEFECT OR ERROR OR THAT OPERATION OF THE LICENSED SOFTWARE WILL BE UNINTERRUPTED.</li>

                <li><label>LIMITATION OF LIABILITY.</label> LICENSOR SHALL NOT BE LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES, OR DAMAGES FOR LOSS OF PROFITS, REVENUE, DATA, OR USE, INCURRED BY LICENSEE OR ANY THIRD PARTY, WHETHER IN AN ACTION IN CONTRACT OR TORT (INCLUDING NEGLIGENCE), OR ANY OTHER LEGAL THEORY, EVEN IF LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</li>

                <li><label>INDEMNIFICATION.</label> Licensee shall indemnify and hold harmless Tibet Innovations, their inventory, officers, agents and employees from and against any claims, demands, or causes of action whatsoever relating to the Agreement, including without limitation those arising on account of Licensee’s use of the Licensed Software or otherwise caused by, or arising out of, or resulting from, the exercise or practice of the license granted hereunder by Licensee, its permitted sublicenses, if any, its subsidiaries or their officers, employees, agents or representatives.</li>

                <li><label> TERM AND TERMINATION</label></li>
                <ul style="padding: 0px;list-style: none">
                    <li style="margin-top: 5px"><label>11.1 Term.</label> The term of the Agreement shall commence on the Effective Date and shall continue for the period indicated as the Term in the Software License Agreement, unless terminated earlier as set forth below.</li>
                    <li style="margin-top: 5px"><label>11.2 Termination for Breach.</label> Either party may terminate the Agreement immediately for a material breach by the other party if such material breach of any provision under the Agreement is not cured within thirty (30) business days after receipt of written notice of breach by the non-breaching party.</li>
                    <li style="margin-top: 5px"><label>11.3 Effect of Termination.</label> At the termination or expiration of the Agreement, (a) Licensee must destroy and/ or return all copies of the Licensed Software (except those already incorporated into Licensee Products); provided, however, that Licensee may keep a reasonable number of copies of the LicensedSoftware, solely to support customers that have previously purchased the LicenseeProducts; (b) all feesdue to Licensor shall immediately become due and payable by Licensee to Licensor; and (c) provided that Licensee’s customers are in compliance with their end user license agreements, all customers may continue to use the Licensed Software as incorporated in the Licensee Products. The following terms of the Agreement shall survive any expiration or termination: Sections 1 (Definitions), 3.8 (Licensor Intellectual Property Rights), 3.9 (Licensee Intellectual Property Rights), 4 (Licensee’s Obligations), 6 (Payment Terms) with respect to any payment obligations that accrued prior to termination or expiration of the Agreement, 8 (Warranty Disclaimer), 9 (Limitation of Liability), 10(Indemnification), 11 (Term and Termination), and 12 (General Provisions).</li>
                </ul>
                <li> <label>GENERAL PROVISIONS</label></li>
                <ul style="list-style: none;padding: 0px">
                    <li style="margin-top: 5px"><label>12.1 Use of Names. </label>Licensee may use the name “Tibet Innovations” only in connection with factually based materials related to its use of the Licensed Software. Licensor may use Licensee’s name only in connection with factually based Licensor publicity related to Licensor intellectual property and commercialization activities and achievements.</li>
                    <li style="margin-top: 5px"><label>12.2 Notices.</label> Any notice or reports required or permitted to be given under the Agreement shall be given in writing and shall be delivered in a manner that provides confirmation or acknowledgement of delivery. Notices to Licensor shall be sent to the address set forth in Section 6. Notices to Licensee shall be sent to the address set forth in the Software LicenseAgreement.</li>
                    <li style="margin-top: 5px"><label>12.3 Severability.</label> If any provision hereof shall be held illegal, invalid or unenforceable, in whole or in part, such provision shall be modified to the minimum extent necessary to make it legal, valid and enforceable, and the remaining provisions of the Agreement shall not be affected thereby.</li>
                    <li style="margin-top: 5px"><label>12.4 Headings.</label> The paragraph headings and captions of the Agreement are included merely for convenience of reference and are not to be considered part of, or to be used in interpreting the Agreement and in no way limit or affect any of the contents of the Agreement or its provisions.</li>
                    <li style="margin-top: 5px"><label>12.5 Governing Law.</label> The Agreement shall be construed in accordance with and all disputes hereunder shall be governed by the laws of the Country of Belgium as applied to transactions taking place wholly within Belgium between Belgian Businesses and their customers home and abroad . The parties exclude in its entirety the application to the Agreement of the United Nations Convention on Contracts for the International MVID-CODE SPECTRA  and its accessories .</li>
                    <li style="margin-top: 5px"><label>12.6 Assignment.</label> Licensee shall not directly or indirectly sell, transfer, assign, convey, pledge, encumber or otherwise dispose of the Agreement without the prior written consent of Licensor,which consent will not be unreasonably withheld. In the event that Licensor consents to an assignment,there will be a license assignment fee imposed by Licensor in the amount set forth in theSoftware License Agreement. In no event will Licensor assign Licensed Software or Documentation toLicensee. Assignments shall include assignments or transfers of the Agreement as part of a corporatereorganization, consolidation, merger or sale of substantially all assets or any other change of control.</li>
                    <li style="margin-top: 5px"><label>12.7 Relationship of the Parties.</label> Nothing contained in the Agreement shall be construed as creating any agency, partnership, or other form of joint enterprise between the parties. The relationship between the parties shall at all times be that of independent contractors. Neither party shall have authority to contract for or bind the other in any manner whatsoever. The Agreement confers no rights upon either party except those expressly granted herein.</li>
                    <li style="margin-top: 5px"><label>12.8 Counterparts.</label> The Agreement may be executed in two or more counterparts, each of which shall be deemed an original, but all of which together shall constitute one and the same instrument.</li>
                    <li style="margin-top: 5px"><label>12.9 Entire Agreement.</label> The Agreement is the complete, entire, final and exclusive statement of the terms and conditions of the agreement between the parties. The Agreement supersedes, and the terms of the Agreement govern, any prior or collateral agreements between the parties with respect to the subject matter hereof. The Agreement may not be modified except in a writing executed by duly authorized representatives of the parties. The terms and conditions of the Agreement shall prevail notwithstanding any variance with the terms and conditions of any other instrument submitted by Licensee.</li>
                    <li style="margin-top: 5px"><label>12.10 Government End Users.</label> The Licensed Software is a "commercial item," as that term is defined in applicablelaws.  All Government End Users acquire the Licensed Software with only those rightsas set forth herein.</li>
                    <li style="margin-top: 5px">12.11. The Licensor and Licensee agree that during the Term of the Agreement and for a period of one (1) year following its expiration or termination, neither of them will entice, solicit or encourage any employee of the other party to leave such party’s employ and neither party may hire an employee of the other party.;</li>
                </ul>
            </ol>

    </div>
  <?php
  include ('footer.php');

?>
<script>
    window.onload = function () {
//            alert('loaded----');
        $('.row.tile_count').css('display','none');
    }
</script>
</html>
