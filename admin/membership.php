<?php
include('header.php');
include('api/Classes/CONNECT.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
require_once('api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">
        <a href="users"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
                <div class="count" id="userCount"></div>
                <span class="count_bottom"><i class="green">Click </i>to Expand</span>
            </div></a>
        <a href="books"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-volume-up"></i> Total Audio Books</span>
                <div class="count" id="booksCount"></div>
                <span class="count_bottom"><i class="green"></i> in All Categories</span>
            </div></a>
        <a href="index"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-book"></i> Total Categories</span>
                <div class="count green" id="catCount"></div>
                <span class="count_bottom"><i class="green"></i> Click to Expand</span>
            </div></a>
        <a href="membership"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-users"></i> MemberShip Types</span>
                <div class="count" id="membershipCount"></div>
                <span class="count_bottom"> Click to View</span>
            </div></a>
        <a href="discount"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-cc-discover"></i> Discount Coupons</span>
                <div class="count" id="allCoupon"></div>
                <span class="count_bottom"><i class="green" id="activeCoupon"></i> is Still Active</span>
            </div></a>
        <a href="orders"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-first-order"></i> Orders</span>
                <div class="count" id="orderCount"></div>
                <span class="count_bottom"><i class="green"></i>Click to Expand</span>
            </div></a>
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All MemberShips <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <button onclick="window.location='api/excelProcess.php?dataType=allPlans'" class="btn btn-info btn-sm">Download Excel File</button>
                            </li>
                            <li><a class="btn btn-info" onclick='addMemberShipPlan()' style="color:white"><i class="fa fa-plus" ></i> Add New MemberShip</a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            Manage All the MemberShip Plans and Also Admin can Create New MemberShip Plan for Users..
                        </p>
                        <table id="memberTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>MemberShip Name</th>
                                <th>Price</th>
                                <th>Renewal Type</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $link = $conn->connect();
                            if ($link) {
                                $query = "select * from membership order by plan_id DESC";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
                                        while ($planData = mysqli_fetch_array($result)) {
                                            $j++;
                                            ?>
                                            <tr>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <td data-title='MemberShip Name'>
                                                    <?php echo $planData['plan_name'] ?>
                                                </td>
                                                <?php
                                                $amount = $planData['plan_price'];
                                                setlocale(LC_MONETARY, 'en_IN');
                                                $amount = money_format('%!i', $amount);
                                                ?>
                                                <td data-title='Price'>$<?php echo $amount ?> /Month</td>
                                                <?php
                                                $plan = $planData['renewal_type'];
                                                if($plan == "all"){
                                                    $plan = "Monthly, Yearly";
                                                }
                                                ?>
                                                <td data-title='Renewal Type'><?php echo $plan ?></td>
                                                <td class='buttonsTd' data-title='Plan Status'>
                                                    <?php
                                                    $box = "";
                                                    if ($planData['plan_status'] == "1") {
                                                        $box = "checked";
                                                    }
                                                    ?>
                                                    <input data-onstyle='info' class='planstatus' <?php echo $box ?>
                                                    data-toggle='toggle' value="<?php echo $planData['plan_status'] ?>"
                                                    id="<?php echo $planData['plan_id']?>" data-size='mini' data-style='ios'
                                                    type='checkbox' />
                                                </td>
                                                <td data-title='Action'>
                                                    <i class='fa fa-edit' onclick=editMemberShipPlan('<?php echo $planData['plan_id'] ?>') style='color:#31B0D5;cursor: pointer'></i>
                                                    <i class='fa fa-trash' onclick=deleteMemberShipPlan('<?php echo $planData['plan_id'] ?>') style='color:#D05E61;cursor: pointer'></i>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    $(document).ready(function () {
        $('#memberTable').DataTable({});
    });
</script>
