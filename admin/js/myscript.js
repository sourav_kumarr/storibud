/**
 * Created by Sourav on 3/10/2017.
 */
var main_items = [],main_items1 = [], edit_items = [];
var isFileChanged = "no";

function addNewCategory(parent_id){
    var url = "api/categoryProcess.php";
    $.post(url,{"type":"getCategory","cat_id":parent_id} ,function (data) {
        var status = data.Status;
        var options="";
        if (status == "Success") {
            var catData = data.catData;
            options += "<option value='"+catData.cat_id+";"+catData.level+"' >"+catData.cat_name+"</option>";
        }
        var cat_name = catData.cat_name;
        $(".modal-header").css({"display": "block"});
        $(".modal-title").html("Add New Category");
        $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
        "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
        "</label></div><br><div class='row'><div class='col-md-6 form-group'><label>Select Parent Category</label>" +
        "<select class='form-control' id='cat_parent'>" + options + "</select>" +
        "</div><div class='col-md-6 form-group'><label>Enter Category Name</label><input type='text'" +
        "id='add_cat_name' value='' placeholder='Enter Category Name' class='form-control' /></div></div><div class='row'>" +
        "<div class='col-md-6 form-group' ><label>Category Display Picture</label><input type='hidden' name='type' " +
        "value='addCategory' /><input type='file'" +
        "id='add_cat_image' onchange=setPhoto(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
        "/><div class='photobox'><img src='images/img.png' class='livepic img-responsive' /></div>" +
        "</div><input type='hidden' value='addCategory' id='type' /><div class='form-group col-md-6' style='text-align:right;margin-top:40px'><input type='button' " +
        "value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp; <input type='button' value='Add Category' " +
        "onclick=addContent('','"+encodeURI(cat_name)+"') class='formbtn btn btn-info' /></div> </div><img src='images/default.gif' class='loadNow' />");
        $(".modal-footer").css({"display": "none"});
        $("#myModal").modal("show");
    });
}

function viewSongBank(id) {
    // alert("song bank is called---");
    window.location = "cdet.php?_="+id+"&type=songbank";
}

function viewVideoBank(id) {
    window.location = "cdet.php?_="+id+"&type=videobank";
}


function viewRefixFM(id) {
    // alert("refixfm is called");
    window.location = "cdet.php?_="+id+"&type=refixfm";
}

function viewRefixTV(id) {
    // alert("refixfm is called");
    window.location = "cdet.php?_="+id+"&type=refixtv";
}
function viewRefixLiterature(id) {
    // alert("refixfm is called");
    window.location = "cdet.php?_="+id+"&type=refixlit";
}
function editCategoryData(cat_id) {
    var url = "api/categoryProcess.php";
    var options = "";
    var url2 = "api/categoryProcess.php";
    $.post(url2, {"type": "getCategory", "cat_id": cat_id}, function (data2) {
        var status2 = data2.Status;
        if (status2 == "Success") {
            var catData2 = data2.catData;
            var parent_id = catData2.parent_id;
            var level = catData2.level;
            $.post(url,{"type":"getCategory","cat_id":parent_id} ,function (data) {
                var status = data.Status;

                if (status == "Success") {
                    var catData = data.catData;
                    options = options+"<option selected='selected' value='"+catData.cat_id+";"+catData.level+"' >"+catData.cat_name+"</option>";
                    $(".modal-header").css({"display":"block"});
                    $(".modal-title").html("Edit Category");
                    $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                    "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                    "</label></div><br><div class='row'><div class='col-md-6 form-group'><label>Select Parent Category</label>" +
                    "<select class='form-control' id='cat_parent'>" + options + "</select>" +
                    "</div><div class='col-md-6 form-group'><label>Enter Category Name</label><input type='text'" +
                    "id='add_cat_name' value='"+catData2.cat_name+"' class='form-control' /></div></div><div class='row'>" +
                    "<div class='col-md-6 form-group' ><label>Category Display Picture</label><input type='hidden' name='type' " +
                    "value='addCategory' /><input type='file'" +
                    "id='add_cat_image' onchange=setPhoto(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
                    "/><div class='photobox'><img src='api/Files/images/"+catData2.cat_image+"' class='livepic img-responsive' /></div>" +
                    "</div><input type='hidden' value='editCategory' id='type' /><div class='form-group col-md-6' style='text-align:right;margin-top:40px'><input type='button' " +
                    "value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp;<input type='button' value='Update Category' " +
                    "onclick=addContent('"+cat_id+"','"+encodeURI(catData.cat_name)+"') class='formbtn btn btn-info' /></div> </div><img src='images/default.gif' class='loadNow' />");
                    $(".modal-footer").css({"display":"none"});
                    $("#myModal").modal("show");
                }
            });
        }
    });
}
function changeApproval(approval_id,element_id,approval_status) {
    var url = "api/categoryProcess.php";
    $.post(url,{"type":"changeApproval",'approval_id':approval_id,'approval_status':approval_status,'element_id':element_id} ,function (data) {
        var status = data.Status;
        setTimeout(function(){
            if (status == "Success") {
                showMessage(data.Message,"green");
                location.reload(true);
            }
            else{
                showMessage(data.Message,"red");
            }
        },500);
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
}
function addContent(cat_id,cat_type)
{
    var cat_parent = $("#cat_parent").val();
    var cat_name = $("#add_cat_name").val();
    var cat_image = $('#add_cat_image').val();
    var type = $('#type').val();
    var admin_type = $("#admin_type").val();
    // alert(admin_type);
    var data = new FormData();
    if(type == "addCategory") {
        if (cat_name == "" || cat_image == "") {
            $("#message").html("Please Fill All the Required Feilds");
            return false;
        }

        else {
            $("#message").html("");
            $(".loadNow").css({"display": "block"});
            $(".formbtn").attr("disabled", true);
            var admin_id = $("#admin_id").val();

            var parent_id = cat_parent.split(";")[0];
            var parentlevel = cat_parent.split(";")[1];
            if(parent_id == "0"){
                var level = "0";
            }else{
                var level = parseInt(parentlevel)+1;
            }
            var file_ext = cat_image.split(".");
            file_ext = file_ext[file_ext.length - 1];
            if (file_ext == "jpeg" || file_ext == "png" || file_ext == "jpg" || file_ext == "gif") {
                var _file = document.getElementById('add_cat_image');
                data.append('type', type);
                data.append('cat_image', _file.files[0]);
                data.append('cat_name', cat_name);
                data.append('cat_parent', parent_id);
                data.append('admin_id', admin_id);
                data.append('level', level);
                data.append('admin_type',admin_type);
                if(cat_type!='' ) {
                    cat_type = decodeURI(cat_type);

                    if (cat_type == 'Literature')
                        data.append('cat_type', 'Sub Category');
                    else
                        data.append('cat_type', 'Genre');
                }
            }
            else {
                $("#message").html("File Should Be JPG,PNG,JPEG,GIF Formats Only");
                $(".loadNow").css({"display": "none"});
                $(".formbtn").attr("disabled", false);
                return false;
            }
        }
    }
    else{
        if(cat_name == "") {
            $("#message").html("Please Fill All the Required Feilds");
            return false;
        }
        else{
            $("#message").html("");
            $(".loadNow").css({"display": "block"});
            $(".formbtn").attr("disabled", true);

            var admin_id = $("#admin_id").val();
            var parent_id = cat_parent.split(";")[0];
            var parentlevel = cat_parent.split(";")[1];
            if(parent_id == "0"){
                var level = "0";
            }else{
                var level = parseInt(parentlevel)+1;
            }
            var imageChanged = "no";
            if(cat_image != "") {
                var file_ext = cat_image.split(".");
                file_ext = file_ext[file_ext.length - 1];
                if (file_ext == "jpeg" || file_ext == "png" || file_ext == "jpg" || file_ext == "gif") {
                    var _file = document.getElementById('add_cat_image');
                    imageChanged = "yes";
                    data.append('cat_image', _file.files[0]);
                }
                else {
                    $("#message").html("File Should Be JPG,PNG,JPEG,GIF Formats Only");
                    $(".loadNow").css({"display": "none"});
                    $(".formbtn").attr("disabled", false);
                    return false;
                }
            }
            data.append('type', type);
            data.append('cat_id', cat_id);
            data.append('cat_name', cat_name);
            data.append('cat_parent', parent_id);
            data.append('admin_id', admin_id);
            data.append('admin_type', admin_type);
            data.append('level', level);
            data.append('imageChanged',imageChanged);
        }
    }
    var request = new XMLHttpRequest();
    request.onreadystatechange = function(){
        // alert(request.readyState);

        if(request.readyState == 4) {

            var response = $.parseJSON(request.response);
            var status = response.Status;

            if(status == "Success")
            {
                $("#myModal").modal("hide");
                location.reload(true);
            }
            else
            {
                $("#modal").modal("hide");
                $("#message").html(response.Message);
            }
        }

    };
    request.open('POST', 'api/categoryProcess.php');
    request.send(data);
}
function deleteCategory(cat_id,booksCount) {
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    var admin_type = $("#admin_type").val();
    if(admin_type == "super"){
        if(booksCount>0) {
            $(".modal-body").html("<span style='color:red'>This Category Contains "+booksCount+" Books. Are You Sure you want to Delete this Category</span>");
        }
        else{

            $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this Category</span>");
        }
        $(".modal-footer").css({"display":"block"});
        $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
            "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmDelete('"+cat_id+"') class='btn btn-sm btn-danger' />");
        $("#myModal").modal("show");

    }
    else{
        $(".modal-body").html("<span style='color:red'>You are not authrized to do this operation</span>");

        $(".modal-footer").css({"display":"block"});
        $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />");
        $("#myModal").modal("show");

    }
}
function confirmDelete(cat_id){
    var url = "api/categoryProcess.php";

    $.post(url,{"type":"deleteCategory","cat_id":cat_id} ,function (data) {
        var status = data.Status;
        setTimeout(function(){
            if (status == "Success"){
                showMessage(data.Message,"green");
                location.reload(true);
            }
            else{
                showMessage(data.Message,"red");
            }
        },500);
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
}
//////////////////////////////////////////////////////////////////////////////
///category end
//////////////////////////////////////////////////////////////////////////////

function addNewProduct(parent_id) {

    var url = "api/categoryProcess.php";
    var cat_type = $("#cat_type").val();
    console.log('cat type ---- ' + cat_type);
    if (!cat_type) {
        $.post(url, {"type": "getCategory", "cat_id": parent_id}, function (data) {
            console.log(JSON.stringify(data));

            var status = data.Status;
            var catShow = "";
            if (status == "Success") {
                var mainCategory = "";
                var categoryData = data.catData;
                mainCategory = data.TopParentName;
                catShow += "<option value='" + categoryData.cat_id + "' >" + categoryData.cat_name + "</option>";

                var prod_file = '<div class="radiogroup">  <ul class="list-unstyled list-inline" >'+
                    '<li><input type="radio" name="crust" id="crust1" value="prod_file" onclick="onTitleChecked(this)" checked/><label for="crust1" style="margin-left: 5px">Product File</label></li>'+
                    '<li><input type="radio" name="crust" id="crust2" value="video_link" onclick="onTitleChecked(this)" /><label for="crust2" style="margin-left: 5px" >Video Link</label></li> </ul></div>'+
                    "<input type='file' id='product_file' style='width:100%' /><input type='text' id='video_link' placeholder='enter youtube or vimeo link' style='width:100%;display: none;padding-left: 5px'/>";

                if(mainCategory!="Video") {
                  prod_file = "<label>Select Product File</label><input type='file' id='product_file' style='width:100%' />";
                }


                $(".modal-header").css({"display": "block"});
                $(".modal-title").html("Add New Product");
                $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                    "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                    "</label></div><br><div class='row'><div class='form-group col-md-3' style='padding-right: 0px'><label>Parent Category</label>" +
                    "<select id='parent_category' class='form-control'>" + catShow + "</select></div> <div class='form-group col-md-3' style='padding: 0px;padding-left: 2px'>" +
                    "<label>Artist Name</label><input type='text' id='artist_name' style='width:95%' placeholder='Enter artist name' class='form-control'/></div><div class='col-md-6 form-group'>" +
                    "<label>Product Name</label><input type='text' id='product_name' value='' placeholder='Enter Product Name' " +
                    "class='form-control' /></div></div><div class='row'><div class='col-md-6 form-group' >" +
                    "<label>Product Price</label><input type='number' class='form-control' value='' placeholder='Enter Product Price'" +
                    " id='product_price' /></div><div class='form-group col-md-6'>"+prod_file +"</div></div><div class='row'>" +
                    "<div class='col-md-6 form-group'><label>Book Description</label><textarea id='product_desc' " +
                    "placeholder='Enter Product Description' class='form-control' ></textarea></div><div class='form-group col-md-3'>" +
                    "<label>Product Image</label><input type='file' id='image_file' onchange=setPhoto(this,'livepic') " +
                    "style='position: absolute;width:58px;height:60px;opacity:0' /><div class='photobox'><img src='images/img.png'" +
                    " class='livepic img-responsive' /></div></div></div><div class='row'><div class='form-group " +
                    "col-md-6 pull-right' style='text-align:right;margin-top:40px'><input type='hidden' value='addProduct' id='type'>" +
                    "<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp;" +
                    "<input type='button' value='Add New Product' class='btn btn-info formbtn' onclick=productProcess('','" + escape(mainCategory) + "') />" +
                    "</div></div><img src='images/default.gif' class='loadNow' />");
                $(".modal-footer").css({"display": "none"});
                $("#myModal").modal("show");
            }
        });
    }

    else if (cat_type == 'songbank' || cat_type == 'videobank') {
        var catShow = "";
        var prod_file = '<div class="radiogroup">  <ul class="list-unstyled list-inline" >'+
            '<li><input type="radio" name="crust" id="crust1" value="prod_file" onclick="onTitleChecked(this)" checked/><label for="crust1" style="margin-left: 5px">Product File</label></li>'+
            '<li><input type="radio" name="crust" id="crust2" value="video_link" onclick="onTitleChecked(this)" /><label for="crust2" style="margin-left: 5px" >Video Link</label></li> </ul></div>'+
            "<input type='file' id='product_file' style='width:100%' /><input type='text' id='video_link' placeholder='enter youtube or vimeo link' style='width:100%;display: none;padding-left: 5px'/>";

        $.post(url, {"type": "getCategories","parentId":getUrlParameter("_")}, function (data) {
            var status = data.Status;
            if (status == "Success") {

                var catArray = data.data;
                var mainCategory = "Music";
                var prod_file = '<div class="radiogroup">  <ul class="list-unstyled list-inline" >'+
                    '<li><input type="radio" name="crust" id="crust1" value="prod_file" onclick="onTitleChecked(this)" checked/><label for="crust1" style="margin-left: 5px">Product File</label></li>'+
                    '<li><input type="radio" name="crust" id="crust2" value="video_link" onclick="onTitleChecked(this)" /><label for="crust2" style="margin-left: 5px" >Video Link</label></li> </ul></div>'+
                    "<input type='file' id='product_file' style='width:100%' /><input type='text' id='video_link' placeholder='enter youtube or vimeo link' style='width:100%;display: none;padding-left: 5px'/>";

                if(cat_type == 'songbank') {
                    mainCategory = "Music";
                    prod_file = "<label>Select Product File</label><input type='file' id='product_file' style='width:100%' />";

                }
                else if(cat_type == "videobank") {
                    mainCategory = "Video";
                }

                for (var i = 0; i < catArray.length; i++) {
                    // if (catArray[i].parent_id == parent_id) {
                        console.log("cats --- " + catArray[i].cat_name);
                      catShow = catShow+"<option value='" + catArray[i].cat_id + "' >" + catArray[i].cat_name+ "</option>";
                    // }
                }

                $(".modal-header").css({"display": "block"});
                $(".modal-title").html("Add New Product");
                $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                    "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                    "</label></div><br><div class='row'><div class='form-group col-md-3' style='padding-right: 0px'><label>Parent Category</label>" +
                    "<select id='parent_category' class='form-control'>" + catShow + "</select></div><div class='form-group col-md-3' style='padding: 0px;padding-left: 2px'>" +
                    "<label>Artist Name</label><input type='text' id='artist_name' style='width:95%' placeholder='Enter artist name' class='form-control'/></div><div class='col-md-6 form-group'>" +
                    "<label>Product Name</label><input type='text' id='product_name' value='' placeholder='Enter Product Name' " +
                    "class='form-control' /></div></div><div class='row'><div class='col-md-6 form-group' >" +
                    "<label>Product Price</label><input type='number' class='form-control' value='' placeholder='Enter Product Price'" +
                    " id='product_price' /></div><div class='form-group col-md-6'>"+prod_file+"</div></div><div class='row'>" +
                    "<div class='col-md-6 form-group'><label>Book Description</label><textarea id='product_desc' " +
                    "placeholder='Enter Product Description' class='form-control' ></textarea></div><div class='form-group col-md-3'>" +
                    "<label>Product Image</label><input type='file' id='image_file' onchange=setPhoto(this,'livepic') " +
                    "style='position: absolute;width:58px;height:60px;opacity:0' /><div class='photobox'><img src='images/img.png'" +
                    " class='livepic img-responsive' /></div></div></div><div class='row'><div class='form-group " +
                    "col-md-6 pull-right' style='text-align:right;margin-top:40px'><input type='hidden' value='addProduct' id='type'>" +
                    "<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp;" +
                    "<input type='button' value='Add New Product' class='btn btn-info formbtn' onclick=productProcess('','" + escape(mainCategory) + "') />" +
                    "</div></div><img src='images/default.gif' class='loadNow' />");
                $(".modal-footer").css({"display": "none"});
                $("#myModal").modal("show");
            }

        });

    }

    else if(cat_type == "refixfm") {
        // alert("refix---");
        var prod_data = "";
        $.post("api/productProcess.php", {"type": "getAllMusic1","cat_type":"Music"}, function (data) {

          if(data.Status=="Success") {
              var prodArray = data.ProductData;
              for(var i=0;i<prodArray.length;i++) {
                  prod_data = prod_data+"<span style='margin-right: 5px;width: 100%;float: left'><input type='checkbox' id='prod_"+prodArray[i].product_id+"' name='music_radio' value='"+prodArray[i].product_id+":music' style='margin-right:7px' onclick=onCheckedItem(this)>"+prodArray[i].product_name+"</span>";
              }
           $.post("api/advertProcess.php",{"type":"getAllAdvert"},function(dataa) {
               console.log('data --- '+JSON.stringify(dataa));
               var add_data = "";
               if(dataa.Status == "Success") {
                 var addArray = dataa.addData;
                 for(var i=0;i<addArray.length;i++) {
                     if(addArray[i].add_filetype == 'audio' )
                     add_data = add_data+"<span style='margin-right: 5px;width: 100%;float:left'><input type='checkbox' name='advert_radio' id='add_"+addArray[i].add_id+"' value='"+addArray[i].add_id+":add' style='margin-right:7px' onclick=onCheckedItem(this)>"+addArray[i].add_title+"</span>";
                 }
               }
               $(".modal-header").css({"display": "block"});
               $(".modal-title").html("Add New RefixFM");
               $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                   "<div class='col-md-12' style='margin: 0px;padding:0px'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                   "</label></div><div class='col-md-6' style='margin: 0px;padding:0px;margin-top: 10px'><label class='col-md-12' style='margin: 0px;padding: 0px'>Name</label><input type='text' placeholder='enter playlist name' id='playlist_name' style='width:100%;margin-right: 10px' class='col-md-12'/>" +
                   "<label class='col-md-12' style='margin: 0px;padding: 0px'>Description</label><textarea  id='playlist_desc' style='width:100%;margin-top: 1px;resize: none;height: 78px' placeholder='enter playlist description' class='col-md-12'/></div><div class='col-md-6' style='margin-top: 10px;'>" +
                   "<label class='col-md-12' style='padding:0px;margin: 0px'>Select Playlist File</label>" +
                   "<img src='api/Files/images/img.png' style='width:160px;height: 120px;border:1px gray solid;padding:2px'  id='title' onclick='playlistProfileClicked(this)' /> <input type='file' id='title_file' style='display: none'/></div> <div class='col-md-12' style='margin: 0px;padding: 0px;margin-top: 10px'>" +
                   "<label class='col-md-6' style='margin: 0px;width: 49%'>Select Music</label><label class='col-md-6' style='margin: 0px;padding: 0px;padding-left:11px'>Select Advert</label></div><div class='col-md-12' style='margin: 0px;padding: 0px'>" +
                   "<div class='col-md-6' style='width: 49%;overflow: auto;height: 200px;margin-right: 8px;'>"+prod_data+"</div>"+
                   "<div class='col-md-6' style='width: 48%;padding: 0px;overflow: auto;height: 200px;'>"+add_data+"</div> </div>"+
                    "<div class='col-md-12' style='text-align: right;margin-top: 50px'><button class='btn btn-sm btn-danger' onclick='addNewRefix()'>Submit</button><button class='btn btn-sm btn-default' data-dismiss='modal' >Cancel</button></div>");
               $(".modal-footer").css({"display": "none"});
               $(".modal-body").css("height","520px");
               $("#myModal").modal("show");

             });

          }
        });

    }

    else if(cat_type == "refixtv") {

        // alert("refix---");
        var uploaded_data = "",youtube_data = "";
        $.post("api/productProcess.php", {"type": "getAllMusic1","cat_type":"Video"}, function (data) {
            console.log('data ---- '+JSON.stringify(data));

            if(data.Status=="Success") {
                var prodArray = data.ProductData;
                for(var i=0;i<prodArray.length;i++) {
                    if(prodArray[i].product_file_type == 'prod_file')
                        uploaded_data = uploaded_data+"<span style='margin-right: 5px;width: 100%;'><input type='checkbox' id='prod_"+prodArray[i].product_id+"' name='music_radio' value='"+prodArray[i].product_id+":video' style='margin-right:7px' onclick=onCheckedItem(this)>"+prodArray[i].product_name+"</span>";
                    else if(prodArray[i].product_file_type == 'prod_link')
                        youtube_data = youtube_data+"<span style='margin-right: 5px;width: 100%;'><input type='checkbox' id='prod1_"+prodArray[i].product_id+"' name='music_radio' value='"+prodArray[i].product_id+":video1' style='margin-right:7px' onclick=onCheckedItem1(this)>"+prodArray[i].product_name+"</span>";

                }
                $.post("api/advertProcess.php",{"type":"getAllAdvert"},function(dataa) {
                    console.log('data --- '+JSON.stringify(dataa));
                    var add_data = "",add_data1 = "";
                    if(dataa.Status == "Success") {
                        var addArray = dataa.addData;
                        for(var i=0;i<addArray.length;i++) {
                            if(addArray[i].add_filetype == 'video') {
                                add_data = add_data+"<span style='margin-right: 5px;width: 100%;'><input type='checkbox' name='advert_radio' id='add_"+addArray[i].add_id+"' value='"+addArray[i].add_id+":add' style='margin-right:7px' onclick=onCheckedItem(this)>"+addArray[i].add_title+"</span>";
                                add_data1 = add_data1+"<span style='margin-right: 5px;width: 100%;'><input type='checkbox' name='advert_radio' id='add1_"+addArray[i].add_id+"' value='"+addArray[i].add_id+":add1' style='margin-right:7px' onclick=onCheckedItem1(this)>"+addArray[i].add_title+"</span>";

                            }
                        }
                    }
                     $(".modal-header").css({"display": "block"});
                     $(".modal-title").html("Add New RefixTV");
                     $(".modal-body").html('<ul class="nav nav-tabs"> <li class="active"><a href="#tab1" data-toggle="tab">Uploaded Videos</a></li>'+
                        '<li ><a href="#tab2" data-toggle="tab">Youtube Videos</a></li> </ul>' +
                        "<div class='col-md-12 tab-content'><p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                        "<div class='col-md-12 tab-pane active' id='tab1'><div class='col-md-12'  style='margin: 0px;padding:0px'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                        "</label></div><div class='col-md-6' style='margin: 0px;padding:0px;margin-top: 10px'><label class='col-md-12' style='margin: 0px;padding: 0px'>Name</label><input type='text' placeholder='enter playlist name' id='playlist_name' style='width:100%;margin-right: 10px' class='col-md-12'/>" +
                        "<label class='col-md-12' style='margin: 0px;padding: 0px'>Description</label><input type='text' id='playlist_desc' style='width:100%;margin-top: 1px' placeholder='enter playlist description' class='col-md-12'/></div><div class='col-md-6' style='margin-top: 10px;'>" +
                        "<label class='col-md-12' style='padding:0px;margin: 0px'>Select Playlist File</label>" +
                        "<img src='api/Files/images/img.png' style='width:75px;height: 69px;border:1px gray solid;padding:2px' onclick=playlistProfileClicked(this) id='title'/> <input type='file' id='title_file' style='display: none'/></div><div class='col-md-12' style='margin: 0px;padding: 0px;margin-top: 10px'>" +
                        "<span class='col-md-6' style='margin: 0px;width: 49%'>Select Video</span><span class='col-md-6' style='margin: 0px;padding: 0px;padding-left:11px'>Select Advert</span></div><div class='col-md-12' style='margin: 0px;padding: 0px'>" +
                        "<div class='col-md-6' style='display: flex;flex-direction: row;width: 49%;flex-wrap: wrap;overflow: scroll;overflow-x: hidden;height: 78px;margin-right: 8px;'>"+uploaded_data+"</div>"+
                        "<div class='col-md-6' style='display: flex;flex-direction: row;width: 48%;flex-wrap: wrap;padding: 0px;overflow: scroll;overflow-x: hidden;height: 78px;'>"+add_data+"</div> </div>"+
                        "<div class='col-md-12' style='text-align: right;margin-top: 50px'><button class='btn btn-sm btn-danger' onclick=addNewRefixTV('uploaded')>Submit</button><button class='btn btn-sm btn-default' data-dismiss='modal' >Cancel</button></div></div>" +
                        "<div class='col-md-12 tab-pane' id='tab2' style='margin: 0px;padding:0px'><div class='col-md-12'  style='margin: 0px;padding:0px'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                        "</label></div><div class='col-md-6' style='margin: 0px;padding:0px;margin-top: 10px'><label class='col-md-12' style='margin: 0px;padding: 0px'>Name</label><input type='text' placeholder='enter playlist name' id='playlist_name1' style='width:100%;margin-right: 10px' class='col-md-12'/>" +
                        "<label class='col-md-12' style='margin: 0px;padding: 0px'>Description</label><input type='text' id='playlist_desc1' style='width:100%;margin-top: 1px' placeholder='enter playlist description' class='col-md-12'/></div><div class='col-md-6' style='margin-top: 10px;'>" +
                        "<label class='col-md-12' style='padding:0px;margin: 0px'>Select Playlist File</label>" +
                        "<img src='api/Files/images/img.png' style='width:75px;height: 69px;border:1px gray solid;padding:2px' onclick=playlistProfileClicked(this) id='title1'/> <input type='file' id='title_file1' style='display: none'/></div><div class='col-md-12' style='margin: 0px;padding: 0px;margin-top: 10px'>" +
                        "<span class='col-md-12' style='margin: 0px;'>Select Video</span></div>" +
                        "<div class='col-md-12' style='margin: 0px;padding: 0px'>" +
                        "<div class='col-md-12' style='display: flex;flex-direction: row;width: 50%;flex-wrap: wrap;overflow: scroll;overflow-x: hidden;height: 78px;margin-right: 8px;'>"+youtube_data+"</div>"+
                        " </div>"+
                        "<div class='col-md-12' style='text-align: right;margin-top: 50px'><button class='btn btn-sm btn-danger' onclick=addNewRefixTV('youtube')>Submit</button><button class='btn btn-sm btn-default' data-dismiss='modal' >Cancel</button></div></div></div>");
                       $(".modal-footer").css({"display": "none"});
                       $(".modal-body").css("height","390px");
                       $("#myModal").modal("show");

                });

            }
        });

    }

    else if(cat_type == "refixlit") {

        // alert("refix---");
        var prod_data = "";

        $.post("api/productProcess.php", {"type": "getAllMusic1","cat_type":"Literature"}, function (data) {
            console.log('data ---- '+JSON.stringify(data));

            if(data.Status=="Success") {
                var prodArray = data.ProductData;
                for(var i=0;i<prodArray.length;i++) {
                    prod_data = prod_data+"<span style='margin-right: 5px;width: 100%;float:left'><input type='checkbox' id='prod_"+prodArray[i].product_id+"' name='music_radio' value='"+prodArray[i].product_id+":books' style='margin-right:7px' onclick=onCheckedItem(this)>"+prodArray[i].product_name+"</span>";
                }
                $.post("api/advertProcess.php",{"type":"getAllAdvert"},function(dataa) {
                    console.log('data --- '+JSON.stringify(dataa));
                    var add_data = "";
                    if(dataa.Status == "Success") {
                        var addArray = dataa.addData;
                        for(var i=0;i<addArray.length;i++) {
                            if(addArray[i].add_filetype == 'books' || addArray[i].add_filetype == 'image')
                                add_data = add_data+"<span style='margin-right: 5px;width: 100%;float: left'><input type='checkbox' name='advert_radio' id='books_"+addArray[i].add_id+"' value='"+addArray[i].add_id+":add' style='margin-right:7px' onclick=onCheckedItem(this)>"+addArray[i].add_title+"</span>";
                        }
                    }
                    $(".modal-header").css({"display": "block"});
                    $(".modal-title").html("Add New RefixLiterature");
                    /*$(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                        "<div class='col-md-12' style='margin: 0px;padding:0px'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                        "</label></div><div class='col-md-6' style='margin: 0px;padding:0px;margin-top: 10px'><label class='col-md-12' style='margin: 0px;padding: 0px'>Name</label><input type='text' placeholder='enter playlist name' id='playlist_name' style='width:100%;margin-right: 10px' class='col-md-12'/>" +
                        "<label class='col-md-12' style='margin: 0px;padding: 0px'>Description</label><input type='text' id='playlist_desc' style='width:100%;margin-top: 1px' placeholder='enter playlist description' class='col-md-12'/></div><div class='col-md-6' style='margin-top: 10px;'>" +
                        "<label class='col-md-12' style='padding:0px;margin: 0px'>Select Playlist File</label>" +
                        "<img src='api/Files/images/img.png' style='width:75px;height: 69px;border:1px gray solid;padding:2px'  id='title' onclick='playlistProfileClicked(this);'/> <input type='file' id='title_file' style='display: none'/></div><div class='col-md-12' style='margin: 0px;padding: 0px;margin-top: 10px'>" +
                        "<span class='col-md-6' style='margin: 0px;width: 49%'>Select Music</span><span class='col-md-6' style='margin: 0px;padding: 0px;padding-left:11px'>Select Advert</span></div><div class='col-md-12' style='margin: 0px;padding: 0px'>" +
                        "<div class='col-md-6' style='display: flex;flex-direction: row;width: 49%;flex-wrap: wrap;overflow: scroll;overflow-x: hidden;height: 78px;margin-right: 8px;'>"+prod_data+"</div>"+
                        "<div class='col-md-6' style='display: flex;flex-direction: row;width: 48%;flex-wrap: wrap;padding: 0px;overflow: scroll;overflow-x: hidden;height: 78px;'>"+add_data+"</div> </div>"+
                        "<div class='col-md-12' style='text-align: right;margin-top: 50px'><button class='btn btn-sm btn-danger' onclick='addNewRefixLiterature()'>Submit</button><button class='btn btn-sm btn-default' data-dismiss='modal' >Cancel</button></div>");
                    */

                    $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                        "<div class='col-md-12' style='margin: 0px;padding:0px'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                        "</label></div><div class='col-md-6' style='margin: 0px;padding:0px;margin-top: 10px'><label class='col-md-12' style='margin: 0px;padding: 0px'>Name</label><input type='text' placeholder='enter playlist name' id='playlist_name' style='width:100%;margin-right: 10px' class='col-md-12'/>" +
                        "<label class='col-md-12' style='margin: 0px;padding: 0px'>Description</label><textarea  id='playlist_desc' style='width:100%;margin-top: 1px;resize: none;height: 78px' placeholder='enter playlist description' class='col-md-12'/></div><div class='col-md-6' style='margin-top: 10px;'>" +
                        "<label class='col-md-12' style='padding:0px;margin: 0px'>Select Playlist File</label>" +
                        "<img src='api/Files/images/img.png' style='width:160px;height: 120px;border:1px gray solid;padding:2px'  id='title' onclick='playlistProfileClicked(this)' /> <input type='file' id='title_file' style='display: none'/></div> <div class='col-md-12' style='margin: 0px;padding: 0px;margin-top: 10px'>" +
                        "<label class='col-md-6' style='margin: 0px;width: 49%'>Select Literature</label><label class='col-md-6' style='margin: 0px;padding: 0px;padding-left:11px'>Select Advert</label></div><div class='col-md-12' style='margin: 0px;padding: 0px'>" +
                        "<div class='col-md-6' style='width: 49%;overflow: auto;height: 200px;margin-right: 8px;'>"+prod_data+"</div>"+
                        "<div class='col-md-6' style='width: 48%;padding: 0px;overflow: auto;height: 200px;'>"+add_data+"</div> </div>"+
                        "<div class='col-md-12' style='text-align: right;margin-top: 50px'><button class='btn btn-sm btn-danger' onclick='addNewRefixLiterature()'>Submit</button><button class='btn btn-sm btn-default' data-dismiss='modal' >Cancel</button></div>");

                    $(".modal-footer").css({"display": "none"});
                    $(".modal-body").css("height","520px");
                    $("#myModal").modal("show");

                });

            }
        });
    }
 }

 function onTitleChecked (ref) {
    if($('#'+ref.id).is(':checked') ) {
       // alert($('#'+ref.id).val());
        var value = $('#'+ref.id).val();
        if(value == 'prod_file') {
          $("#product_file").css("display","block");
          $("#video_link").css("display","none");
        }
        else if( value == 'video_link') {
            $("#product_file").css("display","none");
            $("#video_link").css("display","block");
        }
    }

 }



function onCheckedItem(ref) {

    if($('#'+ref.id).is(':checked')) {
     main_items.push($('#'+ref.id).val());
    }
    else{
        var index = main_items.indexOf($('#'+ref.id).val());
        main_items.splice(index,1);
    }
    console.log('items ---- '+JSON.stringify(main_items));
}

function onCheckedItem1(ref) {

    if($('#'+ref.id).is(':checked')) {

        main_items1.push($('#'+ref.id).val().substr(0,$('#'+ref.id).val().length-1));
    }
    else{
        var index = main_items1.indexOf($('#'+ref.id).val());
        main_items1.splice(index,1);
    }
    console.log('items ---- '+JSON.stringify(main_items));
    console.log('items1 ---- '+JSON.stringify(main_items1));

}

 function onCheckedEditItems(ref) {

     if($('#'+ref.id).is(':checked')) {
         edit_items.push($('#'+ref.id).val());
     }
     else{
         var index = main_items.indexOf($('#'+ref.id).val());
         var index1 = edit_items.indexOf($('#'+ref.id).val());
         if(index!= -1)
           main_items.splice(index,1);
         if(index1!=-1)
             edit_items.splice(index1,1);
     }
     console.log('edit items --- '+JSON.stringify(edit_items)+ 'length here --- '+main_items.length);
 }

function editRefixFM(idd) {
    // alert(id);
    main_items = [];
    var prod_data = "";
    $.post("api/refixProcess.php", {"type": "getParticularRefix", "fm_id": idd}, function (response) {

        var refix_data = response.refixData;
        var ref_data = refix_data.refData;
        console.log('data --- ' + JSON.stringify(response));

        for(var i=0;i<ref_data.length;i++) {
          if(ref_data[i].file_type == 'Music' || ref_data[i].file_type == 'audio') {
              main_items.push(ref_data[i].id+":"+ref_data[i].type);
          }
        }
        $.post("api/productProcess.php", {"type": "getAllMusic1", "cat_type": "Music"}, function (data) {


            if (data.Status == "Success") {
            var prodArray = data.ProductData;
            var musicData = refix_data.musicData;

            for (var i = 0; i < prodArray.length; i++) {

                var isFound = false;
                for(var j=0;j<ref_data.length;j++) {
                       if(prodArray[i].product_id == ref_data[j].id && (ref_data[j].file_type == 'Music')) {
                           prod_data = prod_data + "<span style='margin-right: 5px;width: 100%;float: left'><input type='checkbox' name='music_radio' id='music_"+prodArray[i].product_id+"' value='" + prodArray[i].product_id + ":music'  style='margin-right:7px' onclick='onCheckedEditItems(this)' checked>" + prodArray[i].product_name + "</span>";
                           isFound = true;
                           break;
                       }
                 }
                if(!isFound && prodArray[i].file_type == 'Music') {
                    prod_data = prod_data + "<span style='margin-right: 5px;width: 100%;float: left'><input type='checkbox' name='music_radio' id='music_"+prodArray[i].product_id+"' value='" + prodArray[i].product_id + ":music'  style='margin-right:7px' onclick='onCheckedEditItems(this)'>" + prodArray[i].product_name + "</span>";
                    isFound = false;
                }

            }
            $.post("api/advertProcess.php", {"type": "getAllAdvert"}, function (dataa) {
                var add_data = "";
                if (dataa.Status == "Success") {
                    var addArray = dataa.addData;
                    // var addData = refix_data.addData;
                    for (var i = 0; i < addArray.length; i++) {

                        var isFound = false;
                        for(var j=0;j<ref_data.length;j++) {
                            if(addArray[i].add_id == ref_data[j].id && (ref_data[j].file_type == 'audio')) {
                                add_data = add_data + "<span style='margin-right: 5px;width: 100%;float: left'><input type='checkbox' name='advert_radio' id='add_" + addArray[i].add_id + "' value='" + addArray[i].add_id + ":add' style='margin-right:7px' onclick='onCheckedEditItems(this)' checked>" + addArray[i].add_title + "</span>";
                                isFound = true;
                                break;
                            }
                        }
                        if(!isFound && addArray[i].add_filetype =='audio') {
                            add_data = add_data + "<span style='margin-right: 5px;width: 100%;float: left'><input type='checkbox' name='advert_radio' id='add_" + addArray[i].add_id + "' value='" + addArray[i].add_id + ":add' style='margin-right:7px' onclick='onCheckedEditItems(this)'>" + addArray[i].add_title + "</span>";
                            isFound = false;
                        }
                    }
                }
                    console.log('mainData --- '+JSON.stringify(main_items));
                    $(".modal-header").css({"display": "block"});
                    $(".modal-title").html("Edit RefixFM");

                     $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                        "<div class='col-md-12' style='margin: 0px;padding:0px'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                        "</label></div><div class='col-md-6' style='margin: 0px;padding:0px;margin-top: 10px'><label class='col-md-12' style='margin: 0px;padding: 0px'>Name</label><input type='text' placeholder='enter playlist name' id='playlist_name' style='width:100%;margin-right: 10px' value='"+refix_data.play_name+"' class='col-md-12'/>" +
                        "<label class='col-md-12' style='margin: 0px;padding: 0px'>Description</label><textarea  id='playlist_desc' style='width:100%;margin-top: 1px;resize: none;height: 78px;' placeholder='enter playlist description' class='col-md-12' >"+refix_data.play_desc+"</textarea></div><div class='col-md-6' style='margin-top: 10px;'>" +
                        "<label class='col-md-12' style='padding:0px;margin: 0px'>Select Playlist File</label>" +
                        "<img src='api/Files/images/"+refix_data.play_file+"' style='width:160px;height: 120px;border:1px gray solid;padding:2px'  id='title' onclick='playlistProfileClicked(this)' /> <input type='file' id='title_file' style='display: none'/></div> <div class='col-md-12' style='margin: 0px;padding: 0px;margin-top: 10px'>" +
                        "<label class='col-md-6' style='margin: 0px;width: 49%'>Select Music</label><label class='col-md-6' style='margin: 0px;padding: 0px;padding-left:11px'>Select Advert</label></div><div class='col-md-12' style='margin: 0px;padding: 0px'>" +
                        "<div class='col-md-6' style='width: 49%;overflow: auto;height: 200px;margin-right: 8px;'>"+prod_data+"</div>"+
                        "<div class='col-md-6' style='width: 48%;padding: 0px;overflow: auto;height: 200px;'>"+add_data+"</div> </div>"+
                        "<div class='col-md-12' style='text-align: right;margin-top: 50px'><button class='btn btn-sm btn-danger' onclick=editRefixData('"+idd+"','uploaded')>Submit</button><button class='btn btn-sm btn-default' data-dismiss='modal' >Cancel</button></div>")
                    $(".modal-footer").css({"display": "none"});
                    $(".modal-body").css("height", "520px");
                    $("#myModal").modal("show");



            });
        }
      });
    });

}


function editRefixTV(idd) {
    // alert(id);
    main_items = [];
    var uploaded_data = "",youtube_data = "";
    $.post("api/refixProcess.php", {"type": "getParticularRefix", "fm_id": idd}, function (response) {
        console.log('data --- ' + JSON.stringify(response));
        var refix_data = response.refixData;
        var ref_data = refix_data.refData;

        console.log('data --- ' + JSON.stringify(response));

        for(var i=0;i<ref_data.length;i++) {
            if(ref_data[i].file_type == 'Video' || ref_data[i].file_type == 'video') {
                if(refix_data.play_file_type == 'uploaded')
                 main_items.push(ref_data[i].id+":"+ref_data[i].type);
                else if(refix_data.play_file_type == 'youtube')
                    main_items1.push(ref_data[i].id+":"+ref_data[i].type);
            }
        }

        $.post("api/productProcess.php", {"type": "getAllMusic1", "cat_type": "Video"}, function (data) {

            if (data.Status == "Success") {
                var prodArray = data.ProductData;
                var musicData = refix_data.musicData;
                for (var i = 0; i < prodArray.length; i++) {
                    var isFound = false;
                    for(var j=0;j<ref_data.length;j++) {
                        if(prodArray[i].product_id == ref_data[j].id && (ref_data[j].file_type == 'Video')) {
                            if(prodArray[i].product_file_type == 'prod_file')
                              uploaded_data = uploaded_data + "<span style='margin-right: 5px;width: 100%;'><input type='checkbox' name='music_radio' id='prod_"+prodArray[i].product_id+"' value='" + prodArray[i].product_id + ":video'  style='margin-right:7px' onclick='onCheckedItem(this)' checked>" + prodArray[i].product_name + "</span>";
                            else if(prodArray[i].product_file_type == 'prod_link')
                                youtube_data = youtube_data + "<span style='margin-right: 5px;width: 100%;'><input type='checkbox' name='music_radio' id='prod1_"+prodArray[i].product_id+"' value='" + prodArray[i].product_id + ":video1'  style='margin-right:7px' onclick='onCheckedItem1(this)' checked>" + prodArray[i].product_name + "</span>";

                            isFound = true;
                            break;
                        }
                    }
                    if(!isFound && prodArray[i].file_type == 'Video' ) {
                        if(prodArray[i].product_file_type == 'prod_file')
                            uploaded_data = uploaded_data + "<span style='margin-right: 5px;width: 100%;'><input type='checkbox' name='music_radio' id='prod_"+prodArray[i].product_id+"' value='" + prodArray[i].product_id + ":video'  style='margin-right:7px' onclick='onCheckedItem(this)' >" + prodArray[i].product_name + "</span>";
                        else if(prodArray[i].product_file_type == 'prod_link')
                            youtube_data = youtube_data + "<span style='margin-right: 5px;width: 100%;'><input type='checkbox' name='music_radio' id='prod1_"+prodArray[i].product_id+"' value='" + prodArray[i].product_id + ":video1'  style='margin-right:7px' onclick='onCheckedItem1(this)'>" + prodArray[i].product_name + "</span>";
                        isFound = false;
                    }
                }

                $.post("api/advertProcess.php", {"type": "getAllAdvert"}, function (dataa) {
                    var add_data = "";
                    if (dataa.Status == "Success") {
                        var addArray = dataa.addData;
                        var addData = refix_data.addData;
                        for (var i = 0; i < addArray.length; i++) {

                            var isFound = false;
                            for(var j=0;j<ref_data.length;j++) {
                                if(addArray[i].add_id == ref_data[j].id && (ref_data[j].file_type == 'video')) {
                                    add_data = add_data + "<span style='margin-right: 5px;width: 100%;'><input type='checkbox' name='advert_radio' id='add_"+addArray[i].add_id+"' value='" + addArray[i].add_id + ":add' style='margin-right:7px' onclick='onCheckedItem(this)' checked>" + addArray[i].add_title + "</span>";
                                    isFound = true;
                                    break;
                                }
                            }
                            if(!isFound && addArray[i].add_filetype =='video') {
                                add_data = add_data + "<span style='margin-right: 5px;width: 100%;'><input type='checkbox' name='advert_radio' id='add_"+addArray[i].add_id+"' value='" + addArray[i].add_id + ":add' style='margin-right:7px' onclick='onCheckedItem(this)'>" + addArray[i].add_title + "</span>";
                                isFound = false;
                            }
                        }
                    }
                    var tab_type = 'uploaded';
                    $(".modal-header").css({"display": "block"});
                    $(".modal-title").html("Edit RefixTV");
                    /*$(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                        "<div class='col-md-12' style='margin: 0px;padding:0px'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                        "</label></div><div class='col-md-6' style='margin: 0px;padding:0px;margin-top: 10px'><label class='col-md-12' style='margin: 0px;padding: 0px'>Name</label><input type='text' placeholder='enter playlist name' id='playlist_name' value='"+refix_data.play_name+"' style='width:100%;margin-right: 10px' class='col-md-12'/>" +
                        "<label class='col-md-12' style='margin: 0px;padding: 0px'>Description</label><input type='text' id='playlist_desc' style='width:100%;margin-top: 1px' placeholder='enter playlist description' value='"+refix_data.play_desc+"' class='col-md-12'/></div><div class='col-md-6' style='margin-top: 10px;'>" +
                        "<label class='col-md-12' style='padding:0px;margin: 0px'>Select Playlist File</label>" +
                        '<img src="api/Files/images/'+refix_data.play_file+'" style="width:75px;height: 69px;border:1px gray solid;padding:2px"  onclick="playlistProfileClicked(this);" id="title" /> <input type="file" id="title_file" style="display: none"/></div><div class="col-md-12" style="margin: 0px;padding: 0px;margin-top: 10px">' +
                        "<span class='col-md-6' style='margin: 0px;width: 49%'>Select Music</span><span class='col-md-6' style='margin: 0px;padding: 0px;padding-left: 11px'>Select Advert</span></div><div class='col-md-12' style='margin: 0px;padding: 0px'>" +
                        "<div class='col-md-6' style='display: flex;flex-direction: row;width: 49%;flex-wrap: wrap;overflow: scroll;overflow-x: hidden;height: 78px;margin-right: 8px;'>" + prod_data + "</div>" +
                        "<div class='col-md-6' style='display: flex;flex-direction: row;width: 48%;flex-wrap: wrap;padding: 0px;overflow: scroll;overflow-x: hidden;height: 78px;'>" + add_data + "</div> </div>" +
                        "<div class='col-md-12' style='text-align: right;margin-top: 50px'><button class='btn btn-sm btn-danger' onclick=editRefixData('"+idd+"')>Submit</button><button class='btn btn-sm btn-default' data-dismiss='modal' >Cancel</button></div>");*/

                      $(".modal-body").html("<ul class='nav nav-tabs'> <li class='active'><a href='#tab1' data-toggle='tab'>Uploaded Videos</a></li>"+
                        "<li ><a href='#tab2' data-toggle='tab'>Youtube Videos</a></li> </ul>" +
                        "<div class='col-md-12 tab-content'><p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                        "<div class='col-md-12 tab-pane active' id='tab1'><div class='col-md-12'  style='margin: 0px;padding:0px'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                        "</label></div><div class='col-md-6' style='margin: 0px;padding:0px;margin-top: 10px'><label class='col-md-12' style='margin: 0px;padding: 0px'>Name</label><input type='text' placeholder='enter playlist name' id='playlist_name' style='width:100%;margin-right: 10px' value='"+refix_data.play_name+"' class='col-md-12'/>" +
                        "<label class='col-md-12' style='margin: 0px;padding: 0px'>Description</label><input type='text' id='playlist_desc' style='width:100%;margin-top: 1px' placeholder='enter playlist description' class='col-md-12' '"+refix_data.play_desc+"'/></div><div class='col-md-6' style='margin-top: 10px;'>" +
                        "<label class='col-md-12' style='padding:0px;margin: 0px'>Select Playlist File</label>" +
                        "<img src='api/Files/images/"+refix_data.play_file+"' style='width:75px;height: 69px;border:1px gray solid;padding:2px' onclick=playlistProfileClicked(this) id='title'/> <input type='file' id='title_file' style='display: none'/></div><div class='col-md-12' style='margin: 0px;padding: 0px;margin-top: 10px'>" +
                        "<span class='col-md-6' style='margin: 0px;width: 49%'>Select Video</span><span class='col-md-6' style='margin: 0px;padding: 0px;padding-left:11px'>Select Advert</span></div><div class='col-md-12' style='margin: 0px;padding: 0px'>" +
                        "<div class='col-md-6' style='display: flex;flex-direction: row;width: 49%;flex-wrap: wrap;overflow: scroll;overflow-x: hidden;height: 78px;margin-right: 8px;'>"+uploaded_data+"</div>"+
                        "<div class='col-md-6' style='display: flex;flex-direction: row;width: 48%;flex-wrap: wrap;padding: 0px;overflow: scroll;overflow-x: hidden;height: 78px;'>"+add_data+"</div> </div>"+
                        "<div class='col-md-12' style='text-align: right;margin-top: 50px'><button class='btn btn-sm btn-danger' onclick=editRefixData('"+idd+"','uploaded')>Submit</button><button class='btn btn-sm btn-default' data-dismiss='modal' >Cancel</button></div></div>" +
                        "<div class='col-md-12 tab-pane' id='tab2' style='margin: 0px;padding:0px'><div class='col-md-12'  style='margin: 0px;padding:0px'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                        "</label></div><div class='col-md-6' style='margin: 0px;padding:0px;margin-top: 10px'><label class='col-md-12' style='margin: 0px;padding: 0px'>Name</label><input type='text' placeholder='enter playlist name' id='playlist_name1' style='width:100%;margin-right: 10px' value='"+refix_data.play_name+"' class='col-md-12'/>" +
                        "<label class='col-md-12' style='margin: 0px;padding: 0px'>Description</label><input type='text' id='playlist_desc1' style='width:100%;margin-top: 1px' placeholder='enter playlist description' class='col-md-12' value='"+refix_data.play_desc+"'/></div><div class='col-md-6' style='margin-top: 10px;'>" +
                        "<label class='col-md-12' style='padding:0px;margin: 0px'>Select Playlist File</label>" +
                        "<img src='api/Files/images/"+refix_data.play_file+"' style='width:75px;height: 69px;border:1px gray solid;padding:2px' onclick=playlistProfileClicked(this) id='title1'/> <input type='file' id='title_file1' style='display: none'/></div><div class='col-md-12' style='margin: 0px;padding: 0px;margin-top: 10px'>" +
                        "<span class='col-md-12' style='margin: 0px;'>Select Video</span></div>" +
                        "<div class='col-md-12' style='margin: 0px;padding: 0px'>" +
                        "<div class='col-md-12' style='display: flex;flex-direction: row;width: 50%;flex-wrap: wrap;overflow: scroll;overflow-x: hidden;height: 78px;margin-right: 8px;'>"+youtube_data+"</div>"+
                        " </div>"+
                        "<div class='col-md-12' style='text-align: right;margin-top: 50px'><button class='btn btn-sm btn-danger' onclick=editRefixData('"+idd+"','youtube')>Submit</button><button class='btn btn-sm btn-default' data-dismiss='modal' >Cancel</button></div></div></div>");

                    $(".modal-footer").css({"display": "none"});
                    $(".modal-body").css("height", "380px");
                    $("#myModal").modal("show");

                    console.log('mainData --- '+JSON.stringify(main_items));
                    console.log('mainData1 --- '+JSON.stringify(main_items1));



                });
            }
        });
    });

}


function editRefixLitrature(idd) {
    // alert(id);
    main_items = [];
    var prod_data = "";
    $.post("api/refixProcess.php", {"type": "getParticularRefix", "fm_id": idd}, function (response) {
        console.log('data --- ' + JSON.stringify(response));
        var refix_data = response.refixData;

        var ref_data = refix_data.refData;

        console.log('data --- ' + JSON.stringify(response));

        for(var i=0;i<ref_data.length;i++) {
            if(ref_data[i].file_type == 'books' || ref_data[i].file_type == 'Literature') {
                main_items.push(ref_data[i].id+":"+ref_data[i].type);
            }
        }

        $.post("api/productProcess.php", {"type": "getAllMusic1", "cat_type": "Literature"}, function (data) {

            if (data.Status == "Success") {
                var prodArray = data.ProductData;
                var musicData = refix_data.musicData;
                for (var i = 0; i < prodArray.length; i++) {



                    var isFound = false;
                    for(var j=0;j<ref_data.length;j++) {
                        if(prodArray[i].product_id == ref_data[j].id && (ref_data[j].file_type == 'Literature')) {
                            prod_data = prod_data + "<span style='margin-right: 5px;width: 100%;float: left'><input type='checkbox' name='music_radio' id='books_"+prodArray[i].product_id+"' value='" + prodArray[i].product_id + ":books'  style='margin-right:7px' onclick='onCheckedItem(this)' checked>" + prodArray[i].product_name + "</span>";
                            isFound = true;
                            break;
                        }
                    }
                    if(!isFound && prodArray[i].file_type == 'Literature' ) {
                        prod_data = prod_data + "<span style='margin-right: 5px;width: 100%;float: left'><input type='checkbox' name='music_radio' id='books_"+prodArray[i].product_id+"' value='" + prodArray[i].product_id + ":books'  style='margin-right:7px' onclick='onCheckedItem(this)'>" + prodArray[i].product_name + "</span>";
                        isFound = false;
                    }
                }
                $.post("api/advertProcess.php", {"type": "getAllAdvert"}, function (dataa) {
                    var add_data = "";
                    if (dataa.Status == "Success") {
                        var addArray = dataa.addData;
                        var addData = refix_data.addData;
                        for (var i = 0; i < addArray.length; i++) {
                            // if(addArray[i].add_filetype == 'books' || addArray[i].add_filetype == 'image')
                            //     add_data = add_data + "<span style='margin-right: 5px;width: 100%;'><input type='checkbox' name='advert_radio' id='add_"+addArray[i].add_id+"' value='" + addArray[i].add_id + ":add' style='margin-right:7px' onclick='onCheckedItem(this)'>" + addArray[i].add_title + "</span>";

                            var isFound = false;
                            for(var j=0;j<ref_data.length;j++) {
                                if(addArray[i].add_id == ref_data[j].id && (ref_data[j].file_type == 'books' || ref_data[j].file_type == 'image')) {
                                    add_data = add_data + "<span style='margin-right: 5px;width: 100%;float: left'><input type='checkbox' name='advert_radio' id='add_"+addArray[i].add_id+"' value='" + addArray[i].add_id + ":add' style='margin-right:7px' onclick='onCheckedItem(this)' checked>" + addArray[i].add_title + "</span>";
                                    isFound = true;
                                    break;
                                }
                            }
                            if(!isFound && (addArray[i].add_filetype =='books' || addArray[i].add_filetype == 'image')) {
                                add_data = add_data + "<span style='margin-right: 5px;width: 100%;float: left'><input type='checkbox' name='advert_radio' id='add_"+addArray[i].add_id+"' value='" + addArray[i].add_id + ":add' style='margin-right:7px' onclick='onCheckedItem(this)'>" + addArray[i].add_title + "</span>";
                                isFound = false;
                            }
                        }
                    }

                    $(".modal-header").css({"display": "block"});
                    $(".modal-title").html("Edit RefixLiterature");
                    /*$(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                        "<div class='col-md-12' style='margin: 0px;padding:0px'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                        "</label></div><div class='col-md-6' style='margin: 0px;padding:0px;margin-top: 10px'><label class='col-md-12' style='margin: 0px;padding: 0px'>Name</label><input type='text' placeholder='enter playlist name' id='playlist_name' value='"+refix_data.play_name+"' style='width:100%;margin-right: 10px' class='col-md-12'/>" +
                        "<label class='col-md-12' style='margin: 0px;padding: 0px'>Description</label><input type='text' id='playlist_desc' style='width:100%;margin-top: 1px' placeholder='enter playlist description' value='"+refix_data.play_desc+"' class='col-md-12'/></div><div class='col-md-6' style='margin-top: 10px;'>" +
                        "<label class='col-md-12' style='padding:0px;margin: 0px'>Select Playlist File</label>" +
                        "<img src='api/Files/images/"+refix_data.play_file+"' style='width:75px;height: 69px;border:1px gray solid;padding:2px'  id='title' onclick='playlistProfileClicked(this)'/> <input type='file' id='title_file' style='display: none'/></div><div class='col-md-12' style='margin: 0px;padding: 0px;margin-top: 10px'>" +
                        "<span class='col-md-6' style='margin: 0px;width: 49%'>Select Music</span><span class='col-md-6' style='margin: 0px;padding: 0px;padding-left: 11px'>Select Advert</span></div><div class='col-md-12' style='margin: 0px;padding: 0px'>" +
                        "<div class='col-md-6' style='display: flex;flex-direction: row;width: 49%;flex-wrap: wrap;overflow: scroll;overflow-x: hidden;height: 78px;margin-right: 8px;'>" + prod_data + "</div>" +
                        "<div class='col-md-6' style='display: flex;flex-direction: row;width: 48%;flex-wrap: wrap;padding: 0px;overflow: scroll;overflow-x: hidden;height: 78px;'>" + add_data + "</div> </div>" +
                        "<div class='col-md-12' style='text-align: right;margin-top: 50px'><button class='btn btn-sm btn-danger' onclick=editRefixData('"+idd+"','uploaded')>Submit</button><button class='btn btn-sm btn-default' data-dismiss='modal' >Cancel</button></div>");
                    */
                    $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                        "<div class='col-md-12' style='margin: 0px;padding:0px'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                        "</label></div><div class='col-md-6' style='margin: 0px;padding:0px;margin-top: 10px'><label class='col-md-12' style='margin: 0px;padding: 0px'>Name</label><input type='text' placeholder='enter playlist name' id='playlist_name' style='width:100%;margin-right: 10px' value='"+refix_data.play_name+"' class='col-md-12'/>" +
                        "<label class='col-md-12' style='margin: 0px;padding: 0px'>Description</label><textarea  id='playlist_desc' style='width:100%;margin-top: 1px;resize: none;height: 78px;' placeholder='enter playlist description' class='col-md-12' >"+refix_data.play_desc+"</textarea></div><div class='col-md-6' style='margin-top: 10px;'>" +
                        "<label class='col-md-12' style='padding:0px;margin: 0px'>Select Playlist File</label>" +
                        "<img src='api/Files/images/"+refix_data.play_file+"' style='width:160px;height: 120px;border:1px gray solid;padding:2px'  id='title' onclick='playlistProfileClicked(this)' /> <input type='file' id='title_file' style='display: none'/></div> <div class='col-md-12' style='margin: 0px;padding: 0px;margin-top: 10px'>" +
                        "<label class='col-md-6' style='margin: 0px;width: 49%'>Select Literature</label><label class='col-md-6' style='margin: 0px;padding: 0px;padding-left:11px'>Select Advert</label></div><div class='col-md-12' style='margin: 0px;padding: 0px'>" +
                        "<div class='col-md-6' style='width: 49%;overflow: auto;height: 200px;margin-right: 8px;'>"+prod_data+"</div>"+
                        "<div class='col-md-6' style='width: 48%;padding: 0px;overflow: auto;height: 200px;'>"+add_data+"</div> </div>"+
                        "<div class='col-md-12' style='text-align: right;margin-top: 50px'><button class='btn btn-sm btn-danger' onclick=editRefixData('"+idd+"','uploaded')>Submit</button><button class='btn btn-sm btn-default' data-dismiss='modal' >Cancel</button></div>")

                    $(".modal-footer").css({"display": "none"});
                    $(".modal-body").css("height", "520px");
                    $("#myModal").modal("show");



                });
            }
        });
    });

}

function deleteRefixFM(id) {
    $(".modal-content").css({"display":"block"});
    $(".modal-dialog").css({"display":"block"});
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this RefixFM</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
        "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmRefixDelete('"+id+"') class='btn btn-sm btn-danger' />");
    $(".modal-body").css({"height":"150px"});
    $("#myModal").modal("show");

}

function confirmRefixDelete(id) {
    var url = "api/refixProcess.php";

    $.post(url,{"fm_id":id,"type":"deleteRefix"},function(data){
       console.log(JSON.stringify(data));
       window.location.reload();
    });
}

function editRefixData(id,type) {

    var playlist_name = $("#playlist_name").val();
    var playlist_desc = $("#playlist_desc").val();
    var playlist_file = $("#title_file").val();
    var title_file = document.getElementById('title_file');
    var img = ["jpg","jpeg","png","gif"];

    var ext = playlist_file.substr(playlist_file.lastIndexOf(".")+1,playlist_file.length);

    if(type == 'youtube') {
        playlist_name = $("#playlist_name1").val();
        playlist_desc = $("#playlist_desc1").val();
        playlist_file = $("#title_file1").val();
        title_file = document.getElementById('title_file1');
        img = ["jpg","jpeg","png","gif"];

        ext = playlist_file.substr(playlist_file.lastIndexOf(".")+1,playlist_file.length);

    }
    if(playlist_name == "" || playlist_desc == "" ) {
        $(".error").html("Please enter all fields");
        return false;
    }
    else if(playlist_file!="" && !isItemExists(ext,img)) {
        $(".error").html("Please select jpg,jpeg,png and gif image files");
        return false;
    }
    else if(main_items.length == 0 && type =='uploaded') {
        $(".error").html("Song or advert must be selected");
        return false;
    }
    else if(main_items1.length == 0 && type =='youtube') {
        $(".error").html("Song or advert must be selected");
        return false;
    }
    /*else if(advertList.length == 0) {
     $(".error").html("Add must be selected");
     }*/
    else {
        var url = "api/refixProcess.php";
        var data = new FormData();
        data.append("playlist_name",playlist_name);
        data.append("playlist_desc",playlist_desc);
        data.append("admin_id",$("#admin_id").val());

        if(type == 'uploaded') {


            if(edit_items.length>0) {
                for(var i=0;i<main_items.length;i++) {
                    edit_items.push(main_items[i]);
                }
                data.append("main_list",edit_items.join(","));

            }
            else{
                data.append("main_list",main_items.join(","));
            }
        }
        else
            data.append("main_list",main_items1.join(","));

        data.append("fm_id",id);
        data.append("type","editRefix");
        data.append("playlist_file",title_file.files[0]);
        data.append("file_changed",isFileChanged);

        var request = new XMLHttpRequest();

        request.onreadystatechange = function() {
            if(request.readyState == 4) {
                isFileChanged = "no";

                console.log(request.response);
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if(status == "Success")
                {
                    $("#myModal").modal("hide");
                    window.location.reload();
                    main_items = [];
                    main_items1 = [];
                    edit_items = [];
                }
                else
                {
                    $(".error").html(response.Message);
                    return false;
                }
            }
        };

        request.open('POST', url);
        request.send(data);

        console.log('main list --- ' + JSON.stringify(main_items));
    }
}
function addNewRefix() {
    var playlist_name = $("#playlist_name").val();
    var playlist_desc = $("#playlist_desc").val();
    var playlist_file = $("#title_file").val();
    var title_file = document.getElementById('title_file');
    var img = ["jpg","jpeg","png","gif"];

    var ext = playlist_file.substr(playlist_file.lastIndexOf(".")+1,playlist_file.length);

    if(playlist_name == "" || playlist_desc == "" || playlist_file == "") {
        $(".error").html("Please enter all fields");
        return false;
    }
    else if(!isItemExists(ext,img)) {
        $(".error").html("Please select jpg,jpeg,png and gif image files");
        return false;
    }
    else if(main_items.length == 0) {
        $(".error").html("Song or advert must be selected");
        return false;
    }
    /*else if(advertList.length == 0) {
        $(".error").html("Add must be selected");
    }*/
    else {
        var url = "api/refixProcess.php";
        var data = new FormData();
        data.append("playlist_name",playlist_name);
        data.append("playlist_desc",playlist_desc);
        data.append("admin_id",$("#admin_id").val());
        data.append("main_list",main_items.join(","));
        data.append("fm_type","fm");
        data.append("type","addNewAdvert");
        data.append("playlist_file",title_file.files[0]);

        var request = new XMLHttpRequest();

        request.onreadystatechange = function() {
            if(request.readyState == 4){
                console.log(request.response);
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if(status == "Success")
                {
                    $("#myModal").modal("hide");
                    window.location.reload();
                    main_items = [];
                }
                else
                {
                    $(".error").html(response.Message);
                    return false;
                }
            }
        };

        request.open('POST', url);
        request.send(data);

        console.log('main list --- ' + JSON.stringify(main_items));
    }
}


function addNewRefixTV(id) {
    var playlist_name = $("#playlist_name").val();
    var playlist_desc = $("#playlist_desc").val();
    var playlist_file = $("#title_file").val();
    var title_file = document.getElementById('title_file');
    var img = ["jpg","jpeg","png","gif"];

    var ext = playlist_file.substr(playlist_file.lastIndexOf(".")+1,playlist_file.length);

    if(id == 'youtube') {
        playlist_name = $("#playlist_name1").val();
        playlist_desc = $("#playlist_desc1").val();
        playlist_file = $("#title_file1").val();
        title_file = document.getElementById('title_file1');
        ext = playlist_file.substr(playlist_file.lastIndexOf(".")+1,playlist_file.length);
    }

    if(playlist_name == "" || playlist_desc == "" || playlist_file == "") {
        $(".error").html("Please enter all fields");
        return false;
    }
    else if(!isItemExists(ext,img)) {
        $(".error").html("Please select jpg,jpeg,png and gif image files");
        return false;
    }
    else if((main_items.length == 0 && id == 'uploaded')) {
        $(".error").html("Song or advert must be selected");
        return false;
    }
    else if((main_items1.length == 0 && id == 'youtube')) {
        $(".error").html("Song or advert must be selected");
        return false;
    }
    else {
        var url = "api/refixProcess.php";
        var data = new FormData();
        data.append("playlist_name",playlist_name);
        data.append("playlist_desc",playlist_desc);
        data.append("admin_id",$("#admin_id").val());
        if(id == 'youtube') {
            data.append("main_list",main_items1.join(","));
        }
        else {
            data.append("main_list",main_items.join(","));
        }
        data.append("fm_type","tv");
        data.append("type","addNewAdvert");
        data.append("file_type",id);
        data.append("playlist_file",title_file.files[0]);

        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if(request.readyState == 4) {
                console.log(request.response);
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if(status == "Success") {
                    $("#myModal").modal("hide");
                    window.location.reload();
                    main_items = [];
                    main_items1 = [];
                }
                else {
                    $(".error").html(response.Message);
                    return false;
                }
            }
        };

        request.open('POST', url);
        request.send(data);

        console.log('music list --- ' + JSON.stringify(main_items));
    }
}



function addNewRefixLiterature() {

    var playlist_name = $("#playlist_name").val();
    var playlist_desc = $("#playlist_desc").val();
    var playlist_file = $("#title_file").val();
    var title_file = document.getElementById('title_file');
    var img = ["jpg","jpeg","png","gif"];

    var ext = playlist_file.substr(playlist_file.lastIndexOf(".")+1,playlist_file.length);


    if(playlist_name == "" || playlist_desc == "" || playlist_file == "") {
        $(".error").html("Please enter all fields");
        return false;
    }
    else if(!isItemExists(ext,img)) {
        $(".error").html("Please select jpg,jpeg,png and gif image files");
        return false;
    }
    else if(main_items.length == 0) {
        $(".error").html("Song or advert must be selected");
        return false;
    }
    else {
        var url = "api/refixProcess.php";
        var data = new FormData();
        data.append("playlist_name",playlist_name);
        data.append("playlist_desc",playlist_desc);
        data.append("admin_id",$("#admin_id").val());
        data.append("main_list",main_items.join(","));
        data.append("fm_type","literature");
        data.append("type","addNewAdvert");
        data.append("playlist_file",title_file.files[0]);

        var request = new XMLHttpRequest();

        request.onreadystatechange = function() {
            if(request.readyState == 4){
                console.log(request.response);
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if(status == "Success")
                {
                    $("#myModal").modal("hide");
                    window.location.reload();
                    main_items = [];
                }
                else
                {
                    $(".error").html(response.Message);
                    return false;
                }
            }
        };

        request.open('POST', url);
        request.send(data);

        console.log('music list --- ' + JSON.stringify(main_items));
    }
}


function editProductData(product_id) {
    var url = "api/productProcess.php";
    var cat_type = $("#cat_type").val();
    console.log('cat type ---- ' + cat_type);
    if(!cat_type) {
        $.post(url, {"type": "getParticularProductData", "product_id": product_id}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                var productData = data.ProductData;
                var cat_id = productData.category_id;
                var url = "api/categoryProcess.php";
                $.post(url, {"type": "getCategory", "cat_id": cat_id}, function (data) {
                    var status = data.Status;
                    var catShow = "";
                    var mainCategory = "";
                    if (status == "Success") {
                        var categoryData = data.catData;
                        mainCategory = data.TopParentName;
                        console.log('mainCategory ---- '+mainCategory);
                        var prod_file = '<div class="radiogroup">  <ul class="list-unstyled list-inline" >'+
                            '<li><input type="radio" name="crust" id="crust1" value="prod_file" onclick="onTitleChecked(this)" checked/><label for="crust1" style="margin-left: 5px">Product File</label></li>'+
                            '<li><input type="radio" name="crust" id="crust2" value="video_link" onclick="onTitleChecked(this)" /><label for="crust2" style="margin-left: 5px" >Video Link</label></li> </ul></div>'+
                            "<input type='file' id='product_file' style='width:100%' /><input type='text' id='video_link' placeholder='enter youtube or vimeo link' style='width:100%;display: none;padding-left: 5px'/>";

                        if(mainCategory!="Video") {
                            prod_file = "<label>Select Product File</label><input type='file' id='product_file' style='width:100%' />";
                        }
                        catShow += "<option value='" + categoryData.cat_id + "' selected='selected' >" + categoryData.cat_name + "</option>";
                        $(".modal-header").css({"display": "block"});
                        $(".modal-title").html("Edit Product Detail");
                        $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'>" +
                            "</p><div class='row'><label class='label label-danger'>Please Fill the Required Data</label>" +
                            "<label class='error'></label></div><br><div class='row'><div class='col-md-3 form-group'>" +
                            "<label>Selected Category</label><select id='parent_category' class='form-control'>" + catShow +
                            "</select></div><div class='form-group col-md-3' style='padding: 0px;padding-left: 2px'>" +
                            "<label>Artist Name</label><input type='text' id='artist_name' style='width:95%' placeholder='Enter artist name' value='"+productData.product_artist_name+"' class='form-control'/></div> " +
                            "<div class='col-md-6 form-group'><label>Product Name</label><input type='text' id='product_name' value='" + productData.product_name +
                            "' placeholder='Enter Product Name' class='form-control' /></div></div><div class='row'>" +
                            "<div class='col-md-6 form-group'><label>Product Price</label><input type='number' " +
                            "class='form-control' id='product_price' value='" + productData.product_price + "' " +
                            "placeholder='Enter Product Price'/></div><div class='col-md-6 form-group'>"+prod_file+"</div></div><div class='row'>" +
                            "<div class='col-md-6'><label>Product Description</label><textarea class='form-control' id='product_desc'>" +
                            productData.product_desc + "</textarea></div><div class='form-group col-md-6'>" +
                            "<label>Product Image</label><input type='file' id='image_file' " +
                            "onchange=setPhoto(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0'" +
                            " /><div class='photobox'><img src='api/Files/images/" + productData.product_image + "' " +
                            "class='livepic img-responsive' /></div></div></div><div class='row'><div class='form-group " +
                            "col-md-6 pull-right' style='text-align:right;margin-top:40px'><input type='button' value='Cancel' " +
                            "data-dismiss='modal' class='btn btn-default'/>&nbsp;<input type='button' value='Update Product'" +
                            "class='btn btn-info formbtn' onclick=productProcess('" + product_id + "','" + escape(mainCategory) + "') /></div></div>" +
                            "<img src='images/default.gif' class='loadNow' /><input type='hidden' value='editProduct' id='type' />");
                        $(".modal-footer").css({"display": "none"});
                        $("#myModal").modal("show");
                    }
                });
            }
        });
    }

    else if(cat_type == "songbank" || cat_type =="videobank" ) {
        var catShow = "";
        var cat_id = $("#cat_id").val();

        $.post("api/categoryProcess.php", {"type": "getCategories","parentId":getUrlParameter("_")}, function (data) {
            console.log('result --- '+JSON.stringify(data));
            var status = data.Status;
            if (status == "Success") {

                var catArray = data.data;

                for (var i = 0; i < catArray.length; i++) {
                    // if (catArray[i].parent_id == cat_id) {
                        console.log("cats --- " + catArray[i].cat_name);
                        catShow = catShow + "<option value='" + catArray[i].cat_id + "' >" + catArray[i].cat_name + "</option>";
                    // }
                }
                $.post(url, {"type": "getParticularProductData", "product_id": product_id}, function (data) {
                    var status = data.Status;
                    if (status == "Success") {
                        var productData = data.ProductData;
                        var cat_id = productData.category_id;
                        var url = "api/categoryProcess.php";
                        $.post(url, {"type": "getCategory", "cat_id": cat_id}, function (data) {
                            var status = data.Status;
                            var mainCategory = "";
                            if (status == "Success") {
                                var categoryData = data.catData;
                                mainCategory = data.TopParentName;
                                console.log('mainCategory ---- '+mainCategory);
                                var prod_file = '<div class="radiogroup">  <ul class="list-unstyled list-inline" >'+
                                    '<li><input type="radio" name="crust" id="crust1" value="prod_file" onclick="onTitleChecked(this)" checked/><label for="crust1" style="margin-left: 5px">Product File</label></li>'+
                                    '<li><input type="radio" name="crust" id="crust2" value="video_link" onclick="onTitleChecked(this)" /><label for="crust2" style="margin-left: 5px" >Video Link</label></li> </ul></div>'+
                                    "<input type='file' id='product_file' style='width:100%' /><input type='text' id='video_link' placeholder='enter youtube or vimeo link' style='width:100%;display: none;padding-left: 5px'/>";

                                if(mainCategory!="Video") {
                                    prod_file = "<label>Select Product File</label><input type='file' id='product_file' style='width:100%' />";
                                }
                                // catShow += "<option value='" + categoryData.cat_id + "' selected='selected' >" + categoryData.cat_name + "</option>";
                                $(".modal-header").css({"display": "block"});
                                $(".modal-title").html("Edit Product Detail");
                                $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'>" +
                                    "</p><div class='row'><label class='label label-danger'>Please Fill the Required Data</label>" +
                                    "<label class='error'></label></div><br><div class='row'><div class='col-md-3 form-group'>" +
                                    "<label>Selected Category</label><select id='parent_category' class='form-control'>" + catShow +
                                    "</select></div> <div class='form-group col-md-3' style='padding: 0px;padding-left: 2px'>" +
                                    "<label>Artist Name</label><input type='text' id='artist_name' style='width:95%' placeholder='Enter artist name' value='"+productData.product_artist_name+"' class='form-control'/>" +
                                    "</div><div class='col-md-6 form-group'><label>Product Name</label><input type='text' id='product_name' value='" + productData.product_name +
                                    "' placeholder='Enter Product Name' class='form-control' /></div> </div><div class='row'>" +
                                    "<div class='col-md-6 form-group'><label>Product Price</label><input type='number' " +
                                    "class='form-control' id='product_price' value='" + productData.product_price + "' " +
                                    "placeholder='Enter Product Price'/></div><div class='col-md-6 form-group'>"+prod_file+"</div></div><div class='row'>" +
                                    "<div class='col-md-6'><label>Product Description</label><textarea class='form-control' id='product_desc'>" +
                                    productData.product_desc + "</textarea></div><div class='form-group col-md-6'>" +
                                    "<label>Product Image</label><input type='file' id='image_file' " +
                                    "onchange=setPhoto(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0'" +
                                    " /><div class='photobox'><img src='api/Files/images/" + productData.product_image + "' " +
                                    "class='livepic img-responsive' /></div></div></div><div class='row'><div class='form-group " +
                                    "col-md-6 pull-right' style='text-align:right;margin-top:40px'><input type='button' value='Cancel' " +
                                    "data-dismiss='modal' class='btn btn-default'/>&nbsp;<input type='button' value='Update Product'" +
                                    "class='btn btn-info formbtn' onclick=productProcess('" + product_id + "','" + escape(mainCategory) + "') /></div></div>" +
                                    "<img src='images/default.gif' class='loadNow' /><input type='hidden' value='editProduct' id='type' />");
                                $(".modal-footer").css({"display": "none"});
                                $("#myModal").modal("show");
                            }
                        });
                    }
                });
            }
        });

    }
}

function editproductFile(){
    $("#oldProductFile").hide();
    $("#product_file").show();
}
function productProcess(product_id,file_type)
{
    var parent_category = $("#parent_category").val();
    var product_name = $("#product_name").val();
    var product_desc = $("#product_desc").val();
    var product_price = $("#product_price").val();
    var admin_type = $("#admin_type").val();
    var artist_name = $("#artist_name").val();

    var prod_file_type = '';

    file_type = unescape(file_type);
    product_price = parseFloat(product_price);
    if(product_price<0){
        $("#message").html("Product Price Should Not Less than 0 (Zero)");
        return false;
    }
    var image_file = $('#image_file').val();
    var product_file = $('#product_file').val();
    var video_link = $('#video_link').val();

    var type = $('#type').val();
    var data = new FormData();
    if(type == "addProduct") {
        var isProdFile = false;

        if(file_type == 'Video') {
          if($("#crust1").is(":checked")) {
              console.log('in crust1');
              prod_file_type = 'prod_file';
             if(product_file == ""){
                 isProdFile = false;
             }
             else{
                 isProdFile = true;
             }
          }
          if($("#crust2").is(":checked")) {
              console.log('in crust2');

             if(video_link.length == 0) {
                 isProdFile = false;
             }
             else{
                 isProdFile = true;
             }
              prod_file_type = 'prod_link';
          }
        }
        else{
            if(product_file == ""){
                isProdFile = false;
            }
            else{
                isProdFile = true;
            }
        }

        console.log('prod file type -- '+prod_file_type+ ' isProd file -- '+isProdFile);

        if (parent_category == "" || product_name == "" || product_desc == "" || product_price.length == 0 || image_file == "" || !isProdFile || artist_name == '') {
            $("#message").html("Please Fill All the Required Feilds");
            return false;
        }
        else {
            $("#message").html("");
            $(".loadNow").css({"display": "block"});
            $(".formbtn").attr("disabled", true);
            var file_ext = product_file.split(".");
            file_ext = file_ext[file_ext.length - 1];
            if(product_price<0) {
                $("#message").html("Product price must not be less than 0");
                $(".loadNow").css({"display": "none"});
                $(".formbtn").attr("disabled", false);
                return false;

            }

            if (file_type == "Music") {
                if (file_ext != "mp3" && file_ext != "wav" && file_ext != "amr") {
                    $("#message").html("File Must Be in MP3,AMR and WAV Formats Only");
                    $(".loadNow").css({"display": "none"});
                    $(".formbtn").attr("disabled", false);
                    return false;
                }
            } else if (file_type == "Video" && prod_file_type == 'prod_file') {
                if (file_ext != "3gp" && file_ext != "mp4") {
                    $("#message").html("File Must Be in 3GP and MP4 Formats Only");
                    $(".loadNow").css({"display": "none"});
                    $(".formbtn").attr("disabled", false);
                    return false;
                }
            }
            if(file_type == "Video" && prod_file_type == 'prod_link') {
                var validYoutube = validateYouTubeUrl(video_link);
                if(!validYoutube) {
                    if(!parseVimeoUrl(video_link)) {
                        $("#message").html("Enter valid youtube or vimeo  url");
                        $(".loadNow").css({"display": "none"});
                        $(".formbtn").attr("disabled", false);
                        return false;
                    }

                }

            }
            else if (file_type == "Art") {
                if (file_ext != "jpg" && file_ext != "jpeg" && file_ext != "png" && file_ext != "gif" && file_ext != "pdf") {
                    $("#message").html("File Must Be in JPG,JPEG,PNG,GIF and PDF Formats Only");
                    $(".loadNow").css({"display": "none"});
                    $(".formbtn").attr("disabled", false);
                    return false;
                }
            } else if (file_type == "Literature") {
                if (file_ext != "doc" && file_ext != "docx" && file_ext != "txt" && file_ext != "pdf") {
                    $("#message").html("File Must Be in DOC,DOCX,TXT and PDF Formats Only");
                    $(".loadNow").css({"display": "none"});
                    $(".formbtn").attr("disabled", false);
                    return false;
                }
            }
            var _file = document.getElementById('product_file');
            var image_ext = image_file.split(".");
            image_ext = image_ext[image_ext.length - 1];
            if (image_ext == "jpg" || image_ext == "png" || image_ext == "gif") {
                var img_file = document.getElementById('image_file');

                data.append('type', type);
                data.append('cat_id', parent_category);
                data.append('prod_name', product_name);
                data.append('product_price', product_price);
                data.append('prod_desc', product_desc);
                data.append('prod_file_type', prod_file_type);
                data.append('prod_file_name',$("#video_link").val());
                data.append('file_type', file_type);
                data.append('uploaded_by', "admin");
                data.append('user_id', $("#admin_id").val());
                data.append('product_image', img_file.files[0]);
                data.append('product_file', _file.files[0]);
                data.append('admin_type', admin_type);
                data.append('artist_name',artist_name);

            }
            else {
                $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");
                $(".loadNow").css({"display": "none"});
                $(".formbtn").attr("disabled", false);
                return false;
            }
        }
    }
    else{

        var isProdFile = false;

        if(file_type == 'Video') {
            if($("#crust1").is(":checked")) {
                console.log('in crust1');
                prod_file_type = 'prod_file';
                if(product_file == ""){
                    isProdFile = false;
                }
                else{
                    isProdFile = true;
                }
            }
            if($("#crust2").is(":checked")) {
                console.log('in crust2');

                if(video_link.length == 0) {
                    isProdFile = false;
                }
                else{
                    isProdFile = true;
                }
                prod_file_type = 'prod_link';
            }
        }
        else{
            if(product_file == ""){
                isProdFile = false;
            }
            else{
                isProdFile = true;
            }
        }

        if (parent_category == "" || product_name == "" || product_desc == "" || product_price.length == 0 || !isProdFile || artist_name == "" ) {
            $("#message").html("Please Fill All the Required Feilds");
            return false;
        }
        else{
            $("#message").html("");
            $(".loadNow").css({"display": "block"});
            $(".formbtn").attr("disabled", true);
            var imageChanged = "no";
            var fileChanged = "no";

            if(product_price<0) {
                $("#message").html("Product price must not be less than 0");
                $(".loadNow").css({"display": "none"});
                $(".formbtn").attr("disabled", false);
                return false;

            }
            if(product_file != "") {
                var file_ext = product_file.split(".");
                file_ext = file_ext[file_ext.length - 1];
                if(file_type == "Music") {
                    if (file_ext == "mp3" || file_ext == "wav" || file_ext == "amr") {
                        var _file = document.getElementById('product_file');
                        fileChanged = "yes";
                        data.append('product_file', _file.files[0]);
                    }
                    else {
                        $("#message").html("Music File Should Be MP3,WAV,AMR Formats Only");
                        $(".loadNow").css({"display": "none"});
                        $(".formbtn").attr("disabled", false);
                        return false;
                    }
                }else if(file_type == "Video" && prod_file_type == 'prod_file' ) {
                    if (file_ext == "3gp" || file_ext == "mp4") {
                        var _file = document.getElementById('product_file');
                        fileChanged = "yes";
                        data.append('product_file', _file.files[0]);
                    }
                    else {
                        $("#message").html("Video File Should Be 3GP,MP4 Formats Only");
                        $(".loadNow").css({"display": "none"});
                        $(".formbtn").attr("disabled", false);
                        return false;
                    }
                }else if(file_type == "Art") {
                    if (file_ext == "mp3" || file_ext == "wav" || file_ext == "amr") {
                        var _file = document.getElementById('product_file');
                        fileChanged = "yes";
                        data.append('product_file', _file.files[0]);
                    }
                    else {
                        $("#message").html("Music File Should Be MP3,WAV,AMR Formats Only");
                        $(".loadNow").css({"display": "none"});
                        $(".formbtn").attr("disabled", false);
                        return false;
                    }
                }else if(file_type == "Literature") {
                    if (file_ext == "mp3" || file_ext == "wav" || file_ext == "amr") {
                        var _file = document.getElementById('product_file');
                        fileChanged = "yes";
                        data.append('product_file', _file.files[0]);
                    }
                    else {
                        $("#message").html("Music File Should Be MP3,WAV,AMR Formats Only");
                        $(".loadNow").css({"display": "none"});
                        $(".formbtn").attr("disabled", false);
                        return false;
                    }
                }
            }

             if(file_type == "Video" && prod_file_type == 'prod_link') {

                var validYoutube = validateYouTubeUrl(video_link);
                if(!validYoutube) {
                    if(!parseVimeoUrl(video_link)) {
                        $("#message").html("Enter valid youtube or vimeo  url");
                        $(".loadNow").css({"display": "none"});
                        $(".formbtn").attr("disabled", false);
                         return false;
                    }
                }


            }
            if(image_file != "") {
                var image_ext = image_file.split(".");
                image_ext = image_ext[image_ext.length - 1];
                if (image_ext == "jpg" || image_ext == "png" || image_ext == "gif") {
                    var img_file = document.getElementById('image_file');
                    imageChanged = "yes";
                    data.append('product_image', img_file.files[0]);
                }
                else {
                    $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");
                    $(".loadNow").css({"display": "none"});
                    $(".formbtn").attr("disabled", false);
                    return false;
                }
            }
            data.append('type', type);
            data.append('prod_id', product_id);
            data.append('imageChanged', imageChanged);
            data.append('fileChanged', fileChanged);
            data.append('file_type', file_type);
            data.append('prod_name', product_name);
            data.append('product_price', product_price);
            data.append('prod_desc', product_desc);
            data.append('prod_file_type', prod_file_type);
            data.append('prod_file_name',$("#video_link").val());
            data.append('uploaded_by', "admin");
            data.append('user_id', $("#admin_id").val());
            data.append('admin_type', admin_type);
            data.append('artist_name',artist_name);

        }
    }
    var request = new XMLHttpRequest();
    request.onreadystatechange = function(){
        if(request.readyState == 4){
            var response = $.parseJSON(request.response);
            var status = response.Status;
            if(status == "Success")
            {
                $("#myModal").modal("hide");
                // location.reload(true);
            }
            else
            {
                $("#message").html(response.Message);
                $(".loadNow").css({"display": "none"});
                $(".formbtn").attr("disabled", false);
                return false;
            }
        }
    };
    request.open('POST', 'api/productProcess.php');
    request.send(data);
}
function delete_product(product_id) {

    var admin_type = $("#admin_type").val();
    if(admin_type == "super"){
        $(".modal-content").css({"display":"block"});
        $(".modal-dialog").css({"display":"block"});
        $(".modal-header").css({"display":"block"});
        $(".modal-title").html("Delete Permission");
        $(".modal-body").css({"display":"block"});
        $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this Product</span>");
        $(".modal-footer").css({"display":"block"});
        $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
            "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmProductDelete('"+product_id+"') class='btn btn-sm btn-danger' />");
    }
    else if(admin_type == "admin" || admin_type == "user") {
        $(".modal-content").css({"display":"block"});
        $(".modal-dialog").css({"display":"block"});
        $(".modal-header").css({"display":"block"});
        $(".modal-title").html("Delete Permission");
        $(".modal-body").css({"display":"block"});
        $(".modal-body").html("<span style='color:red'>You are not authorized to do this operation</span>");
        $(".modal-footer").css({"display":"block"});
        $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />");

    }
    $("#myModal").modal("show");
}
function confirmProductDelete(product_id){
    var url = "api/productProcess.php";
    $.post(url,{"type":"deleteProduct","product_id":product_id} ,function (data) {
        var status = data.Status;
        setTimeout(function(){
            if (status == "Success"){
                showMessage(data.Message,"green");
                location.reload(true);
            }
            else{
                showMessage(data.Message,"red");
            }
        },500);
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
}
function playSong(songfile) {
    $(".modal-header").css({"display":"none"});
    $(".modal-body").css({"display":"none"});
    $(".modal-footer").css({"display":"none"});
    $(".modal-dialog").html("<audio class='playsong' style='margin:20% 0 0 40%' controls autoplay>" +
    "<source src='api/Files/audio/"+songfile+"' /></audio><i class='fa fa-close fa-2x' onclick='stopMusic()' " +
    "style='color:white;cursor:pointer;margin-top:-60px'></i>");
    $("#myModal").modal("show");
}
function playVideoSong(songfile,prod_image) {
    // $(".modal-dialog").css({"display":"block"});
    // $(".modal-header").css({"display":"block"});
    // $(".modal-body").css({"display":"block"});
    // $(".modal-footer").css({"display":"block"});
    // $(".modal-header").html("Video");
    // $(".modal-body").html('<div class="videoContainer" style="width: 580px;"> <video id="myVideo" controls preload="auto" poster="api/Files/images/'+prod_image+'" width="500" height="330" >'+
    //     '<source src="api/Files/video/'+songfile+'" type="video/mp4" /> <source src="api/Files/video/'+songfile+'" type="video/webM" /> <source src="api/Files/video/'+songfile+'" type="video/ogg" /> <source src="api/Files/video/'+songfile+'" type="video/3gpp" /> <p>Your browser does not support the video tag.</p>'+
    //     '</video><div class="control" style="margin-bottom: 20px;"><div class="progress"><span class="bufferBar"></span><span class="timeBar"></span></div> <div class="btmControl"><div class="mainControl"> <a class="btnStop lvl3 btnmain" href="#" tabindex="0" title="Stop play"></a>'+
    //      '<a class="btnBck lvl2 btnmain" href="#" tabindex="0" title="Rewind"></a><a class="btnPlay lvl1 btnmain" href="#" tabindex="0" title="Play/Pause video"></a><a class="btnFwd lvl2 btnmain" href="#" tabindex="0" title="Fast forward"></a><a class="btnEnd lvl3 btnmain" href="#" tabindex="0" title="End video"></a>'+
    //      '</div><div class="volume"><div class="sound" title="Mute/Unmute sound"></div><span class="volumeCover" title="Set volume"></span> <span class="volumeBar"></span> </div> </div> </div> <div class="loading"></div> </div>');
    // alert(songfile);

    $(".modal-header").css({"display":"block"});
    $(".modal-body").css({"display":"block"});
    $(".modal-footer").css({"display":"block"});
     $(".modal-header").html('<button type="button" class="close" data-dismiss="modal">&times;</button>'+
         '<h4 class="modal-title">Video</h4>');
    if(songfile.indexOf("youtube")!=-1){
         var video_id = songfile.split('?')[1].split('=')[1];

        $(".modal-body").html('<iframe id="video" width="570" height="315" src="//www.youtube.com/embed/'+video_id+'?rel=0" frameborder="0" allowfullscreen></iframe>');
    }
    else if(songfile.indexOf("vimeo")!=-1) {
        var video_id = songfile.substr(songfile.lastIndexOf("/")+1,songfile.length);
        // alert(video_id);
        $(".modal-body").html('<iframe src="//player.vimeo.com/video/'+video_id+'?byline=0&amp;portrait=0" width="570" height="400" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');

    }
    else{
        $(".modal-body").html("<video class='playsong' style='margin:20% 0 0 9%;width: 300px;height: 200px;' controls autoplay>" +
            "<source src='api/Files/video/"+songfile+"' type='video/mp4' /></video>" +
            "<i class='fa fa-close fa-2x' onclick='stopMusic()' style='position:absolute;color:white;cursor:pointer;top:96px'></i>");

    }
         $("#myModal").modal("show");
}
function stopMusic(){
    $(".playsong").remove();
    $("#myModal").modal("hide");
    setTimeout(function () {
        $(".modal-dialog").html("<div class='modal-content'><div class='modal-header'><button type='button' class='close' " +
        "data-dismiss='modal'>&times;</button><h4 class='modal-title'></h4></div><div class='modal-body'>" +
        "</div><div class='modal-footer'></div></div>");
    },300);
}
///////////////////////////////////////////////////////////////////////////////////
/////////////////////////////book portion End//////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
function deleteUser(user_id) {
    $(".modal-content").css({"display":"block"});
    $(".modal-dialog").css({"display":"block"});
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this User</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
    "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmUserDelete('"+user_id+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}
function confirmUserDelete(user_id){
    var url = "api/userProcess.php";
    $.post(url,{"type":"deleteUser","user_id":user_id} ,function (data) {
        var status = data.Status;
        setTimeout(function(){
            if (status == "Success"){
                showMessage(data.Message,"green");
                location.reload(true);
            }
            else{
                showMessage(data.Message,"red");
            }
        },500);
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
}
///////////////////////////////////////////////////////////////////////////////////
/////////////////////////////users portion End//////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
function deleteMemberShipPlan(plan_id) {
    $(".modal-content").css({"display":"block"});
    $(".modal-dialog").css({"display":"block"});
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this MemberShip Plan</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
    "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmPlanDelete('"+plan_id+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}
function confirmPlanDelete(plan_id){
    var url = "api/membershipProcess.php";
    $.post(url,{"type":"deletePlan","plan_id":plan_id} ,function (data) {
        var status = data.Status;
        setTimeout(function(){
            if (status == "Success"){
                showMessage(data.Message,"green");
                location.reload(true);
            }
            else{
                showMessage(data.Message,"red");
            }
        },500);
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
}
function addMemberShipPlan(){
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Add New MemberShip Plan");
    $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
    "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
    "</label></div><br><div class='row'><div class='col-md-6 form-group'><label>Plan Name</label>" +
    "<input type='text' id='plan_name' value='' placeholder='Enter Plan Name' class='form-control' />" +
    "</div><div class='col-md-6 form-group'><label>List Price</label><input type='text' class='form-control' " +
    "id='plan_price' value='' placeholder='Enter Plan Price'/></div></div><div class='row'>" +
    "<div class='col-md-6 form-group'><label>Select Renewal Type</label><select id='renewal_type' class='form-control'>" +
    "<option value='' selected='selected'> Select Renewal Type</option><option value='Monthly'>Monthly</option>" +
    "<option value='Yearly'>Yearly</option><option value='all'>Monthly,yearly</option></select></div>" +
    "<div class='col-md-6 form-group'><label>Select Visibilty Status</label><div><label><input type='radio' " +
    "name='plan_status' value='1' checked /> Show </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
    "<label><input type='hidden' value='addBook' id='type' /><input type='radio' name='plan_status' value='0' /> Hide" +
    "</label></div></div></div><div class='row'>" +
    "<div class='form-group col-md-4'></div><input type='hidden' id='servicetype' value='addPlan' />" +
    "<div class='form-group col-md-8' style='text-align:right;" +
    "margin-top:40px'><input type='button' value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp;" +
    "<input type='button' value='Add New Plan' class='btn btn-info formbtn' onclick=savePlanContent('') /></div></div>" +
    "<img src='images/default.gif' class='loadNow' />");
    $(".modal-footer").css({"display":"none"});
    $("#myModal").modal("show");
}
function editMemberShipPlan(plan_id){
    var url = "api/membershipProcess.php";
    $.post(url, {"type": "getPlan", "plan_id": plan_id}, function (data) {
        var status = data.Status;
        if (status == "Success") {
            var planData = data.planData;
            $(".modal-header").css({"display": "block"});
            $(".modal-title").html("Edit MemberShip Plan");
            $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                "</label></div><br><div class='row'><div class='col-md-6 form-group'><label>Plan Name</label>" +
                "<input type='text' id='plan_name' value='"+planData.plan_name+"' placeholder='Enter Plan Name' class='form-control' />" +
                "</div><div class='col-md-6 form-group'><label>List Price</label><input type='text' class='form-control' " +
                "id='plan_price' value='"+planData.plan_price+"' placeholder='Enter Plan Price'/></div></div><div class='row'>" +
                "<div class='col-md-6 form-group'><label>Select Renewal Type</label><select id='renewal_type' class='form-control'>" +
                "<option value='' > Select Renewal Type</option><option value='Monthly'>Monthly</option>" +
                "<option value='Yearly'>Yearly</option><option value='all'>Monthly,yearly</option></select></div>" +
                "<div class='col-md-6 form-group'><label>Select Visibilty Status</label><div><label><input type='radio' " +
                "name='plan_status' id='on' value='1' /> Show </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                "<label><input type='hidden' value='addBook' id='type' /><input id='off' type='radio' name='plan_status' value='0' /> Hide" +
                "</label></div></div></div><div class='row'>" +
                "<div class='form-group col-md-4'></div><input type='hidden' id='servicetype' value='editPlan' />" +
                "<div class='form-group col-md-8' style='text-align:right;" +
                "margin-top:40px'><input type='button' value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp;" +
                "<input type='button' value='Update Plan' class='btn btn-info formbtn' onclick=savePlanContent('"+plan_id+"') />" +
                "</div></div><img src='images/default.gif' class='loadNow' />");
            $(".modal-footer").css({"display": "none"});
            var planName = planData.renewal_type;
            if(planName == "all"){
                planName = "Monthly, Yearly";
            }
            if(planData.plan_status == "1"){
                $("#on").attr("checked",true);
            }else{
                $("#off").attr("checked",true);
            }
            $("#renewal_type").append("<option value='"+planData.renewal_type+"' selected='selected'>"+planName+"</option>");
            $("#myModal").modal("show");
        }
    });
}
function savePlanContent(plan_id)
{
    var plan_name = $("#plan_name").val();
    var plan_price = $("#plan_price").val();
    plan_price = parseFloat(plan_price);
    if(plan_price<0){
        $("#message").html("Plan Price Should Not Less than 0 (Zero)");
        return false;
    }
    var renewal_type = $("#renewal_type").val();
    var plan_status = $('input[name=plan_status]:checked').val();
    var type = $('#servicetype').val();
    var data = new FormData();
    if (plan_name == "" || renewal_type == "") {
        $("#message").html("Please Fill All the Required Feilds");
        return false;
    }
    else {
        $("#message").html("");
        $(".loadNow").css({"display": "block"});
        $(".formbtn").attr("disabled", true);
        data.append('type', type);
        data.append('plan_name', plan_name);
        data.append('plan_price', plan_price);
        data.append('renewal_type', renewal_type);
        data.append('plan_status', plan_status);
        data.append('plan_id', plan_id);
    }
    var request = new XMLHttpRequest();
    request.onreadystatechange = function(){
        if(request.readyState == 4){
            var response = $.parseJSON(request.response);
            var status = response.Status;
            if(status == "Success")
            {
                $("#myModal").modal("hide");
                location.reload(true);
            }
            else
            {
                $("#message").html(response.Message);
                $(".loadNow").css({"display": "none"});
                $(".formbtn").attr("disabled", false);
                return false;
            }
        }
    };
    request.open('POST', 'api/membershipProcess.php');
    request.send(data);
}

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////////MemberShip Plans portion End//////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
function deleteDiscountCoupon(coupon_id) {
    $(".modal-content").css({"display":"block"});
    $(".modal-dialog").css({"display":"block"});
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this Discount Coupon</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
    "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmCouponDelete('"+coupon_id+"') " +
    "class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}
function confirmCouponDelete(coupon_id){
    var url = "api/couponProcess.php";
    $.post(url,{"type":"deleteCoupon","coupon_id":coupon_id} ,function (data) {
        var status = data.Status;
        setTimeout(function(){
            if (status == "Success"){
                showMessage(data.Message,"green");
                location.reload(true);
            }
            else{
                showMessage(data.Message,"red");
            }
        },500);
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
}
function addNewDiscountCoupon(){
    window.location="addCoupon";
}
function editDiscountCoupon(coupon_id){
    var url = "api/couponProcess.php";
    $.post(url, {"type": "getCoupon", "coupon_id": coupon_id}, function (data) {
        var status = data.Status;
        if (status == "Success") {
            var couponData = data.couponData;
            $("#coupon_code").val(couponData.coupon_code);
            $("#code_value").val(couponData.coupon_value);
            var generatedDate = new Date(couponData.generated_on*1000);
            var date = generatedDate.getDate();
            var month = generatedDate.getMonth();
            var year = generatedDate.getFullYear();
            var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
            $("#datetimepicker6").val(date+" "+months[month]+" "+year);
            var expiredDate = new Date(couponData.expired_on*1000);
            var edate = expiredDate.getDate();
            var emonth = expiredDate.getMonth();
            var eyear = expiredDate.getFullYear();
            $("#datetimepicker7").val(edate+" "+months[emonth]+" "+eyear);
            if(couponData.coupon_status == "1"){
                $("#on").attr("checked",true);
            }else{
                $("#off").attr("checked",true);
            }
        }
    });
}
function validateNumberFloat(number) {
    var re = /^(?:\d{1,9})?(?:\.\d{1,9})?$/;
    return re.test(number);
}
function couponprocess(coupon_id)
{
    var coupon_code = $("#coupon_code").val();
    var code_value = $("#code_value").val();
    if(!validateNumberFloat(code_value)){
        $("#message").html("Code Value Should be Numbers Only");
        return false;
    }
    var generated_on = $("#datetimepicker6").val();
    var expired_on = $("#datetimepicker7").val();
    var coupon_status = $('input[name=coupon_status]:checked').val();
    var type = $('#couponservicetype').val();
    var data = new FormData();
    if (coupon_code == "" || code_value == "" || generated_on == "" || expired_on == "") {
        $("#message").html("Please Fill All the Required Feilds");
        return false;
    }
    else {
        $("#message").html("");
        $(".loadNow").css({"display": "block"});
        $(".formbtn").attr("disabled", true);
        data.append('type', type);
        data.append('coupon_code', coupon_code);
        data.append('coupon_value', code_value);
        data.append('generated_on', generated_on);
        data.append('expired_on', expired_on);
        data.append('coupon_status', coupon_status);
        data.append('coupon_id', coupon_id);
    }
    var request = new XMLHttpRequest();
    request.onreadystatechange = function(){
        if(request.readyState == 4){
            var response = $.parseJSON(request.response);
            var status = response.Status;
            if(status == "Success")
            {
                $("#myModal").modal("hide");
                window.location="discount";
            }
            else
            {
                $("#message").html(response.Message);
                $(".loadNow").css({"display": "none"});
                $(".formbtn").attr("disabled", false);
                return false;
            }
        }
    };
    request.open('POST', 'api/couponProcess.php');
    request.send(data);
}
$(document).ready(function () {
    var futureDate = new Date();
    var day = futureDate.getDate();
    var month = futureDate.getMonth();
    var year = futureDate.getFullYear()+1;
    var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    var newFutureDate = months[month]+" "+day+", "+year;
    $('#datetimepicker6').datetimepicker({
        format:"D MMM YYYY",
        minDate:new Date(),
        maxDate:new Date(newFutureDate),
        showTodayButton: true,
        tooltips: {
            today: 'Go to today'
        }
    });
    $('#datetimepicker7').datetimepicker({
        format:"D MMM YYYY",
        useCurrent: false, //Important! See issue #1075
        showTodayButton: true,
        tooltips: {
            today: 'Go to today'
        }
    });
    $("#datetimepicker6").on("dp.change", function (e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    $("#datetimepicker7").on("dp.change", function (e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
});
/////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////end of discount coupon data///////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

function viewOrderDetail() {
    window.location='odet?_=1';
}
function deleteOrder() {
    $(".modal-content").css({"display":"block"});
    $(".modal-dialog").css({"display":"block"});
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this Order</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
    "<input type='button' value='Delete' class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}
///////////////////////////////////////*/*/*/*/*/*/*/*/*/*/*End Of Order Part*/*/*/*/*/*/*/*/*/*/*/
////////////////////////////////////////////////
////////////////////////////////////////////////

function loadIndexData() {

    var url = "api/indexProcess.php";
    $.post(url, {"type": "getFrontPageData"}, function (data) {
        var status = data.Status;
        if (status == "Success") {
            var frontPageData = data.frontPageData;
            $("#userCount").html(frontPageData.userCount);
            $("#musicCount").html(frontPageData.musicCount);
            $("#videoCount").html(frontPageData.videoCount);
            $("#artCount").html(frontPageData.artCount);
            $("#literatureCount").html(frontPageData.literatureCount);
            //$("#orderCount").html(frontPageData.orderCount);
        }
    });

    $('ul.radio li').focusin( function() {
            $(this).addClass('focus');
        }
    );

    // Remove the "focus" value to class attribute
    $('ul.radio li').focusout( function() {
            $(this).removeClass('focus');
        }
    );
}
function setPhoto(input,showClass) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.'+showClass).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function showMessage(message,color){
    $(".modal-header").css({"display":"none"});
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:"+color+"'>"+message+"</span>");
    $(".modal-footer").css({"display":"none"});
    $("#myModal").modal("show");
    setTimeout(stopMusic,2000);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////common start for Status Change Data///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
$(document).ready(function(){
    $(".userstatus").change(function () {
        if($(this).val() == "0"){
            $(this).val("1");
        }else{
            $(this).val("0");
        }
        var value = $(this).val();
        var id = $(this).attr("id");
        var url = "api/userProcess.php";
        $.post(url,{"type":"statusChange","value":value,"user_id":id} ,function (data) {
            var status = data.Status;
            if (status == "Success"){
                showMessage(data.Message,"green");
            }
            else{
                showMessage(data.Message,"red");
            }
        }).fail(function(){
            showMessage("Server Error!!! Please Try After Some Time","red")
        });
    });
});
function changeCouponStatus(id,value){
    var url = "api/couponProcess.php";
    $.post(url,{"type":"statusChange","value":value,"coupon_id":id} ,function (data) {
        var status = data.Status;
        if (status == "Success"){
            showMessage(data.Message,"green");
        }
        else{
            showMessage(data.Message,"red");
        }
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
}
/////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////for change password of admin///////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function changeAdminPassword(admin_id) {
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html('Change Admin Password');
    $(".modal-body").html("<div class='row'><div " +
    "class='col-md-3'></div><div class='col-md-6'><div class='form-group'>" +
    "<label>Old Password</label><input type='password' class='form-control' id='oldPassword' /></div><div class='form-group'><label>" +
    "New Password</label><input type='password' class='form-control' id='newPassword' /></div><div class='form-group'><label>" +
    "Confirm Password</label><input type='password' class='form-control' id='confirmNewPassword' /></div><input type='submit' " +
    "class='btn btn-info pull-right' data-dismiss='modal' onclick=changePassword('"+admin_id+"') value='Change Password'/>" +
    "</div></div><div class='col-md-3'></div>");
    $(".modal-footer").css({"display":"none"});
    $("#myModal").modal("show");
}

function changePassword(admin_id) {

    var url = "api/admin_login.php";
    var oldPassword = $("#oldPassword").val();
    var newPassword = $("#newPassword").val();
    var confirmNewPassword = $("#confirmNewPassword").val();
    if(newPassword == confirmNewPassword) {
        setTimeout(function(){
            $.post(url, {
                "type": "changeAdminPassword",
                "old_password": oldPassword,
                "new_password": newPassword,
                "admin_id": admin_id
            }, function (data) {
                var status = data.Status;
                if (status == "Success") {
                    showMessage(data.Message, "green");
                }
                else {
                    showMessage(data.Message, "red");
                }
            }).fail(function () {
                showMessage("Server Error!!! Please Try After Some Time", "red")
            });
        },1000);
    }
    else{
        showMessage("New Password and Confirm Password Should Be Same", "red");
    }
}
function back(){
    window.history.back();
}
function refundProcess(order_id,order_status,payment_status,object){
    var value = object.value;
    if(value == "No"){
        value = "Yes";
    }else{
        value = "No";
    }
    if(order_status == "Cancelled" && payment_status == "Success"){
        var url = "api/orderProcess.php";
        $.post(url,{"type":"refundStatus","value":value,"order_id":order_id} ,function (data) {
            var status = data.Status;
            if (status == "Success"){
                showMessage("The Refund Information will be Notified to User Shortly !!! Thanks","green");
            }
            else{
                showMessage(data.Message,"red");
            }
        }).fail(function(){
            showMessage("Server Error!!! Please Try After Some Time","red")
        });
    }
    else{
        $("#"+order_id).prop("checked",false);
        showMessage("For Refund Order Status Should Be Cancelled and Payment Status Should be Success","red");
    }
}
function cancelOrder(order_id,generated_date){
    var last30 = moment().subtract(30, 'days');
    var last30ts = Math.floor(new Date(last30).getTime()/1000);
    if(generated_date<last30ts){
        showMessage("Sorry Your Order is More Than 30 Days Old so we are not able to cancel this order","red");
    }
    else{
        var url = "api/orderProcess.php";
        $.post(url,{"type":"cancelOrder","order_id":order_id} ,function (data) {
            var status = data.Status;
            if (status == "Success"){
                showMessage("Your Order has Been Cancelled SuccessFully","green");
                location.reload();
            }
            else{
                showMessage(data.Message,"red");
            }
        }).fail(function(){
            showMessage("Server Error!!! Please Try After Some Time","red")
        });
    }
}

function validateYouTubeUrl(url) {

    if (url != undefined || url != '') {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = url.match(regExp);
        if (match && match[2].length == 11) {
            // Do anything for being valid
            // if need to change the url to embed url then use below line
            return true;
        }
    }
    return false;
}

function parseVimeoUrl(url) {
    var regExp = /(?:https?:\/\/(?:www\.)?)?vimeo.com\/(?:channels\/|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/,
        match = url.match(regExp);

    return match ? match[3] : false;
}

function playlistProfileClicked(ref) {
    console.log('id here ---- '+ref.id);
    if(ref.id == 'title') {
        $('#title_file').click();
       $ ('#title_file').change(function (event) {

            var selectedFile = event.target.files[0];
            var reader = new FileReader();
            var imgtag = null;
            imgtag = document.getElementById("title");
            imgtag.title = selectedFile.name;

            reader.onload = function(event) {
                imgtag.src = event.target.result;
                isFileChanged = "yes";
            };
            reader.readAsDataURL(selectedFile);
        });
    }


    else if(ref.id == 'title1') {
        $('#title_file1').click();
        $('#title_file1').change(function (event) {

            var selectedFile = event.target.files[0];
            var reader = new FileReader();
            var imgtag = null;
            imgtag = document.getElementById("title1");
            imgtag.title = selectedFile.name;

            reader.onload = function(event) {
                imgtag.src = event.target.result;
                isFileChanged = "yes";
            };
            reader.readAsDataURL(selectedFile);
        });
    }

}

function isItemExists(item,arr) {
    for(var i=0;i<arr.length;i++) {
        if(arr[i] == item) {
            return true;
        }
    }
}
$(document).ready(function () {
    $('#startFilter').datetimepicker({
        format:"D MMM YYYY"
    });
    $('#endFilter').datetimepicker({
        format:"D MMM YYYY",
        useCurrent: false //Important! See issue #1075
    });
    $("#startFilter").on("dp.change", function (e) {
        $('#endFilter').data("DateTimePicker").minDate(e.date);
    });
    $("#endFilter").on("dp.change", function (e) {
        $('#startFilter').data("DateTimePicker").maxDate(e.date);
    });
});
(function(){
    function id(v){return document.getElementById(v); }
    function loadbar() {
        var ovrl = id("overlay"),
            prog = id("progress"),
            stat = id("progstat"),
            img = document.images,
            c = 0;
        tot = img.length;

        function imgLoaded(){
            c += 1;
            var perc = ((100/tot*c) << 0) +"%";
            prog.style.width = perc;
            stat.innerHTML = "Loading "+ perc;
            if(c===tot) return doneLoading();
        }
        function doneLoading(){
            ovrl.style.opacity = 0;
            setTimeout(function(){
                ovrl.style.display = "none";
            }, 1200);
        }
        for(var i=0; i<tot; i++) {
            var tImg     = new Image();
            tImg.onload  = imgLoaded;
            tImg.onerror = imgLoaded;
            tImg.src     = img[i].src;
        }
    }
    document.addEventListener('DOMContentLoaded', loadbar, false);
}());

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

loadIndexData();
